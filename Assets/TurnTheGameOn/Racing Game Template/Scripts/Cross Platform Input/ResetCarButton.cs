﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ResetCarButton : MonoBehaviour, IPointerClickHandler
{
    private GameObject playerCar;
    private RG_WaypointCircuit circuit;	// A route of waypoints used for pathfinding
    private RG_SceneManager sceneManager;

    void Start()
    {
        GetPlayer();
        circuit = GameObject.Find("Scene Manager").GetComponent<RG_WaypointCircuit>();
        sceneManager = GameObject.Find("Scene Manager").GetComponent<RG_SceneManager>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //Debug.Log("Resetting Car!");
        GetPlayer();
        Transform tempTransform = circuit.GetNearestWaypoint(sceneManager.racerInfo[0].currentWaypoint.position);
        playerCar.transform.position = tempTransform.position;
        playerCar.transform.rotation = tempTransform.rotation;
        playerCar.GetComponent<EVP.VehicleController>().ResetVehicle();
        
    }

    void GetPlayer()
    {
        if (playerCar == null)
        {
            playerCar = GameObject.FindGameObjectWithTag("Player");
        }
    }
}
