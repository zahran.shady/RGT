﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TurnTheGameOn.RacingGameTemplate;
using System.Collections.Generic;
using System.Collections;
using System;
using GameSparks.Api.Responses;
using GameSparks.Core;

[ExecuteInEditMode]
public class RG_GarageManager : MonoBehaviour
{
    public enum Switch { On, Off }
    public enum MusicSelection { ListOrder, Random }
    [System.Serializable]
    public class GarageSceneReference
    {
        public string developerAddress;
        public string reviewAddress;

        [Header("Text")]
        public Text currencyText;
        public Text raceNameText;
        public Text carNameText;
        public Text carSpeedText;
        public Text rewardText;
        public Text lapText;
        public Text numberOfRacersText;
        public Text selectRaceText;
        public Text selectCarText;
        public Text selectPaintText;
        public Text selectGlowText;
        public Text selectBrakeColorText;
        public Text selectRimColorText;
        public Text selectGlassTintText;
        public Text topSpeedPriceText;
        public Text accelerationPriceText;
        public Text brakePowerPriceText;
        public Text tireTractionPriceText;
        public Text steerSensetivityPriceText;
        public Text buyCarText;
        public Text buyPaintText;
        public Text buyGlowText;
        public Text buyGlassTintText;
        public Text buyBrakeColorText;
        public Text buyRimColorText;
        public Text unlockLevelButtonText;
        public Text unlockLevelText;
        public Text upgradeConfirmText;
        public Text scoreText;
        public Text pointsText;
        public Text playerNameText;
        public Text questionText;
        public Text answer1Text;
        public Text answer2Text;
        public Text answer3Text;
        public Text answer4Text;
        public Text answer5Text;
        public Text confirmWindowText;
        public Text connectingWindowText;
        [Header("Sliders")]
        public Slider redSlider;
        public Slider blueSlider;
        public Slider greenSlider;
        public Slider redGlowSlider;
        public Slider blueGlowSlider;
        public Slider greenGlowSlider;
        public Slider redBrakeSlider;
        public Slider blueBrakeSlider;
        public Slider greenBrakeSlider;
        public Slider redGlassSlider;
        public Slider blueGlassSlider;
        public Slider greenGlassSlider;
        public Slider alphaGlassSlider;
        public Slider redRimSlider;
        public Slider blueRimSlider;
        public Slider greenRimSlider;
        public Slider topSpeedSlider;
        public Slider accelerationSlider;
        public Slider brakePowerSlider;
        public Slider tireTractionSlider;
        public Slider steerSensitivitySlidetr;
        [Header("Garage Menu Windows")]
        public GameObject startMenuWindow;
        public GameObject settingsMenuWindow;
        public GameObject mainMenuWindow;
        public GameObject customizeCarMenuWindow;
        public GameObject quitConfirmWindow;
        public GameObject buyCarConfirmWindow;
        public GameObject buyCarButton;
        public GameObject carConfirmWindow;
        public GameObject paintWindow;
        public GameObject rimColorWindow;
        public GameObject glassColorWindow;
        public GameObject brakeColorWindow;
        public GameObject paintConfirmWindow;
        public GameObject rimColorConfirmWindow;
        public GameObject glassColorConfirmWindow;
        public GameObject brakeColorConfirmWindow;
        public GameObject glowWindow;
        public GameObject glowConfirmWindow;
        public GameObject upgradesWindow;
        public GameObject upgradesConfirmWindow;
        public GameObject raceDetailsWindow;
        public GameObject unlockRaceConfirmWindow;
        public GameObject loadingWindow;
        public GameObject connectingWindow;
        public GameObject raceDetails;
        public GameObject racesWindow;
        public GameObject multiplayerWindow;
        public GameObject singlePlayerModeWindow;
        public GameObject multiplayerModeWindow;
        public GameObject LANWindow;
        public GameObject paintShopWindow;
        public GameObject garageCarSelectionWindow;
        public GameObject currencyAndCarTextWindow;
        public GameObject login;
        public InputField username;
        public InputField password;
        public Text status;
        public GameObject loginBtn;
        public GameObject questionsWindow;
        public GameObject questionsConfirmWindow;
        public GameObject questionsConfirmBtnYes;
        public GameObject questionsConfirmBtnNo;
        public GameObject questionsBankWindow;
        public GameObject questionsSheetWindow;
        public GameObject adminWindow;
        public GameObject publishedQuestionsWindow;
        public GameObject unpublishedQuestionsWindow;
        public GameObject playerHistoryWindow;
        public GameObject leaderboardWindow;
        public GameObject genericWindow;
        public Text genericWindowTitle;
        public GameObject genericWindowContent;
        public GameObject genericWindowAction_Yes;
        public GameObject genericWindowAction_No;
        [Header("Other")]
        public GameObject raceLockedIcon;
        public GameObject unlockRaceButton;
        public GameObject upgradeTopSpeedButton;
        public GameObject upgradeAccelerationButton;
        public GameObject upgradeBrakePowerButton;
        public GameObject upgradeTireTractionButton;
        public GameObject upgradeSteerSensitivityButton;
        public GameObject selectCarButton;
        public GameObject mainCaameraObject;
        public GameObject BackgroundImage;
        public Button[] carBodyColorButtons;
        public Button[] carGlassColorButtons;
        public Button[] carBrakeColorButtons;
        public Button[] carRimColorButtons;
        public Button[] carNeonColorButtons;
    }

    //public RaceData RGT_PlayerPrefs.raceData;
    //public PlayableVehicles RGT_PlayerPrefs.playableVehicles;
    //public PlayerPrefsData RGT_PlayerPrefs.playerPrefsData;
    //public AudioData RGT_PlayerPrefs.audioData;
    public GameObject lobbyManager;
    private static RG_GarageManager instance = null;
    //public Switch configureRaceSize;
    public Switch configureCarSize;
    [Tooltip("Enable an option to increase or decrease the amount of races available.")]
    public String openWorldName;
    public GarageSceneReference uI;
    public ParticleSystem[] sceneCarGlowLight;
    public GameObject[] sceneCarModel;
    public Image raceImage;
    private GameObject audioContainer;
    private GameObject emptyObject;
    private AudioSource garageAudioSource;
    private bool colorChange;
    private bool brakeColorChange;
    private bool glassColorChange;
    private bool rimColorChange;
    private bool glowChange;
    private bool upgradeChange;
    private bool quitConfirmIsOpen;
    private Color carColor;
    private int vehicleNumber;
    private int raceNumber;
    private int currency;
    private int score;
    [Range(0, 1f)]
    private float carAlpha;
    private int totalRaces;
    private string raceNameToLoad;
    private float cameraOffset;
    public float cameraOffsetMenus;
    public float cameraOffsetUpgrades;
    public float cameraOffsetRaces;
    public float cameraShiftSpeed;
    private Vector3 velocity = Vector3.zero;
    public ReflectionProbe reflectProbe;
    public RG_RotateAround cameraControl;
    int firstPrize;
    int secondPrize;
    int thirdPrize;
    float levelBonusTopSpeed;
    float levelBonusAcceleration;
    float levelBonusBrakePower;
    float levelBonusTireTraction;
    float levelBonusSteerSensitivity;
    //	public string currentVersion;
    private List<Text> answersTextList;
    public GameObject questionsBankContent;
    public GameObject questionsSheetContent;
    public GameObject playerHistoryContent;
    public GameObject leaderboardContent;
    public GameObject QuestionUIPrefab;
    public GameObject SelectableQuestionUIPrefab;
    public GameObject playerRecordUIPrefab;
    public GameObject genericPlayerRecordPrefab;
    public GameObject genericQuestionRecordPrefab;
    public GameObject genericSelectableQuestionRecordPrefab;
    private List<QuestionType> selectedQuestionToPublish;
    private List<QuestionType> selectedQuestionToRePublish;
    public static RG_GarageManager Instance()
    {
        if (instance != null)
        {
            return instance; // return the singleton if the instance has been setup
        }
        else
        { // otherwise return an error
            Debug.LogWarning("GM| GarageManager Not Initialized...");
        }
        return null;
    }
    void Awake()
    {
        if (instance == null)
        {
            instance = this;// if not, give it a reference to this class...
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
    }
    void Start()
    {


        if (GameSparksManager.Instance() != null && GameSparksManager.Instance().isPlayerAuthenticated())
        {

            UpdatePlayerDetails();

        }


        //		if (PlayerPrefs.GetString("CURRENTVERSION") != currentVersion)  {
        //			PlayerPrefs.DeleteAll();
        //			PlayerPrefs.SetString("CURRENTVERSION", currentVersion);
        //		}
        if (Application.isPlaying)
        {
            cameraOffset = cameraOffsetMenus;
            audioContainer = new GameObject();
            audioContainer.name = "Audio Container";
            audioContainer.transform.SetParent(gameObject.transform);
            Time.timeScale = 1.0f;
            AudioMusic();
            GetPlayerData();
            UpdateCurrency();
            for (int i = 0; i < RGT_PlayerPrefs.playableVehicles.numberOfCars; i++)
            {
                sceneCarModel[i].SetActive(false);
                if (i > 0 && RGT_PlayerPrefs.GetVehicleLock(i) == "UNLOCKED")
                {
                    RGT_PlayerPrefs.playableVehicles.carUnlocked[i] = true;
                }
                if (i == vehicleNumber)
                {
                    sceneCarModel[i].SetActive(true);
                    uI.topSpeedSlider.value = RGT_PlayerPrefs.playableVehicles.topSpeedLevel[i];
                    uI.accelerationSlider.value = RGT_PlayerPrefs.playableVehicles.torqueLevel[i];
                    uI.brakePowerSlider.value = RGT_PlayerPrefs.playableVehicles.brakeTorqueLevel[i];
                    uI.tireTractionSlider.value = RGT_PlayerPrefs.playableVehicles.tireTractionLevel[i];
                    uI.steerSensitivitySlidetr.value = RGT_PlayerPrefs.playableVehicles.steerSensitivityLevel[i];
                }
            }

            for (int i = 0; i < uI.carBodyColorButtons.Length; i++)
            {
                Color myColor = RGT_PlayerPrefs.playableVehicles.carBodyColorPreset[i];
                UnityEngine.UI.ColorBlock colorBlock = uI.carBodyColorButtons[i].colors;
                colorBlock.normalColor = myColor;
                colorBlock.highlightedColor = myColor;
                myColor.a = 0.4f;
                colorBlock.disabledColor = myColor;
                myColor.a = 0.25f;
                colorBlock.pressedColor = myColor;
                uI.carBodyColorButtons[i].colors = colorBlock;

                myColor = RGT_PlayerPrefs.playableVehicles.carGlassColorPreset[i];
                colorBlock = uI.carGlassColorButtons[i].colors;
                colorBlock.normalColor = myColor;
                colorBlock.highlightedColor = myColor;
                myColor.a = 0.4f;
                colorBlock.disabledColor = myColor;
                myColor.a = 0.25f;
                colorBlock.pressedColor = myColor;
                uI.carGlassColorButtons[i].colors = colorBlock;

                myColor = RGT_PlayerPrefs.playableVehicles.carBrakeColorPreset[i];
                colorBlock = uI.carBrakeColorButtons[i].colors;
                colorBlock.normalColor = myColor;
                colorBlock.highlightedColor = myColor;
                myColor.a = 0.4f;
                colorBlock.disabledColor = myColor;
                myColor.a = 0.25f;
                colorBlock.pressedColor = myColor;
                uI.carBrakeColorButtons[i].colors = colorBlock;

                myColor = RGT_PlayerPrefs.playableVehicles.carRimColorPreset[i];
                colorBlock = uI.carRimColorButtons[i].colors;
                colorBlock.normalColor = myColor;
                colorBlock.highlightedColor = myColor;
                myColor.a = 0.4f;
                colorBlock.disabledColor = myColor;
                myColor.a = 0.25f;
                colorBlock.pressedColor = myColor;
                uI.carRimColorButtons[i].colors = colorBlock;

                myColor = RGT_PlayerPrefs.playableVehicles.carGlassColorPreset[i];
                colorBlock = uI.carNeonColorButtons[i].colors;
                colorBlock.normalColor = myColor;
                colorBlock.highlightedColor = myColor;
                myColor.a = 0.4f;
                colorBlock.disabledColor = myColor;
                myColor.a = 0.25f;
                colorBlock.pressedColor = myColor;
                uI.carNeonColorButtons[i].colors = colorBlock;
            }
            uI.topSpeedPriceText.text = RGT_PlayerPrefs.playableVehicles.upgradeSpeedPrice.ToString("N0") + " Coins";
            uI.accelerationPriceText.text = RGT_PlayerPrefs.playableVehicles.upgradeAccelerationPrice.ToString("N0") + " Coins";
            uI.brakePowerPriceText.text = RGT_PlayerPrefs.playableVehicles.upgradeBrakesPrice.ToString("N0") + " Coins";
            uI.tireTractionPriceText.text = RGT_PlayerPrefs.playableVehicles.upgradeTiresPrice.ToString("N0") + " Coins";
            uI.steerSensetivityPriceText.text = RGT_PlayerPrefs.playableVehicles.upgradeSteeringPrice.ToString("N0") + " Coins";
            uI.buyGlowText.text = "Change Neon Light\nfor\n" + RGT_PlayerPrefs.playableVehicles.glowPrice.ToString("N0") + " Coins";
            uI.buyPaintText.text = "Paint this car\nfor\n" + RGT_PlayerPrefs.playableVehicles.paintPrice.ToString("N0") + " Coins";
            uI.buyBrakeColorText.text = "Change Brake Color\nfor\n" + RGT_PlayerPrefs.playableVehicles.brakeColorPrice.ToString("N0") + " Coins";
            uI.buyRimColorText.text = "Change Rim Color\nfor\n" + RGT_PlayerPrefs.playableVehicles.rimColorPrice.ToString("N0") + " Coins";
            uI.buyGlassTintText.text = "Change Glass Tint\nfor\n" + RGT_PlayerPrefs.playableVehicles.glassColorPrice.ToString("N0") + " Coins";
            uI.selectGlowText.text = RGT_PlayerPrefs.playableVehicles.glowPrice.ToString("N0") + " Coins";
            uI.selectPaintText.text = RGT_PlayerPrefs.playableVehicles.paintPrice.ToString("N0") + " Coins";
            uI.selectBrakeColorText.text = RGT_PlayerPrefs.playableVehicles.brakeColorPrice.ToString("N0") + " Coins";
            uI.selectRimColorText.text = RGT_PlayerPrefs.playableVehicles.rimColorPrice.ToString("N0") + " Coins";
            uI.selectGlassTintText.text = RGT_PlayerPrefs.playableVehicles.glassColorPrice.ToString("N0") + " Coins";
            uI.carNameText.text = RGT_PlayerPrefs.playableVehicles.vehicleNames[vehicleNumber];
            UpdateRaceDetails();
            if (RGT_PlayerPrefs.raceData.purchaseLevelUnlock == RaceData.Switch.Off)
            {
                uI.unlockRaceButton.SetActive(false);
            }
            else
            {
                uI.unlockRaceButton.SetActive(true);
            }
        }
        UpdateCar();
        carColor.a = 0.1f;
        carColor.r = RGT_PlayerPrefs.playerPrefsData.redGlowValues[vehicleNumber];
        carColor.b = RGT_PlayerPrefs.playerPrefsData.blueGlowValues[vehicleNumber];
        carColor.g = RGT_PlayerPrefs.playerPrefsData.greenGlowValues[vehicleNumber];



        answersTextList = new List<Text>();
        answersTextList.Add(uI.answer1Text);
        answersTextList.Add(uI.answer2Text);
        answersTextList.Add(uI.answer3Text);
        answersTextList.Add(uI.answer4Text);
        answersTextList.Add(uI.answer5Text);

        selectedQuestionToPublish = new List<QuestionType>();
        selectedQuestionToRePublish = new List<QuestionType>();
    }

    // Called on Scene start to load data from PlayerPrefs and Scriptable Objects
    public void GetPlayerData()
    {
        for (int i = 0; i < RGT_PlayerPrefs.playableVehicles.numberOfCars; i++)
        {
            RGT_PlayerPrefs.playableVehicles.topSpeedLevel[i] = RGT_PlayerPrefs.GetVehicleTopSpeedLevel(i);
            RGT_PlayerPrefs.playableVehicles.torqueLevel[i] = RGT_PlayerPrefs.GetVehicleAccelerationLevel(i);
            RGT_PlayerPrefs.playableVehicles.brakeTorqueLevel[i] = RGT_PlayerPrefs.GetVehicleBrakePowerLevel(i);
            RGT_PlayerPrefs.playableVehicles.tireTractionLevel[i] = RGT_PlayerPrefs.GetVehicleTireTractionLevel(i);
            RGT_PlayerPrefs.playableVehicles.steerSensitivityLevel[i] = RGT_PlayerPrefs.GetVehicleSteerSensitivityLevel(i);
            //get vehicle body color values
            RGT_PlayerPrefs.playerPrefsData.redValues[i] = RGT_PlayerPrefs.GetVehicleBodyColor("Red", i, RGT_PlayerPrefs.playableVehicles.defaultBodyColors[i].r);
            RGT_PlayerPrefs.playerPrefsData.blueValues[i] = RGT_PlayerPrefs.GetVehicleBodyColor("Blue", i, RGT_PlayerPrefs.playableVehicles.defaultBodyColors[i].b);
            RGT_PlayerPrefs.playerPrefsData.greenValues[i] = RGT_PlayerPrefs.GetVehicleBodyColor("Green", i, RGT_PlayerPrefs.playableVehicles.defaultBodyColors[i].g);
            //get vehicle neon light color values
            RGT_PlayerPrefs.playerPrefsData.redGlowValues[i] = RGT_PlayerPrefs.GetVehicleNeonLightColor("Red", i, RGT_PlayerPrefs.playableVehicles.defaultNeonColors[i].r);
            RGT_PlayerPrefs.playerPrefsData.blueGlowValues[i] = RGT_PlayerPrefs.GetVehicleNeonLightColor("Blue", i, RGT_PlayerPrefs.playableVehicles.defaultNeonColors[i].b);
            RGT_PlayerPrefs.playerPrefsData.greenGlowValues[i] = RGT_PlayerPrefs.GetVehicleNeonLightColor("Green", i, RGT_PlayerPrefs.playableVehicles.defaultNeonColors[i].g);
            //get vehicle brake color values
            RGT_PlayerPrefs.playerPrefsData.redBrakeValues[i] = RGT_PlayerPrefs.GetVehicleBrakeColor("Red", i, RGT_PlayerPrefs.playableVehicles.defaultBrakeColors[i].r);
            RGT_PlayerPrefs.playerPrefsData.blueBrakeValues[i] = RGT_PlayerPrefs.GetVehicleBrakeColor("Blue", i, RGT_PlayerPrefs.playableVehicles.defaultBrakeColors[i].b);
            RGT_PlayerPrefs.playerPrefsData.greenBrakeValues[i] = RGT_PlayerPrefs.GetVehicleBrakeColor("Green", i, RGT_PlayerPrefs.playableVehicles.defaultBrakeColors[i].g);
            //get vehicle rim color values
            RGT_PlayerPrefs.playerPrefsData.redRimValues[i] = RGT_PlayerPrefs.GetVehicleRimColor("Red", i, RGT_PlayerPrefs.playableVehicles.defaultRimColors[i].r);
            RGT_PlayerPrefs.playerPrefsData.blueRimValues[i] = RGT_PlayerPrefs.GetVehicleRimColor("Blue", i, RGT_PlayerPrefs.playableVehicles.defaultRimColors[i].b);
            RGT_PlayerPrefs.playerPrefsData.greenRimValues[i] = RGT_PlayerPrefs.GetVehicleRimColor("Green", i, RGT_PlayerPrefs.playableVehicles.defaultRimColors[i].g);
            //get vehicle glass color values
            RGT_PlayerPrefs.playerPrefsData.redGlassValues[i] = RGT_PlayerPrefs.GetVehicleGlassColor("Red", i, RGT_PlayerPrefs.playableVehicles.defaultGlassColors[i].r);
            RGT_PlayerPrefs.playerPrefsData.blueGlassValues[i] = RGT_PlayerPrefs.GetVehicleGlassColor("Blue", i, RGT_PlayerPrefs.playableVehicles.defaultGlassColors[i].b);
            RGT_PlayerPrefs.playerPrefsData.greenGlassValues[i] = RGT_PlayerPrefs.GetVehicleGlassColor("Green", i, RGT_PlayerPrefs.playableVehicles.defaultGlassColors[i].g);
            RGT_PlayerPrefs.playerPrefsData.alphaGlassValues[i] = RGT_PlayerPrefs.GetVehicleGlassColor("Alpha", i, RGT_PlayerPrefs.playableVehicles.defaultGlassColors[i].a);

            carColor.a = carAlpha;
            carColor.r = RGT_PlayerPrefs.playerPrefsData.redValues[i];
            carColor.b = RGT_PlayerPrefs.playerPrefsData.blueValues[i];
            carColor.g = RGT_PlayerPrefs.playerPrefsData.greenValues[i];
            RGT_PlayerPrefs.playableVehicles.carMaterial[i].color = carColor;
            carColor.a = 0.1f;
            carColor.r = RGT_PlayerPrefs.playerPrefsData.redGlowValues[i];
            carColor.b = RGT_PlayerPrefs.playerPrefsData.blueGlowValues[i];
            carColor.g = RGT_PlayerPrefs.playerPrefsData.greenGlowValues[i];
            //var main = RGT_PlayerPrefs.playableVehicles.carGlowLight[i].main;
            //main.startColor = carColor;


            //var main0 = sceneCarGlowLight[i].main;
            //main0.startColor = carColor;


            carColor.a = carAlpha;
            carColor.r = RGT_PlayerPrefs.playerPrefsData.redBrakeValues[i];
            carColor.b = RGT_PlayerPrefs.playerPrefsData.blueBrakeValues[i];
            carColor.g = RGT_PlayerPrefs.playerPrefsData.greenBrakeValues[i];
            RGT_PlayerPrefs.playableVehicles.brakeMaterial[i].color = carColor;
            carColor.a = carAlpha;
            carColor.r = RGT_PlayerPrefs.playerPrefsData.redRimValues[i];
            carColor.b = RGT_PlayerPrefs.playerPrefsData.blueRimValues[i];
            carColor.g = RGT_PlayerPrefs.playerPrefsData.greenRimValues[i];
            RGT_PlayerPrefs.playableVehicles.rimMaterial[i].color = carColor;
            carColor.a = RGT_PlayerPrefs.playerPrefsData.alphaGlassValues[i];
            carColor.r = RGT_PlayerPrefs.playerPrefsData.redGlassValues[i];
            carColor.b = RGT_PlayerPrefs.playerPrefsData.blueGlassValues[i];
            carColor.g = RGT_PlayerPrefs.playerPrefsData.greenGlassValues[i];
            RGT_PlayerPrefs.playableVehicles.glassMaterial[i].color = carColor;

            //var main1 = RGT_PlayerPrefs.playableVehicles.carGlowLight[i].main;
            //main1.startColor = carColor;
            //sceneCarGlowLight[i].gameObject.SetActive(false);
            //sceneCarGlowLight[i].gameObject.SetActive(true);

        }
        raceNumber = RGT_PlayerPrefs.GetRaceNumber();
        //get race laps
        for (int i = 0; i < RGT_PlayerPrefs.raceData.numberOfRaces; i++)
        {
            RGT_PlayerPrefs.GetRaceLaps(i, RGT_PlayerPrefs.raceData.raceLaps[raceNumber]);
        }
        //get vehicle number
        vehicleNumber = RGT_PlayerPrefs.GetVehicleNumber();
        RGT_PlayerPrefs.playableVehicles.currentVehicleNumber = vehicleNumber;
        //get currency
        currency = RGT_PlayerPrefs.GetCurrency();
        if (currency == 0)
        {
            //set starting currency
            currency = RGT_PlayerPrefs.playableVehicles.startingCurrency;
            RGT_PlayerPrefs.SetCurrency(RGT_PlayerPrefs.playableVehicles.startingCurrency);
        }
        //set number of races
        RGT_PlayerPrefs.SetNumberOfRaces(RGT_PlayerPrefs.raceData.numberOfRaces);
        for (int i = 1; i < RGT_PlayerPrefs.raceData.numberOfRaces; i++)
        {
            //set race names
            RGT_PlayerPrefs.SetRaceName(i, RGT_PlayerPrefs.raceData.raceNames[i]);
            if (RGT_PlayerPrefs.raceData.raceLocked[i])
            {
                //get race locks
                if (RGT_PlayerPrefs.GetRaceLock(RGT_PlayerPrefs.raceData.raceNames[i]) == "UNLOCKED")
                {
                    RGT_PlayerPrefs.raceData.raceLocked[i] = false;
                }
                else
                {
                    RGT_PlayerPrefs.raceData.raceLocked[i] = true;
                }
            }
        }
        //raceDetails[raceNumber].bestPosition = PlayerPrefs.GetInt ("Best Position" + raceNumber.ToString(), 8);
        //raceDetails[raceNumber].bestTime = PlayerPrefs.GetFloat ("Best Time" + raceNumber.ToString(), 9999.99f);
        //raceDetails[raceNumber].bestLapTime = PlayerPrefs.GetFloat ("Best Lap Time" + raceNumber.ToString(), 9999.99f);

    }

    // Here we check to see if the player is in a car part color modification menu and update the color changes in real time as they adjust sliders
    public void Update()
    {
        if (Application.isPlaying)
        {
            if (RGT_PlayerPrefs.audioData.music.Length > 0)
            {
                if (!garageAudioSource.isPlaying) PlayNextAudioTrack();
            }
            if (colorChange) CarColor();
            if (brakeColorChange) BrakeColor();
            if (rimColorChange) RimColor();
            if (glassColorChange) GlassColor();
            if (glowChange) GlowColor();
        }
        Vector3 pos = uI.mainCaameraObject.transform.localPosition;
        pos.x = cameraOffset; ;
        uI.mainCaameraObject.transform.localPosition = Vector3.SmoothDamp(uI.mainCaameraObject.transform.localPosition, pos, ref velocity, cameraShiftSpeed);
    }

    // These methods are used for the main menu
    #region MainMenu Methods
    // Loads garage menu
    public void Button_Play()
    {
        if (uI.startMenuWindow.activeInHierarchy)
        {
            DisableBG();
            uI.startMenuWindow.SetActive(false);
            uI.mainMenuWindow.SetActive(true);
            uI.currencyAndCarTextWindow.SetActive(true);
            AudioMenuSelect();
        }
        else
        {
            EnableBG();
            if (RGT_PlayerPrefs.GetVehicleLock(vehicleNumber) != "UNLOCKED")
            {
                sceneCarModel[vehicleNumber].SetActive(false);
                vehicleNumber = RGT_PlayerPrefs.playableVehicles.currentVehicleNumber;
                //RGT_PlayerPrefs.playableVehicles.currentVehicleNumber = vehicleNumber;
                uI.carNameText.text = RGT_PlayerPrefs.playableVehicles.vehicleNames[vehicleNumber];
                sceneCarModel[vehicleNumber].SetActive(true);
            }
            uI.currencyAndCarTextWindow.SetActive(false);
            uI.buyCarConfirmWindow.SetActive(false);
            for (int i = 0; i < RGT_PlayerPrefs.playableVehicles.numberOfCars; i++)
            {
                sceneCarModel[i].SetActive(false);
                if (i == vehicleNumber)
                {
                    sceneCarModel[i].SetActive(true);
                }
            }
            uI.mainMenuWindow.SetActive(false);
            uI.startMenuWindow.SetActive(true);
            uI.topSpeedSlider.value = RGT_PlayerPrefs.playableVehicles.topSpeedLevel[vehicleNumber];
            uI.accelerationSlider.value = RGT_PlayerPrefs.playableVehicles.torqueLevel[vehicleNumber];
            uI.brakePowerSlider.value = RGT_PlayerPrefs.playableVehicles.brakeTorqueLevel[vehicleNumber];
            uI.tireTractionSlider.value = RGT_PlayerPrefs.playableVehicles.tireTractionLevel[vehicleNumber];
            uI.steerSensitivitySlidetr.value = RGT_PlayerPrefs.playableVehicles.steerSensitivityLevel[vehicleNumber];
            AudioMenuBack();
        }
        UpdateCar();
    }

    // Loads the options menu
    public void Button_Settings()
    {
        if (uI.settingsMenuWindow.activeInHierarchy)
        {
            uI.mainMenuWindow.SetActive(true);
            uI.settingsMenuWindow.SetActive(false);
            AudioMenuBack();
        }
        else
        {
            uI.settingsMenuWindow.SetActive(true);
            uI.mainMenuWindow.SetActive(false);
            AudioMenuSelect();
        }
    }

    // Prompts user with a quit confirm window
    public void Button_QuitGame()
    {
        if (quitConfirmIsOpen)
        {
            quitConfirmIsOpen = false;
            uI.quitConfirmWindow.SetActive(false);
            uI.startMenuWindow.SetActive(true);
        }
        else
        {
            quitConfirmIsOpen = true;
            uI.quitConfirmWindow.SetActive(true);
            uI.startMenuWindow.SetActive(false);
        }
    }

    //Closes the quit confirm window
    public void Button_QuitConfirm()
    {
        GS.ShutDown();
        GS.Disconnect();
        SceneManager.LoadScene("ExitScene");
        //Application.Quit();
    }

    public void Button_Review()
    {
        Application.OpenURL(uI.reviewAddress);
    }

    public void Button_More()
    {
        Application.OpenURL(uI.developerAddress);
    }

    public void Button_MenuToQuestionsConfirmation()
    {
        if (uI.questionsConfirmWindow.activeInHierarchy)
        {
            uI.mainMenuWindow.SetActive(true);
            uI.questionsConfirmWindow.SetActive(false);
            AudioMenuBack();
        }
        else
        {
            FormatQuestionConfirmationWindow();
            uI.questionsConfirmWindow.SetActive(true);
            uI.mainMenuWindow.SetActive(false);
            AudioMenuSelect();
        }
    }

    public void DisableBG()
    {
        uI.BackgroundImage.SetActive(false);
    }

    public void EnableBG()
    {
        uI.BackgroundImage.SetActive(true);
    }

    public void Button_Assessment()
    {
        if (GameSparksManager.Instance().TeamName == "AdminEntyvio" || GameSparksManager.Instance().TeamName == "AdminAdcetris")
        {
            if (uI.adminWindow.activeInHierarchy)
            {
                uI.mainMenuWindow.SetActive(true);
                uI.adminWindow.SetActive(false);
                AudioMenuBack();
            }
            else
            {
                uI.adminWindow.SetActive(true);
                uI.mainMenuWindow.SetActive(false);
                AudioMenuSelect();
            }
        }
        else
        {
            //Button_MenuToQuestionsConfirmation();
        }
    }
    #endregion

    // These methods check player currency and prompt player with a purchase confirmation window if the player has enough currency
    #region SelectItemToPurchase Methods

    public void Button_Garage()
    {
        if (uI.garageCarSelectionWindow.activeInHierarchy)
        {
            cameraOffset = cameraOffsetMenus;
            uI.garageCarSelectionWindow.SetActive(false);
            uI.mainMenuWindow.SetActive(true);
        }
        else
        {
            cameraOffset = 0;
            uI.garageCarSelectionWindow.SetActive(true);
            uI.mainMenuWindow.SetActive(false);
        }
    }

    public void Button_PaintShop()
    {
        if (uI.paintShopWindow.activeInHierarchy)
        {
            cameraOffset = cameraOffsetMenus;
            uI.paintShopWindow.SetActive(false);
            uI.customizeCarMenuWindow.SetActive(true);
        }
        else
        {
            uI.paintShopWindow.SetActive(true);
            uI.customizeCarMenuWindow.SetActive(false);
        }
    }

    public void Button_Upgrades()
    {
        if (upgradeChange)
        {
            cameraOffset = cameraOffsetMenus;
            upgradeChange = false;
            uI.upgradesWindow.SetActive(false);
            uI.upgradesConfirmWindow.SetActive(false);
            uI.customizeCarMenuWindow.SetActive(true);
        }
        else
        {
            cameraOffset = cameraOffsetUpgrades;
            uI.customizeCarMenuWindow.SetActive(false);
            uI.upgradesWindow.SetActive(true);
            AudioMenuSelect();
            upgradeChange = true;
        }
    }

    public void Button_MultiplayerMenu()
    {
        if (uI.multiplayerModeWindow.activeInHierarchy)
        {
            uI.multiplayerModeWindow.SetActive(false);
            uI.mainMenuWindow.SetActive(true);
        }
        else
        {
            uI.mainMenuWindow.SetActive(false);
            uI.multiplayerModeWindow.SetActive(true);
        }
    }

    public void Button_SinglePlayerMenu()
    {
        if (uI.singlePlayerModeWindow.activeInHierarchy)
        {
            uI.singlePlayerModeWindow.SetActive(false);
            uI.mainMenuWindow.SetActive(true);
        }
        else
        {
            uI.mainMenuWindow.SetActive(false);
            uI.singlePlayerModeWindow.SetActive(true);
        }
    }

    public void Button_BuyCar()
    {
        uI.buyCarText.text = "Buy " + RGT_PlayerPrefs.playableVehicles.vehicleNames[vehicleNumber].ToString() + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.price[vehicleNumber].ToString("N0") + " Coins";
        if (currency >= RGT_PlayerPrefs.playableVehicles.price[vehicleNumber])
            uI.carConfirmWindow.SetActive(true);
        AudioMenuSelect();
    }

    public void Button_BodyColorChange(int buttonNumber)
    {
        uI.redSlider.value = RGT_PlayerPrefs.playableVehicles.carBodyColorPreset[buttonNumber].r;
        uI.blueSlider.value = RGT_PlayerPrefs.playableVehicles.carBodyColorPreset[buttonNumber].b;
        uI.greenSlider.value = RGT_PlayerPrefs.playableVehicles.carBodyColorPreset[buttonNumber].g;
        FormatSelectedColors();
        uI.carBodyColorButtons[buttonNumber].interactable = false;
    }

    public void Button_GlassColorChange(int buttonNumber)
    {
        uI.redGlassSlider.value = RGT_PlayerPrefs.playableVehicles.carGlassColorPreset[buttonNumber].r;
        uI.blueGlassSlider.value = RGT_PlayerPrefs.playableVehicles.carGlassColorPreset[buttonNumber].b;
        uI.greenGlassSlider.value = RGT_PlayerPrefs.playableVehicles.carGlassColorPreset[buttonNumber].g;
        FormatSelectedColors();
        uI.carGlassColorButtons[buttonNumber].interactable = false;
    }

    public void Button_BrakeColorChange(int buttonNumber)
    {
        uI.redBrakeSlider.value = RGT_PlayerPrefs.playableVehicles.carBrakeColorPreset[buttonNumber].r;
        uI.blueBrakeSlider.value = RGT_PlayerPrefs.playableVehicles.carBrakeColorPreset[buttonNumber].b;
        uI.greenBrakeSlider.value = RGT_PlayerPrefs.playableVehicles.carBrakeColorPreset[buttonNumber].g;
        FormatSelectedColors();
        uI.carBrakeColorButtons[buttonNumber].interactable = false;
    }

    public void Button_RimColorChange(int buttonNumber)
    {
        uI.redRimSlider.value = RGT_PlayerPrefs.playableVehicles.carRimColorPreset[buttonNumber].r;
        uI.blueRimSlider.value = RGT_PlayerPrefs.playableVehicles.carRimColorPreset[buttonNumber].b;
        uI.greenRimSlider.value = RGT_PlayerPrefs.playableVehicles.carRimColorPreset[buttonNumber].g;
        FormatSelectedColors();
        uI.carRimColorButtons[buttonNumber].interactable = false;
    }

    public void Button_NeonColorChange(int buttonNumber)
    {
        uI.redGlowSlider.value = RGT_PlayerPrefs.playableVehicles.carNeonColorPreset[buttonNumber].r;
        uI.blueGlowSlider.value = RGT_PlayerPrefs.playableVehicles.carNeonColorPreset[buttonNumber].b;
        uI.greenGlowSlider.value = RGT_PlayerPrefs.playableVehicles.carNeonColorPreset[buttonNumber].g;
        FormatSelectedColors();
        uI.carNeonColorButtons[buttonNumber].interactable = false;
    }

    // Loads the Change Paint Color menu for the selected car
    public void Button_BodyColor()
    {
        if (colorChange)
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetMenus;
            colorChange = false;
            uI.paintWindow.SetActive(false);
            uI.paintConfirmWindow.SetActive(false);
            uI.paintShopWindow.SetActive(true);
            uI.redSlider.value = RGT_PlayerPrefs.playerPrefsData.redValues[vehicleNumber];
            uI.blueSlider.value = RGT_PlayerPrefs.playerPrefsData.blueValues[vehicleNumber];
            uI.greenSlider.value = RGT_PlayerPrefs.playerPrefsData.greenValues[vehicleNumber];
            CarColor();
        }
        else
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetUpgrades;
            uI.paintShopWindow.SetActive(false);
            uI.paintWindow.SetActive(true);
            AudioMenuSelect();
            colorChange = true;
            uI.redSlider.value = RGT_PlayerPrefs.playerPrefsData.redValues[vehicleNumber];
            uI.blueSlider.value = RGT_PlayerPrefs.playerPrefsData.blueValues[vehicleNumber];
            uI.greenSlider.value = RGT_PlayerPrefs.playerPrefsData.greenValues[vehicleNumber];
            CarColor();
        }
    }

    // Loads the Change Glass Color menu for the selected car
    public void Button_GlassColor()
    {
        if (glassColorChange)
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetMenus;
            glassColorChange = false;
            uI.glassColorWindow.SetActive(false);
            uI.glassColorConfirmWindow.SetActive(false);
            uI.paintShopWindow.SetActive(true);
            uI.redGlassSlider.value = RGT_PlayerPrefs.playerPrefsData.redGlassValues[vehicleNumber];
            uI.blueGlassSlider.value = RGT_PlayerPrefs.playerPrefsData.blueGlassValues[vehicleNumber];
            uI.greenGlassSlider.value = RGT_PlayerPrefs.playerPrefsData.greenGlassValues[vehicleNumber];
            uI.alphaGlassSlider.value = RGT_PlayerPrefs.playerPrefsData.alphaGlassValues[vehicleNumber];
            GlassColor();
        }
        else
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetUpgrades;
            uI.paintShopWindow.SetActive(false);
            uI.glassColorWindow.SetActive(true);
            AudioMenuSelect();
            glassColorChange = true;
            uI.redGlassSlider.value = RGT_PlayerPrefs.playerPrefsData.redGlassValues[vehicleNumber];
            uI.blueGlassSlider.value = RGT_PlayerPrefs.playerPrefsData.blueGlassValues[vehicleNumber];
            uI.greenGlassSlider.value = RGT_PlayerPrefs.playerPrefsData.greenGlassValues[vehicleNumber];
            uI.alphaGlassSlider.value = RGT_PlayerPrefs.playerPrefsData.alphaGlassValues[vehicleNumber];
            GlassColor();
        }
    }

    // Loads the Change Brake Color menu for the selected car
    public void Button_BrakeColor()
    {
        if (brakeColorChange)
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetMenus;
            brakeColorChange = false;
            uI.brakeColorWindow.SetActive(false);
            uI.brakeColorConfirmWindow.SetActive(false);
            uI.paintShopWindow.SetActive(true);
            uI.redBrakeSlider.value = RGT_PlayerPrefs.playerPrefsData.redBrakeValues[vehicleNumber];
            uI.blueBrakeSlider.value = RGT_PlayerPrefs.playerPrefsData.blueBrakeValues[vehicleNumber];
            uI.greenBrakeSlider.value = RGT_PlayerPrefs.playerPrefsData.greenBrakeValues[vehicleNumber];
            BrakeColor();
        }
        else
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetUpgrades;
            uI.paintShopWindow.SetActive(false);
            uI.brakeColorWindow.SetActive(true);
            AudioMenuSelect();
            brakeColorChange = true;
            uI.redBrakeSlider.value = RGT_PlayerPrefs.playerPrefsData.redBrakeValues[vehicleNumber];
            uI.blueBrakeSlider.value = RGT_PlayerPrefs.playerPrefsData.blueBrakeValues[vehicleNumber];
            uI.greenBrakeSlider.value = RGT_PlayerPrefs.playerPrefsData.greenBrakeValues[vehicleNumber];
            BrakeColor();
        }
    }

    // Loads the Change Rim Color menu for the selected car
    public void Button_RimColor()
    {
        if (rimColorChange)
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetMenus;
            rimColorChange = false;
            uI.rimColorWindow.SetActive(false);
            uI.rimColorConfirmWindow.SetActive(false);
            uI.paintShopWindow.SetActive(true);
            uI.redRimSlider.value = RGT_PlayerPrefs.playerPrefsData.redRimValues[vehicleNumber];
            uI.blueRimSlider.value = RGT_PlayerPrefs.playerPrefsData.blueRimValues[vehicleNumber];
            uI.greenRimSlider.value = RGT_PlayerPrefs.playerPrefsData.greenRimValues[vehicleNumber];
            RimColor();
        }
        else
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetUpgrades;
            uI.paintShopWindow.SetActive(false);
            uI.rimColorWindow.SetActive(true);
            AudioMenuSelect();
            rimColorChange = true;
            uI.redRimSlider.value = RGT_PlayerPrefs.playerPrefsData.redRimValues[vehicleNumber];
            uI.blueRimSlider.value = RGT_PlayerPrefs.playerPrefsData.blueRimValues[vehicleNumber];
            uI.greenRimSlider.value = RGT_PlayerPrefs.playerPrefsData.greenRimValues[vehicleNumber];
            RimColor();
        }
    }

    // Loads the Change Neon Glow menu for the selected car
    public void Button_NeonLightColor()
    {
        if (glowChange)
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetMenus;
            glowChange = false;
            uI.glowWindow.SetActive(false);
            uI.glowConfirmWindow.SetActive(false);
            uI.paintShopWindow.SetActive(true);
            uI.redGlowSlider.value = RGT_PlayerPrefs.playerPrefsData.redGlowValues[vehicleNumber];
            uI.blueGlowSlider.value = RGT_PlayerPrefs.playerPrefsData.blueGlowValues[vehicleNumber];
            uI.greenGlowSlider.value = RGT_PlayerPrefs.playerPrefsData.greenGlowValues[vehicleNumber];
            GlowColor();
        }
        else
        {
            cameraControl.CanControl();
            cameraOffset = cameraOffsetUpgrades;
            uI.paintShopWindow.SetActive(false);
            uI.glowWindow.SetActive(true);
            AudioMenuSelect();
            glowChange = true;
            uI.redGlowSlider.value = RGT_PlayerPrefs.playerPrefsData.redGlowValues[vehicleNumber];
            uI.blueGlowSlider.value = RGT_PlayerPrefs.playerPrefsData.blueGlowValues[vehicleNumber];
            uI.greenGlowSlider.value = RGT_PlayerPrefs.playerPrefsData.greenGlowValues[vehicleNumber];
            GlowColor();
        }
    }

    public void SelectGlow()
    {
        AudioMenuSelect();
        if (currency >= RGT_PlayerPrefs.playableVehicles.glowPrice) uI.glowConfirmWindow.SetActive(true);
    }

    public void SelectPaint()
    {
        AudioMenuSelect();
        if (currency >= RGT_PlayerPrefs.playableVehicles.paintPrice) uI.paintConfirmWindow.SetActive(true);
    }

    public void SelectBrakeColor()
    {
        AudioMenuSelect();
        if (currency >= RGT_PlayerPrefs.playableVehicles.brakeColorPrice) uI.brakeColorConfirmWindow.SetActive(true);
    }

    public void SelectRimColor()
    {
        AudioMenuSelect();
        if (currency >= RGT_PlayerPrefs.playableVehicles.rimColorPrice) uI.rimColorConfirmWindow.SetActive(true);
    }

    public void SelectGlassColor()
    {
        AudioMenuSelect();
        if (currency >= RGT_PlayerPrefs.playableVehicles.glassColorPrice) uI.glassColorConfirmWindow.SetActive(true);
    }
    #endregion

    // These methods are used to confirm a purchase and update PlayerPrefs data and other components with the changes
    #region AcceptPurchases Methods

    public void AcceptPurchasePaint()
    {
        AudioMenuSelect();
        //set currency
        RGT_PlayerPrefs.SetCurrency(currency - RGT_PlayerPrefs.playableVehicles.paintPrice);
        //set vehicle body color values
        RGT_PlayerPrefs.SetVehicleBodyColor("Red", vehicleNumber, uI.redSlider.value);
        RGT_PlayerPrefs.SetVehicleBodyColor("Blue", vehicleNumber, uI.blueSlider.value);
        RGT_PlayerPrefs.SetVehicleBodyColor("Green", vehicleNumber, uI.greenSlider.value);
        RGT_PlayerPrefs.playerPrefsData.redValues[vehicleNumber] = uI.redSlider.value;
        RGT_PlayerPrefs.playerPrefsData.blueValues[vehicleNumber] = uI.blueSlider.value;
        RGT_PlayerPrefs.playerPrefsData.greenValues[vehicleNumber] = uI.greenSlider.value;
        uI.paintConfirmWindow.SetActive(false);
        UpdateCurrency();
        Button_BodyColor();
    }

    public void AcceptPurchaseGlassColor()
    {
        AudioMenuSelect();
        //set currency
        RGT_PlayerPrefs.SetCurrency(currency - RGT_PlayerPrefs.playableVehicles.glassColorPrice);
        //set vehicle glass color values
        RGT_PlayerPrefs.SetVehicleGlassColor("Red", vehicleNumber, uI.redGlassSlider.value);
        RGT_PlayerPrefs.SetVehicleGlassColor("Blue", vehicleNumber, uI.blueGlassSlider.value);
        RGT_PlayerPrefs.SetVehicleGlassColor("Green", vehicleNumber, uI.greenGlassSlider.value);
        RGT_PlayerPrefs.SetVehicleGlassColor("Alpha", vehicleNumber, uI.alphaGlassSlider.value);
        RGT_PlayerPrefs.playerPrefsData.redGlassValues[vehicleNumber] = uI.redGlassSlider.value;
        RGT_PlayerPrefs.playerPrefsData.blueGlassValues[vehicleNumber] = uI.blueGlassSlider.value;
        RGT_PlayerPrefs.playerPrefsData.greenGlassValues[vehicleNumber] = uI.greenGlassSlider.value;
        RGT_PlayerPrefs.playerPrefsData.alphaGlassValues[vehicleNumber] = uI.alphaGlassSlider.value;
        uI.glassColorConfirmWindow.SetActive(false);
        UpdateCurrency();
        Button_GlassColor();
    }

    public void AcceptPurchaseBrakeColor()
    {
        AudioMenuSelect();
        //set currency
        RGT_PlayerPrefs.SetCurrency(currency - RGT_PlayerPrefs.playableVehicles.brakeColorPrice);
        //set vehicle brake color values
        RGT_PlayerPrefs.SetVehicleBrakeColor("Red", vehicleNumber, uI.redBrakeSlider.value);
        RGT_PlayerPrefs.SetVehicleBrakeColor("Blue", vehicleNumber, uI.blueBrakeSlider.value);
        RGT_PlayerPrefs.SetVehicleBrakeColor("Green", vehicleNumber, uI.greenBrakeSlider.value);
        RGT_PlayerPrefs.playerPrefsData.redBrakeValues[vehicleNumber] = uI.redBrakeSlider.value;
        RGT_PlayerPrefs.playerPrefsData.blueBrakeValues[vehicleNumber] = uI.blueBrakeSlider.value;
        RGT_PlayerPrefs.playerPrefsData.greenBrakeValues[vehicleNumber] = uI.greenBrakeSlider.value;
        uI.brakeColorConfirmWindow.SetActive(false);
        UpdateCurrency();
        Button_BrakeColor();
    }

    public void AcceptPurchaseRimColor()
    {
        AudioMenuSelect();
        //set currency
        RGT_PlayerPrefs.SetCurrency(currency - RGT_PlayerPrefs.playableVehicles.rimColorPrice);
        //set vehicle rim color values
        RGT_PlayerPrefs.SetVehicleRimColor("Red", vehicleNumber, uI.redRimSlider.value);
        RGT_PlayerPrefs.SetVehicleRimColor("Blue", vehicleNumber, uI.blueRimSlider.value);
        RGT_PlayerPrefs.SetVehicleRimColor("Green", vehicleNumber, uI.greenRimSlider.value);
        RGT_PlayerPrefs.playerPrefsData.redRimValues[vehicleNumber] = uI.redRimSlider.value;
        RGT_PlayerPrefs.playerPrefsData.blueRimValues[vehicleNumber] = uI.blueRimSlider.value;
        RGT_PlayerPrefs.playerPrefsData.greenRimValues[vehicleNumber] = uI.greenRimSlider.value;
        uI.rimColorConfirmWindow.SetActive(false);
        UpdateCurrency();
        Button_RimColor();
    }

    public void AcceptPurchaseGlow()
    {
        AudioMenuSelect();
        //set currency
        RGT_PlayerPrefs.SetCurrency(currency - RGT_PlayerPrefs.playableVehicles.glowPrice);
        //set vehicle neon light color values
        RGT_PlayerPrefs.SetVehicleNeonLightColor("Red", vehicleNumber, uI.redGlowSlider.value);
        RGT_PlayerPrefs.SetVehicleNeonLightColor("Blue", vehicleNumber, uI.blueGlowSlider.value);
        RGT_PlayerPrefs.SetVehicleNeonLightColor("Green", vehicleNumber, uI.greenGlowSlider.value);
        RGT_PlayerPrefs.playerPrefsData.redGlowValues[vehicleNumber] = uI.redGlowSlider.value;
        RGT_PlayerPrefs.playerPrefsData.blueGlowValues[vehicleNumber] = uI.blueGlowSlider.value;
        RGT_PlayerPrefs.playerPrefsData.greenGlowValues[vehicleNumber] = uI.greenGlowSlider.value;
        uI.glowConfirmWindow.SetActive(false);
        var main = RGT_PlayerPrefs.playableVehicles.carGlowLight[vehicleNumber].main;
        main.startColor = carColor;

        UpdateCurrency();
        Button_NeonLightColor();
    }

    public void Button_AcceptUpgrade()
    {
        if (uI.upgradeConfirmText.text == "Upgrade " + "Top Speed" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeSpeedPrice.ToString("N0") + " Coins")
        {
            currency -= RGT_PlayerPrefs.playableVehicles.upgradeSpeedPrice;
            RGT_PlayerPrefs.playableVehicles.topSpeedLevel[vehicleNumber] += 1;
            RGT_PlayerPrefs.SetVehicleTopSpeedLevel(vehicleNumber, RGT_PlayerPrefs.playableVehicles.topSpeedLevel[vehicleNumber]);
        }
        if (uI.upgradeConfirmText.text == "Upgrade " + "Acceleration" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeAccelerationPrice.ToString("N0") + " Coins")
        {
            currency -= RGT_PlayerPrefs.playableVehicles.upgradeAccelerationPrice;
            RGT_PlayerPrefs.playableVehicles.torqueLevel[vehicleNumber] += 1;
            RGT_PlayerPrefs.SetVehicleAccelerationLevel(vehicleNumber, RGT_PlayerPrefs.playableVehicles.torqueLevel[vehicleNumber]);
        }
        if (uI.upgradeConfirmText.text == "Upgrade " + "Brake Power" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeBrakesPrice.ToString("N0") + " Coins")
        {
            currency -= RGT_PlayerPrefs.playableVehicles.upgradeBrakesPrice;
            RGT_PlayerPrefs.playableVehicles.brakeTorqueLevel[vehicleNumber] += 1;
            RGT_PlayerPrefs.SetVehicleBrakePowerLevel(vehicleNumber, RGT_PlayerPrefs.playableVehicles.brakeTorqueLevel[vehicleNumber]);
        }
        if (uI.upgradeConfirmText.text == "Upgrade " + "Tire Traction" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeTiresPrice.ToString("N0") + " Coins")
        {
            currency -= RGT_PlayerPrefs.playableVehicles.upgradeTiresPrice;
            RGT_PlayerPrefs.playableVehicles.tireTractionLevel[vehicleNumber] += 1;
            RGT_PlayerPrefs.SetVehicleTireTractionLevel(vehicleNumber, RGT_PlayerPrefs.playableVehicles.tireTractionLevel[vehicleNumber]);
        }
        if (uI.upgradeConfirmText.text == "Upgrade " + "Steer Sensitivity" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeSteeringPrice.ToString("N0") + " Coins")
        {
            currency -= RGT_PlayerPrefs.playableVehicles.upgradeSteeringPrice;
            RGT_PlayerPrefs.playableVehicles.steerSensitivityLevel[vehicleNumber] += 1;
            RGT_PlayerPrefs.SetVehicleSteerSensitivityLevel(vehicleNumber, RGT_PlayerPrefs.playableVehicles.steerSensitivityLevel[vehicleNumber]);
        }
        uI.upgradesConfirmWindow.SetActive(false);
        //set currency
        RGT_PlayerPrefs.SetCurrency(currency);
        UpdateCar();
    }
    #endregion

    // These methods are used to change the color of a car part
    #region ColorChange Methods
    public void GlassColor()
    {
        carColor.a = uI.alphaGlassSlider.value;
        carColor.r = uI.redGlassSlider.value;
        carColor.b = uI.blueGlassSlider.value;
        carColor.g = uI.greenGlassSlider.value;
        RGT_PlayerPrefs.playableVehicles.glassMaterial[vehicleNumber].color = carColor;
    }

    public void BrakeColor()
    {
        carColor.a = carAlpha;
        carColor.r = uI.redBrakeSlider.value;
        carColor.b = uI.blueBrakeSlider.value;
        carColor.g = uI.greenBrakeSlider.value;
        RGT_PlayerPrefs.playableVehicles.brakeMaterial[vehicleNumber].color = carColor;
    }

    public void RimColor()
    {
        carColor.a = carAlpha;
        carColor.r = uI.redRimSlider.value;
        carColor.b = uI.blueRimSlider.value;
        carColor.g = uI.greenRimSlider.value;
        RGT_PlayerPrefs.playableVehicles.rimMaterial[vehicleNumber].color = carColor;
    }

    public void CarColor()
    {
        carColor.a = carAlpha;
        carColor.r = uI.redSlider.value;
        carColor.b = uI.blueSlider.value;
        carColor.g = uI.greenSlider.value;
        RGT_PlayerPrefs.playableVehicles.carMaterial[vehicleNumber].color = carColor;
    }

    public void GlowColor()
    {
        if (carColor.g != uI.greenGlowSlider.value || carColor.r != uI.redGlowSlider.value || carColor.b != uI.blueGlowSlider.value)
        {
            carColor.a = 0.1f;
            carColor.r = uI.redGlowSlider.value;
            carColor.b = uI.blueGlowSlider.value;
            carColor.g = uI.greenGlowSlider.value;
            var main = sceneCarGlowLight[vehicleNumber].main;
            main.startColor = carColor;
            ///Reset the particle effect
            sceneCarGlowLight[vehicleNumber].gameObject.SetActive(false);
            sceneCarGlowLight[vehicleNumber].gameObject.SetActive(true);
            //reflectProbe.backgroundColor = carColor;
        }
    }
    #endregion

    // These methods can be called to play an audio sound
    #region PlayAudio Methods
    void AudioMusic()
    {
        if (RGT_PlayerPrefs.audioData.music.Length > 0)
        {
            emptyObject = new GameObject("Audio Clip: Music");
            emptyObject.transform.parent = audioContainer.transform;
            emptyObject.AddComponent<AudioSource>();
            garageAudioSource = emptyObject.GetComponent<AudioSource>();
            RGT_PlayerPrefs.audioData.currentAudioTrack = 0;
            garageAudioSource.clip = RGT_PlayerPrefs.audioData.music[RGT_PlayerPrefs.audioData.currentAudioTrack];
            garageAudioSource.loop = false;
            garageAudioSource.Play();
        }
    }

    void PlayNextAudioTrack()
    {
        if (RGT_PlayerPrefs.audioData.musicSelection == AudioData.MusicSelection.ListOrder)
        {
            if (RGT_PlayerPrefs.audioData.currentAudioTrack < RGT_PlayerPrefs.audioData.music.Length - 1)
            {
                RGT_PlayerPrefs.audioData.currentAudioTrack += 1;
            }
            else
            {
                RGT_PlayerPrefs.audioData.currentAudioTrack = 0;
            }
        }
        else if (RGT_PlayerPrefs.audioData.musicSelection == AudioData.MusicSelection.Random)
        {
            RGT_PlayerPrefs.audioData.currentAudioTrack = UnityEngine.Random.Range(0, RGT_PlayerPrefs.audioData.music.Length);
        }
        garageAudioSource.clip = RGT_PlayerPrefs.audioData.music[RGT_PlayerPrefs.audioData.currentAudioTrack];
        garageAudioSource.Play();
    }

    public void AudioMenuSelect()
    {
        emptyObject = new GameObject("Audio Clip: Menu Select");
        emptyObject.transform.parent = audioContainer.transform;
        emptyObject.AddComponent<AudioSource>();
        emptyObject.GetComponent<AudioSource>().clip = RGT_PlayerPrefs.audioData.menuSelect;
        emptyObject.GetComponent<AudioSource>().loop = false;
        emptyObject.GetComponent<AudioSource>().Play();
        emptyObject.AddComponent<DestroyAudio>();
        emptyObject = null;
    }

    public void AudioMenuBack()
    {
        emptyObject = new GameObject("Audio Clip: Menu Back");
        emptyObject.transform.parent = audioContainer.transform;
        emptyObject.AddComponent<AudioSource>();
        emptyObject.GetComponent<AudioSource>().clip = RGT_PlayerPrefs.audioData.menuBack;
        emptyObject.GetComponent<AudioSource>().loop = false;
        emptyObject.GetComponent<AudioSource>().Play();
        emptyObject.AddComponent<DestroyAudio>();
        emptyObject = null;
    }
    #endregion

    // These methods are used for the login menu
    #region Login menu
    public void Button_Login()
    {
        if (GameSparks.Core.GS.Available)
        {
            uI.loginBtn.GetComponent<Button>().interactable = false;
            uI.status.text = "Loading...";

            GameSparksManager.Instance().AuthenticatePlayer(uI.username.text.ToLower(), uI.password.text.ToLower(), PlayerAuthenticated, PlayerNotAuthenticated);
        }
        else
        {
            uI.status.text = "Make sure you are connected to the internet first.";
        }


    }

    public void PlayerAuthenticated(AuthenticationResponse fResponse)
    {
        uI.status.text = "Success!";
        if (uI.login.activeInHierarchy)
        {
            uI.startMenuWindow.SetActive(true);
            uI.login.SetActive(false);
            AudioMenuBack();
        }
        else
        {
            uI.login.SetActive(true);
            uI.startMenuWindow.SetActive(false);
            AudioMenuSelect();
        }
        uI.loginBtn.GetComponent<Button>().interactable = true;
    }
    public void PlayerNotAuthenticated(AuthenticationResponse fResponse)
    {
        uI.status.text = "Wrong username or password, please try again.";
        uI.loginBtn.GetComponent<Button>().interactable = true;
    }

    public void Button_Customize_Car()
    {
        if (uI.mainMenuWindow.activeInHierarchy)
        {
            uI.customizeCarMenuWindow.SetActive(true);
            uI.mainMenuWindow.SetActive(false);
            AudioMenuBack();
        }
        else
        {
            uI.mainMenuWindow.SetActive(true);
            uI.customizeCarMenuWindow.SetActive(false);
            AudioMenuSelect();
        }
    }

    public void ConnectingWindow(bool fFlag)
    {
        if (fFlag)
        {
            uI.connectingWindow.SetActive(true);
        }
        else
        {
            uI.connectingWindow.SetActive(false);
        }
    }
    #endregion

    // These methods are used for the questions menu
    #region Questions menu

    public void InitiateQuestionWindow()
    {
        QuestionType currentQuestion = GameSparksManager.Instance().RequestQuestion();

        GameSparksManager.Instance().InitializeQuestion(currentQuestion.ID);
    }

    public void InitiateSubmitAnswerWindow(int fMyAnswer)
    {
        GameSparksManager.Instance().EnableLoading("Submitting answer...");
        GameSparksManager.Instance().SubmitAnswer(fMyAnswer);
        uI.questionsWindow.SetActive(false);
    }

    public void FormatQuestionWindow(QuestionType fCurrentQuestion)
    {
        uI.questionText.text = fCurrentQuestion.Question;
        uI.questionText.transform.parent.gameObject.SetActive(true);



        int numberOfAnswers = fCurrentQuestion.Answers.Count;

        for (int i = 0; i < numberOfAnswers; i++)
        {
            answersTextList[i].text = fCurrentQuestion.Answers[i];
        }

        //adding on click events for each answer button
        uI.answer1Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.answer1Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { uI.connectingWindowText.text = "Submitting answer..."; uI.questionsWindow.SetActive(false); uI.connectingWindow.SetActive(true); });
        uI.answer1Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => InitiateSubmitAnswerWindow(1));
        uI.answer1Text.transform.parent.gameObject.SetActive(true);

        uI.answer2Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.answer2Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { uI.connectingWindowText.text = "Submitting answer..."; uI.questionsWindow.SetActive(false); uI.connectingWindow.SetActive(true); });
        uI.answer2Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => GameSparksManager.Instance().SubmitAnswer(2));
        uI.answer2Text.transform.parent.gameObject.SetActive(true);

        uI.answer3Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.answer3Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { uI.connectingWindowText.text = "Submitting answer..."; uI.questionsWindow.SetActive(false); uI.connectingWindow.SetActive(true); });
        uI.answer3Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => GameSparksManager.Instance().SubmitAnswer(3));
        uI.answer3Text.transform.parent.gameObject.SetActive(true);

        uI.answer4Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.answer4Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { uI.connectingWindowText.text = "Submitting answer..."; uI.questionsWindow.SetActive(false); uI.connectingWindow.SetActive(true); });
        uI.answer4Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => GameSparksManager.Instance().SubmitAnswer(4));
        uI.answer4Text.transform.parent.gameObject.SetActive(true);


        uI.answer5Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.answer5Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { uI.connectingWindowText.text = "Submitting answer..."; uI.questionsWindow.SetActive(false); uI.connectingWindow.SetActive(true); });
        uI.answer5Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => GameSparksManager.Instance().SubmitAnswer(5));
        uI.answer5Text.transform.parent.gameObject.SetActive(false);

        //activating answer button according to how many answers the current question has
        switch (numberOfAnswers)
        {
            case 3:
                uI.answer1Text.transform.parent.gameObject.SetActive(true);
                uI.answer2Text.transform.parent.gameObject.SetActive(true);
                uI.answer3Text.transform.parent.gameObject.SetActive(true);
                uI.answer4Text.transform.parent.gameObject.SetActive(false);
                uI.answer5Text.transform.parent.gameObject.SetActive(false);
                break;
            case 4:
                uI.answer1Text.transform.parent.gameObject.SetActive(true);
                uI.answer2Text.transform.parent.gameObject.SetActive(true);
                uI.answer3Text.transform.parent.gameObject.SetActive(true);
                uI.answer4Text.transform.parent.gameObject.SetActive(true);
                uI.answer5Text.transform.parent.gameObject.SetActive(false);
                break;
            case 5:
                uI.answer1Text.transform.parent.gameObject.SetActive(true);
                uI.answer2Text.transform.parent.gameObject.SetActive(true);
                uI.answer3Text.transform.parent.gameObject.SetActive(true);
                uI.answer4Text.transform.parent.gameObject.SetActive(true);
                uI.answer5Text.transform.parent.gameObject.SetActive(true);
                break;

            default:
                break;
        }
    }

    public void FormatQuestionConfirmationWindow()
    {
        //
        if (GameSparksManager.Instance().IsQBankAvailable())
        {
            uI.confirmWindowText.text = GameSparksManager.Instance().QuestionsBank.Count + " Questions available for you, show question?";

            uI.questionsConfirmBtnYes.GetComponent<Button>().onClick.RemoveAllListeners();
            uI.questionsConfirmBtnYes.GetComponent<Button>().onClick.AddListener(() => Button_ConfirmationToQuestion());
            uI.questionsConfirmBtnYes.SetActive(true);

            uI.questionsConfirmBtnNo.GetComponent<Button>().onClick.RemoveAllListeners();
            uI.questionsConfirmBtnNo.GetComponent<Button>().onClick.AddListener(() => Button_MenuToQuestionsConfirmation());
            uI.questionsConfirmBtnNo.SetActive(true);
        }
        else
        {
            uI.confirmWindowText.text = "No Questions available for you";
            uI.questionsConfirmBtnYes.GetComponent<Button>().onClick.RemoveAllListeners();
            uI.questionsConfirmBtnYes.SetActive(false);

            uI.questionsConfirmBtnNo.GetComponent<Button>().onClick.RemoveAllListeners();
            uI.questionsConfirmBtnNo.GetComponent<Button>().onClick.AddListener(() => Button_MenuToQuestionsConfirmation());
            uI.questionsConfirmBtnNo.SetActive(true);
        }

    }

    public void FormatQuestionConfirmationWindow(QuestionType fMyQuestion)
    {
        if (fMyQuestion.Correct == 1)
        {
            uI.confirmWindowText.text = "Your answer is correct. ";
        }
        else if (fMyQuestion.Correct == 0)
        {
            uI.confirmWindowText.text = "Your answer is wrong. ";
        }

        if (GameSparksManager.Instance().IsQBankAvailable())
        {
            uI.confirmWindowText.text += GameSparksManager.Instance().QuestionsBank.Count + " Questions available for you, show question?";

            uI.questionsConfirmBtnYes.GetComponent<Button>().onClick.RemoveAllListeners();
            uI.questionsConfirmBtnYes.GetComponent<Button>().onClick.AddListener(() => Button_ConfirmationToQuestion());
            uI.questionsConfirmBtnYes.SetActive(true);

            uI.questionsConfirmBtnNo.GetComponent<Button>().onClick.RemoveAllListeners();
            uI.questionsConfirmBtnNo.GetComponent<Button>().onClick.AddListener(() => Button_MenuToQuestionsConfirmation());
            uI.questionsConfirmBtnNo.SetActive(true);
        }
        else
        {
            uI.confirmWindowText.text += "No more questions available for you.";
            uI.questionsConfirmBtnYes.GetComponent<Button>().onClick.RemoveAllListeners();
            uI.questionsConfirmBtnYes.SetActive(false);

            uI.questionsConfirmBtnNo.GetComponent<Button>().onClick.RemoveAllListeners();
            uI.questionsConfirmBtnNo.GetComponent<Button>().onClick.AddListener(() => Button_MenuToQuestionsConfirmation());
            uI.questionsConfirmBtnNo.SetActive(true);
        }

    }

    public void Button_ConfirmationToQuestion()
    {
        GameSparksManager.Instance().EnableLoading("Requesting question...");
        GameSparksManager.Instance().InitiateQuestionWindow();
        uI.questionsConfirmWindow.SetActive(false);
        AudioMenuSelect();
    }

    public void Button_QuestionToConfirmation(QuestionType fMyQuestion)
    {
        //uI.connectingWindow.SetActive(false);
        FormatQuestionConfirmationWindow(fMyQuestion);
        uI.questionsConfirmWindow.SetActive(true);
        uI.questionsWindow.SetActive(false);
        AudioMenuSelect();
    }

    public void FormatQuestionBankWindow()
    {
        //reseting selected questions
        selectedQuestionToPublish.Clear();

        //reset scroll position of the window
        questionsBankContent.transform.position = new Vector3(questionsBankContent.transform.position.x, -200, questionsBankContent.transform.position.z);

        //clearing old questions from window
        int tempCount = questionsBankContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(questionsBankContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().QuestionsBank.Count < 1)
        {
            GameObject tempObj = Instantiate(SelectableQuestionUIPrefab, questionsBankContent.transform);
            tempObj.GetComponent<QuestionUI>().questionText.text = "No questions in bank.";
        }

        //creating questions in the window
        else
        {
            foreach (var question in GameSparksManager.Instance().QuestionsBank)
            {
                GameObject tempObj = Instantiate(SelectableQuestionUIPrefab, questionsBankContent.transform);
                tempObj.GetComponent<QuestionUI>().myQuestion = question;
                tempObj.GetComponent<QuestionUI>().SetQuestionsForAdminView(question);
            }
        }
        //fitting content area according to aspect ratio
        if (Camera.main.aspect >= 1.7)
        {
            Debug.Log("16:9");
            questionsBankContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(questionsBankContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 990);
        }
        else if (Camera.main.aspect >= 1.5)
        {
            Debug.Log("3:2");
        }
        else
        {
            Debug.Log("4:3");
            questionsBankContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(questionsBankContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 1200);
        }
    }

    public void FormatQuestionsSheetWindow()
    {
        //reseting selected questions
        selectedQuestionToPublish.Clear();

        //reset scroll position of the window
        questionsSheetContent.transform.position = new Vector3(questionsSheetContent.transform.position.x, -200, questionsSheetContent.transform.position.z);

        //clearing old questions from window
        int tempCount = questionsSheetContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(questionsSheetContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().QuestionsSheet.Count < 1)
        {
            GameObject tempObj = Instantiate(SelectableQuestionUIPrefab, questionsSheetContent.transform);
            tempObj.GetComponent<QuestionUI>().questionText.text = "No questions in bank.";
            tempObj.GetComponent<Toggle>().interactable = false;
        }

        //creating questions in the window
        else
        {
            foreach (var question in GameSparksManager.Instance().QuestionsSheet)
            {
                if (question.Published == 0)
                {
                    GameObject tempObj = Instantiate(SelectableQuestionUIPrefab, questionsSheetContent.transform);
                    tempObj.GetComponent<QuestionUI>().myQuestion = question;
                    tempObj.GetComponent<QuestionUI>().SetQuestionsForAdminView(question);
                }

            }
        }
        //fitting content area according to aspect ratio
        if (Camera.main.aspect >= 1.7)
        {
            Debug.Log("16:9");
            questionsSheetContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(questionsSheetContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 990);
        }
        else if (Camera.main.aspect >= 1.5)
        {
            Debug.Log("3:2");
        }
        else
        {
            Debug.Log("4:3");
            questionsSheetContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(questionsSheetContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 1200);
        }
    }

    public void FormatPlayerHistoryWindow()
    {
        //reset scroll position of the window
        playerHistoryContent.transform.position = new Vector3(playerHistoryContent.transform.position.x, -200, playerHistoryContent.transform.position.z);

        //clearing old questions from window
        int tempCount = playerHistoryContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(playerHistoryContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().PlayersHistory.Count < 1)
        {
            GameObject tempObj = Instantiate(playerRecordUIPrefab, playerHistoryContent.transform);
            tempObj.GetComponent<PlayerRecordUI>().playerRecordText.text = "No players history.";
        }

        //creating questions in the window
        else
        {
            foreach (var record in GameSparksManager.Instance().PlayersHistory)
            {
                GameObject tempObj = Instantiate(playerRecordUIPrefab, playerHistoryContent.transform);
                tempObj.GetComponent<PlayerRecordUI>().myRecord = record;
                tempObj.GetComponent<PlayerRecordUI>().SetRecordForView(record);
            }
        }
        //fitting content area according to aspect ratio
        if (Camera.main.aspect >= 1.7)
        {
            Debug.Log("16:9");
            playerHistoryContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(playerHistoryContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 990);
        }
        else if (Camera.main.aspect >= 1.5)
        {
            Debug.Log("3:2");
        }
        else
        {
            Debug.Log("4:3");
            playerHistoryContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(playerHistoryContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 1200);
        }
    }

    public void FormatLeaderboardWindow()
    {
        //reset scroll position of the window
        leaderboardContent.transform.position = new Vector3(leaderboardContent.transform.position.x, -200, leaderboardContent.transform.position.z);

        //clearing old questions from window
        int tempCount = leaderboardContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(leaderboardContent.transform.GetChild(i).gameObject);
        }

        //if (GameSparksManager.Instance().ScoreLeaderboard.Count < 1)
        //{
        //    GameObject tempObj = Instantiate(playerRecordUIPrefab, leaderboardContent.transform);
        //    tempObj.GetComponent<PlayerRecordUI>().playerRecordText.text = "Leaderboard empty";
        //}

        if (GameSparksManager.Instance().Players.Count < 1)
        {
            //GameObject tempObj = Instantiate(playerRecordUIPrefab, leaderboardContent.transform);
            //tempObj.GetComponent<PlayerRecordUI>().playerRecordText.text = "Leaderboard empty";
        }

        //creating questions in the window
        else
        {
            //foreach (var record in GameSparksManager.Instance().ScoreLeaderboard)
            //{
            //    GameObject tempObj = Instantiate(playerRecordUIPrefab, leaderboardContent.transform);
            //    tempObj.GetComponent<PlayerRecordUI>().myRecord = record;
            //    tempObj.GetComponent<PlayerRecordUI>().SetRecordForLeaderboardView(record);
            //}

            foreach (var player in GameSparksManager.Instance().Players)
            {
                if (player.Team != "AdminAdcetris" && player.Team != "AdminEntyvio")
                {
                    GameObject tempObj = Instantiate(genericPlayerRecordPrefab, leaderboardContent.transform);
                    //tempObj.GetComponent<PlayerRecordUI>().myRecord = player;
                    tempObj.GetComponent<PlayerRecordUI>().SetRecordForLeaderboardView(player);
                }
            }
        }
        //fitting content area according to aspect ratio
        if (Camera.main.aspect >= 1.7)
        {
            //Debug.Log("16:9");
            leaderboardContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(leaderboardContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 990);
        }
        else if (Camera.main.aspect >= 1.5)
        {
            //Debug.Log("3:2");
        }
        else
        {
            //Debug.Log("4:3");
            leaderboardContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(leaderboardContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 1200);
        }

    }

    public void FormatPlayerHistoryGenericWindow(List<PlayerRecord> fPlayerHistory, GameObject fSourceWindow)
    {
        //format the cell size height to fit all lines in the player record
        uI.genericWindowContent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(800, 210);

        uI.genericWindowTitle.text = "Player's Records";

        //clearing old questions from window
        int tempCount = uI.genericWindowContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(uI.genericWindowContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().PlayersHistory.Count < 1)
        {
            //GameObject tempObj = Instantiate(playerRecordUIPrefab, playerHistoryContent.transform);
            //tempObj.GetComponent<PlayerRecordUI>().playerRecordText.text = "No players history.";
        }

        //creating questions in the window
        else
        {
            foreach (var record in fPlayerHistory)
            {
                GameObject tempObj = Instantiate(genericPlayerRecordPrefab, uI.genericWindowContent.transform);
                tempObj.GetComponent<PlayerRecordUI>().myRecord = record;
                tempObj.GetComponent<PlayerRecordUI>().SetRecordForView(record);
            }
        }
        //fitting content area according to aspect ratio
        if (Camera.main.aspect >= 1.7)
        {
            Debug.Log("16:9");
        }
        else if (Camera.main.aspect >= 1.5)
        {
            Debug.Log("3:2");
        }
        else
        {
            Debug.Log("4:3");
        }
        //formatting listeners on action buttons
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_Yes.SetActive(false);

        uI.genericWindowAction_No.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_No.GetComponent<Button>().onClick.AddListener(() => { uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_No.SetActive(true);
    }

    public void ResetScrollRect(GameObject fContent)
    {
        fContent.transform.parent.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;
    }

    public void FormatQuestionBankGenericWindow(List<QuestionType> fQuestionBank, GameObject fSourceWindow)
    {
        //reseting selected questions
        selectedQuestionToPublish.Clear();

        //format the cell size width & height to fit all lines in the question record
        uI.genericWindowContent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(1900, 500);

        uI.genericWindowTitle.text = "Available Questions";

        //clearing old questions from window
        int tempCount = uI.genericWindowContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(uI.genericWindowContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().QuestionsBank.Count < 1)
        {

        }
        //creating questions in the window
        else
        {
            foreach (var record in fQuestionBank)
            {
                GameObject tempObj = Instantiate(genericSelectableQuestionRecordPrefab, uI.genericWindowContent.transform);
                tempObj.GetComponent<QuestionUI>().myQuestion = record;
                tempObj.GetComponent<QuestionUI>().SetQuestionsForAdminView(record);
            }
            Debug.Log("creating question");
        }

        //formatting listeners on action buttons
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.AddListener(() => { Button_RepublishSelectedQuestions(); });
        uI.genericWindowAction_Yes.SetActive(true);

        uI.genericWindowAction_No.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_No.GetComponent<Button>().onClick.AddListener(() => { uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_No.SetActive(true);
    }

    public void FormatQuestionSheetGenericWindow(List<QuestionType> fQuestionSheet, GameObject fSourceWindow)
    {
        //reseting selected questions
        selectedQuestionToPublish.Clear();

        //format the cell size width & height to fit all lines in the question record
        uI.genericWindowContent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(1900, 500);

        uI.genericWindowTitle.text = "All Questions";

        //clearing old questions from window
        int tempCount = uI.genericWindowContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(uI.genericWindowContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().QuestionsSheet.Count < 1)
        {

        }

        //creating questions in the window
        else
        {
            foreach (var question in fQuestionSheet)
            {
                if (question.Published == 0)
                {
                    GameObject tempObj = Instantiate(genericSelectableQuestionRecordPrefab, uI.genericWindowContent.transform);
                    tempObj.GetComponent<QuestionUI>().myQuestion = question;
                    tempObj.GetComponent<QuestionUI>().SetQuestionsForAdminView(question);
                }

            }
            Debug.Log("creating question");
        }
        //formatting listeners on action buttons
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.AddListener(() => { Button_PublishSelectedQuestions(); uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_Yes.SetActive(true);

        uI.genericWindowAction_No.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_No.GetComponent<Button>().onClick.AddListener(() => { uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_No.SetActive(true);
    }

    public void FormatQuestionBankMedicalGenericWindow(List<QuestionType> fQuestionBank, GameObject fSourceWindow)
    {
        //reseting selected questions
        selectedQuestionToPublish.Clear();

        //format the cell size width & height to fit all lines in the question record
        uI.genericWindowContent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(1900, 500);

        uI.genericWindowTitle.text = "Available Questions";

        //clearing old questions from window
        int tempCount = uI.genericWindowContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(uI.genericWindowContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().QuestionsBank.Count < 1)
        {

        }
        //creating questions in the window
        else
        {
            foreach (var record in fQuestionBank)
            {
                if (record.MyQuestionType == "medical")
                {
                    GameObject tempObj = Instantiate(genericSelectableQuestionRecordPrefab, uI.genericWindowContent.transform);
                    tempObj.GetComponent<QuestionUI>().myQuestion = record;
                    tempObj.GetComponent<QuestionUI>().SetQuestionsForAdminView(record);
                }
            }
            Debug.Log("creating question");
        }


        //formatting listeners on action buttons
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.AddListener(() => { Button_RepublishSelectedQuestions(); });
        uI.genericWindowAction_Yes.SetActive(true);

        uI.genericWindowAction_No.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_No.GetComponent<Button>().onClick.AddListener(() => { uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_No.SetActive(true);
    }

    public void FormatQuestionBankMarketingGenericWindow(List<QuestionType> fQuestionBank, GameObject fSourceWindow)
    {
        //reseting selected questions
        selectedQuestionToPublish.Clear();

        //format the cell size width & height to fit all lines in the question record
        uI.genericWindowContent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(1900, 500);

        uI.genericWindowTitle.text = "Available Questions";

        //clearing old questions from window
        int tempCount = uI.genericWindowContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(uI.genericWindowContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().QuestionsBank.Count < 1)
        {

        }
        //creating questions in the window
        else
        {
            foreach (var record in fQuestionBank)
            {
                if (record.MyQuestionType == "marketing")
                {
                    GameObject tempObj = Instantiate(genericSelectableQuestionRecordPrefab, uI.genericWindowContent.transform);
                    tempObj.GetComponent<QuestionUI>().myQuestion = record;
                    tempObj.GetComponent<QuestionUI>().SetQuestionsForAdminView(record);
                }
            }
            Debug.Log("creating question");
        }

        //formatting listeners on action buttons
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.AddListener(() => { Button_RepublishSelectedQuestions(); });
        uI.genericWindowAction_Yes.SetActive(true);

        uI.genericWindowAction_No.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_No.GetComponent<Button>().onClick.AddListener(() => { uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_No.SetActive(true);
    }

    public void FormatQuestionSheetMedicalGenericWindow(List<QuestionType> fQuestionSheet, GameObject fSourceWindow)
    {
        //reseting selected questions
        selectedQuestionToPublish.Clear();

        //format the cell size width & height to fit all lines in the question record
        uI.genericWindowContent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(1900, 500);

        uI.genericWindowTitle.text = "All Questions";

        //clearing old questions from window
        int tempCount = uI.genericWindowContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(uI.genericWindowContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().QuestionsSheet.Count < 1)
        {

        }

        //creating questions in the window
        else
        {
            foreach (var question in fQuestionSheet)
            {
                if (question.Published == 0 && question.MyQuestionType == "medical")
                {
                    GameObject tempObj = Instantiate(genericSelectableQuestionRecordPrefab, uI.genericWindowContent.transform);
                    tempObj.GetComponent<QuestionUI>().myQuestion = question;
                    tempObj.GetComponent<QuestionUI>().SetQuestionsForAdminView(question);
                }

            }
            Debug.Log("creating question");
        }
        //formatting listeners on action buttons
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.AddListener(() => { Button_PublishSelectedQuestions(); uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_Yes.SetActive(true);

        uI.genericWindowAction_No.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_No.GetComponent<Button>().onClick.AddListener(() => { uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_No.SetActive(true);
    }

    public void FormatQuestionSheetMarketingGenericWindow(List<QuestionType> fQuestionSheet, GameObject fSourceWindow)
    {
        //reseting selected questions
        selectedQuestionToPublish.Clear();

        //format the cell size width & height to fit all lines in the question record
        uI.genericWindowContent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(1900, 500);

        uI.genericWindowTitle.text = "All Questions";

        //clearing old questions from window
        int tempCount = uI.genericWindowContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(uI.genericWindowContent.transform.GetChild(i).gameObject);
        }

        if (GameSparksManager.Instance().QuestionsSheet.Count < 1)
        {

        }

        //creating questions in the window
        else
        {
            foreach (var question in fQuestionSheet)
            {
                if (question.Published == 0 && question.MyQuestionType == "marketing")
                {
                    GameObject tempObj = Instantiate(genericSelectableQuestionRecordPrefab, uI.genericWindowContent.transform);
                    tempObj.GetComponent<QuestionUI>().myQuestion = question;
                    tempObj.GetComponent<QuestionUI>().SetQuestionsForAdminView(question);
                }

            }
            Debug.Log("creating question");
        }
        //formatting listeners on action buttons
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.AddListener(() => { Button_PublishSelectedQuestions(); uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_Yes.SetActive(true);

        uI.genericWindowAction_No.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_No.GetComponent<Button>().onClick.AddListener(() => { uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_No.SetActive(true);
    }

    public void FormatLeaderboardGenericWindow(GameObject fSourceWindow)
    {
        //reset scroll position of the window
        uI.genericWindowContent.transform.position = new Vector3(leaderboardContent.transform.position.x, -200, leaderboardContent.transform.position.z);
        //format the cell size width & height to fit all lines in the question record
        uI.genericWindowContent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(1900, 200);
        uI.genericWindowTitle.text = "Leaderboard";
        //clearing old questions from window
        int tempCount = uI.genericWindowContent.transform.childCount;
        for (int i = 0; i < tempCount; i++)
        {
            Destroy(uI.genericWindowContent.transform.GetChild(i).gameObject);
        }



        if (GameSparksManager.Instance().Players.Count < 1)
        {

        }

        //creating questions in the window
        else
        {

            foreach (var player in GameSparksManager.Instance().Players)
            {
                if (GameSparksManager.Instance().TeamName == "AdminAdcetris" || GameSparksManager.Instance().TeamName == "AdminEntyvio")
                {
                    if (player.Team != "AdminAdcetris" && player.Team != "AdminEntyvio")
                    {

                        GameObject tempObj = Instantiate(genericPlayerRecordPrefab, uI.genericWindowContent.transform);
                        //tempObj.GetComponent<PlayerRecordUI>().myRecord = player;
                        tempObj.GetComponent<PlayerRecordUI>().SetRecordForLeaderboardView(player);
                    }
                }
                else
                {
                    if (player.Team == GameSparksManager.Instance().TeamName)
                    {
                        GameObject tempObj = Instantiate(genericPlayerRecordPrefab, uI.genericWindowContent.transform);
                        //tempObj.GetComponent<PlayerRecordUI>().myRecord = player;
                        tempObj.GetComponent<PlayerRecordUI>().SetRecordForLeaderboardView(player);
                    }
                }

            }
        }
        //fitting content area according to aspect ratio
        if (Camera.main.aspect >= 1.7)
        {
            //Debug.Log("16:9");
            //uI.genericWindowContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(uI.genericWindowContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 990);
        }
        else if (Camera.main.aspect >= 1.5)
        {
            //Debug.Log("3:2");
        }
        else
        {
            //Debug.Log("4:3");
            //uI.genericWindowContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(uI.genericWindowContent.transform.parent.parent.GetComponent<RectTransform>().sizeDelta.x, 1200);
        }

        //formatting listeners on action buttons
        uI.genericWindowAction_Yes.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_Yes.SetActive(false);

        uI.genericWindowAction_No.GetComponent<Button>().onClick.RemoveAllListeners();
        uI.genericWindowAction_No.GetComponent<Button>().onClick.AddListener(() => { uI.genericWindow.SetActive(false); fSourceWindow.SetActive(true); });
        uI.genericWindowAction_No.SetActive(true);
    }

    public IEnumerator RefreshWindow(GameObject fTargetContent)
    {
        yield return new WaitForSeconds(2f);
        fTargetContent.transform.parent.parent.gameObject.SetActive(false);
        fTargetContent.transform.parent.parent.gameObject.SetActive(true);
    }

    public void QuestionSelected(QuestionType fMyQuestion)
    {
        selectedQuestionToPublish.Add(fMyQuestion);
    }

    public void QuestionUnSelected(QuestionType fMyQuestion)
    {
        selectedQuestionToPublish.Remove(fMyQuestion);
    }

    //public void QuestionSelectedToRepublish(QuestionType fMyQuestion)
    //{
    //    selectedQuestionToRePublish.Add(fMyQuestion);
    //}

    //public void QuestionUnSelectedToRepublish(QuestionType fMyQuestion)
    //{
    //    selectedQuestionToRePublish.Remove(fMyQuestion);
    //}

    public void Button_MenuToQuestionsBank()
    {
        Button_AdminToQuestionsBank();
    }

    public void Button_QuestionsBankToMenu()
    {
        uI.questionsBankWindow.SetActive(false);
        uI.adminWindow.SetActive(true);
        AudioMenuSelect();
    }

    public void Button_AdminToPublishedQuestion()
    {
        uI.adminWindow.SetActive(false);
        uI.publishedQuestionsWindow.SetActive(true);
        AudioMenuSelect();
    }

    public void Button_AdminToUnPublishedQuestion()
    {
        uI.adminWindow.SetActive(false);
        uI.unpublishedQuestionsWindow.SetActive(true);
        AudioMenuSelect();
    }

    public void Button_QuestionTypesToAdmin()
    {
        uI.publishedQuestionsWindow.SetActive(false);
        uI.unpublishedQuestionsWindow.SetActive(false);
        uI.adminWindow.SetActive(true);
        AudioMenuSelect();
    }

    public void Button_AdminToQuestionsSheet()
    {
        //FormatQuestionsSheetWindow();
        FormatQuestionSheetGenericWindow(GameSparksManager.Instance().QuestionsSheet, uI.adminWindow);
        uI.adminWindow.SetActive(false);
        uI.genericWindow.SetActive(true);
        ResetScrollRect(uI.genericWindowContent);
        AudioMenuSelect();
    }

    public void Button_QuestionsSheetToMenu()
    {
        uI.questionsSheetWindow.SetActive(false);
        uI.adminWindow.SetActive(true);
        AudioMenuSelect();
    }

    public void Button_PublishSelectedQuestions()
    {
        if (selectedQuestionToPublish.Count > 0)
        {
            EnableLoading("Publishing Questions...");

            foreach (var question in selectedQuestionToPublish)
            {
                GameSparksManager.Instance().PublishQuestion(question);
            }
            GameSparksManager.Instance().GetQuestionsBank();
        }
    }

    public void Button_RepublishSelectedQuestions()
    {
        if (selectedQuestionToPublish.Count > 0)
        {
            EnableLoading("Republishing Questions...");

            foreach (var question in selectedQuestionToPublish)
            {
                GameSparksManager.Instance().DeleteQuestionHistory(question.ID);
            }
            //GameSparksManager.Instance().GetQuestionsBank();
        }
    }

    public void Button_QuestionToResult()
    {

    }


    public void Button_ToQuestionsBankMedicalGenericWindow()
    {
        FormatQuestionBankMedicalGenericWindow(GameSparksManager.Instance().QuestionsBank, uI.publishedQuestionsWindow);
        uI.publishedQuestionsWindow.SetActive(false);
        uI.genericWindow.SetActive(true);
        ResetScrollRect(uI.genericWindowContent);
    }

    public void Button_ToQuestionsBankMarketingGenericWindow()
    {
        FormatQuestionBankMarketingGenericWindow(GameSparksManager.Instance().QuestionsBank, uI.publishedQuestionsWindow);
        uI.publishedQuestionsWindow.SetActive(false);
        uI.genericWindow.SetActive(true);
        ResetScrollRect(uI.genericWindowContent);
    }

    public void Button_ToQuestionsSheetMedicalGenericWindow()
    {
        FormatQuestionSheetMedicalGenericWindow(GameSparksManager.Instance().QuestionsSheet, uI.unpublishedQuestionsWindow);
        uI.unpublishedQuestionsWindow.SetActive(false);
        uI.genericWindow.SetActive(true);
        ResetScrollRect(uI.genericWindowContent);
    }

    public void Button_ToQuestionsSheetMarketingGenericWindow()
    {
        FormatQuestionSheetMarketingGenericWindow(GameSparksManager.Instance().QuestionsSheet, uI.unpublishedQuestionsWindow);
        uI.unpublishedQuestionsWindow.SetActive(false);
        uI.genericWindow.SetActive(true);
        ResetScrollRect(uI.genericWindowContent);
    }

    #endregion

    #region Admin menu
    public void Button_AdminToQuestionsBank()
    {
        FormatQuestionBankGenericWindow(GameSparksManager.Instance().QuestionsBank, uI.adminWindow);
        uI.adminWindow.SetActive(false);
        uI.genericWindow.SetActive(true);
        ResetScrollRect(uI.genericWindowContent);
        AudioMenuSelect();
    }

    //public void Button_AdminToQuestionsSheet()
    //{
    //    FormatQuestionsSheetWindow();
    //    uI.adminWindow.SetActive(false);
    //    uI.questionsSheetWindow.SetActive(true);
    //    StopAllCoroutines();
    //    IEnumerator tempCoroutine = RefreshWindow(questionsSheetContent);
    //    StartCoroutine(tempCoroutine);
    //    AudioMenuSelect();
    //}

    public void Button_AdminToPlayerHistory()
    {
        FormatPlayerHistoryGenericWindow(GameSparksManager.Instance().PlayersHistory, uI.adminWindow);
        uI.adminWindow.SetActive(false);
        uI.genericWindow.SetActive(true);
        ResetScrollRect(uI.genericWindowContent);
        AudioMenuSelect();
    }

    public void Button_PlayerHistoryToAdmin()
    {
        uI.playerHistoryWindow.SetActive(false);
        uI.adminWindow.SetActive(true);
        AudioMenuSelect();
    }

    public void Button_MenuToLeaderboard()
    {
        //FormatLeaderboardWindow();
        FormatLeaderboardGenericWindow(uI.mainMenuWindow);
        uI.mainMenuWindow.SetActive(false);
        uI.genericWindow.SetActive(true);
        StopAllCoroutines();
        IEnumerator tempCoroutine = RefreshWindow(uI.genericWindowContent);
        StartCoroutine(tempCoroutine);
        AudioMenuSelect();
    }

    public void Button_LeaderboardToMenu()
    {
        uI.leaderboardWindow.SetActive(false);
        uI.mainMenuWindow.SetActive(true);
        AudioMenuSelect();
    }

    public void Button_GenericWindowToDestinationWindow(GameObject fDestinationWindow)
    {
        uI.playerHistoryWindow.SetActive(false);
        fDestinationWindow.SetActive(true);
        AudioMenuSelect();
    }
    #endregion


    public void Button_TopSpeed()
    {
        AudioMenuSelect();
        uI.upgradeConfirmText.text = "Upgrade " + "Top Speed" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeSpeedPrice.ToString("N0") + " Coins";
        if (currency >= RGT_PlayerPrefs.playableVehicles.upgradeSpeedPrice) uI.upgradesConfirmWindow.SetActive(true);
    }

    public void Button_Acceleration()
    {
        AudioMenuSelect();
        uI.upgradeConfirmText.text = "Upgrade " + "Acceleration" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeAccelerationPrice.ToString("N0") + " Coins";
        if (currency >= RGT_PlayerPrefs.playableVehicles.upgradeAccelerationPrice) uI.upgradesConfirmWindow.SetActive(true);
    }

    public void Button_BrakePower()
    {
        AudioMenuSelect();
        uI.upgradeConfirmText.text = "Upgrade " + "Brake Power" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeBrakesPrice.ToString("N0") + " Coins";
        if (currency >= RGT_PlayerPrefs.playableVehicles.upgradeBrakesPrice) uI.upgradesConfirmWindow.SetActive(true);
    }

    public void Button_TireTraction()
    {
        AudioMenuSelect();
        uI.upgradeConfirmText.text = "Upgrade " + "Tire Traction" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeTiresPrice.ToString("N0") + " Coins";
        if (currency >= RGT_PlayerPrefs.playableVehicles.upgradeTiresPrice) uI.upgradesConfirmWindow.SetActive(true);
    }

    public void Button_SteerSensitivity()
    {
        AudioMenuSelect();
        uI.upgradeConfirmText.text = "Upgrade " + "Steer Sensitivity" + "\nfor\n" + RGT_PlayerPrefs.playableVehicles.upgradeSteeringPrice.ToString("N0") + " Coins";
        if (currency >= RGT_PlayerPrefs.playableVehicles.upgradeSteeringPrice) uI.upgradesConfirmWindow.SetActive(true);
    }

    // Call this to cancel a purchase confirmation
    public void DeclinePurchase()
    {
        uI.carConfirmWindow.SetActive(false);
        uI.paintConfirmWindow.SetActive(false);
        uI.glowConfirmWindow.SetActive(false);
        uI.glassColorConfirmWindow.SetActive(false);
        uI.brakeColorConfirmWindow.SetActive(false);
        uI.rimColorConfirmWindow.SetActive(false);
        uI.unlockRaceConfirmWindow.SetActive(false);
        uI.upgradesConfirmWindow.SetActive(false);
        AudioMenuBack();
    }

    public void AcceptPurchase()
    {
        uI.carConfirmWindow.SetActive(false);
        RGT_PlayerPrefs.SetVehicleLock(vehicleNumber, "UNLOCKED");
        RGT_PlayerPrefs.playableVehicles.carUnlocked[vehicleNumber] = true;
        //set currency
        RGT_PlayerPrefs.SetCurrency(currency - RGT_PlayerPrefs.playableVehicles.price[vehicleNumber]);
        //set Vehicle Number
        RGT_PlayerPrefs.SetVehicleNumber(vehicleNumber);
        UpdateCurrency();
        uI.buyCarButton.SetActive(false);
        uI.selectCarButton.SetActive(true);
    }

    public void NextCar()
    {
        uI.buyCarConfirmWindow.SetActive(false);
        AudioMenuSelect();
        sceneCarModel[vehicleNumber].SetActive(false);
        if (vehicleNumber < RGT_PlayerPrefs.playableVehicles.numberOfCars - 1)
        {
            vehicleNumber += 1;
            sceneCarModel[vehicleNumber].SetActive(true);
        }
        else
        {
            vehicleNumber = 0;
            sceneCarModel[vehicleNumber].SetActive(true);
        }
        uI.carNameText.text = RGT_PlayerPrefs.playableVehicles.vehicleNames[vehicleNumber];
        if (RGT_PlayerPrefs.playableVehicles.carUnlocked[vehicleNumber])
        {
            uI.buyCarButton.SetActive(false);
            uI.selectCarButton.SetActive(false);
            uI.selectCarButton.SetActive(true);
            //set Vehicle Number
            RGT_PlayerPrefs.SetVehicleNumber(vehicleNumber);
        }
        else
        {
            uI.selectCarButton.SetActive(false);
            uI.buyCarButton.SetActive(false);
            uI.buyCarButton.SetActive(true);
            uI.selectCarText.text = RGT_PlayerPrefs.playableVehicles.price[vehicleNumber].ToString("N0") + " Coins";
        }
        RGT_PlayerPrefs.playableVehicles.currentVehicleNumber = vehicleNumber;
        UpdateCar();
    }

    public void PreviousCar()
    {
        uI.buyCarConfirmWindow.SetActive(false);
        AudioMenuSelect();
        sceneCarModel[vehicleNumber].SetActive(false);
        if (vehicleNumber > 0)
        {
            vehicleNumber -= 1;
            sceneCarModel[vehicleNumber].SetActive(true);
        }
        else
        {
            vehicleNumber = RGT_PlayerPrefs.playableVehicles.numberOfCars - 1;
            sceneCarModel[vehicleNumber].SetActive(true);
        }
        uI.carNameText.text = RGT_PlayerPrefs.playableVehicles.vehicleNames[vehicleNumber];
        if (RGT_PlayerPrefs.playableVehicles.carUnlocked[vehicleNumber])
        {
            uI.buyCarButton.SetActive(false);
            uI.selectCarButton.SetActive(false);
            uI.selectCarButton.SetActive(true);
            //set Vehicle Number
            RGT_PlayerPrefs.SetVehicleNumber(vehicleNumber);
        }
        else
        {
            uI.selectCarButton.SetActive(false);
            uI.buyCarButton.SetActive(false);
            uI.buyCarButton.SetActive(true);
            uI.selectCarText.text = RGT_PlayerPrefs.playableVehicles.price[vehicleNumber].ToString("N0") + " Coins";
        }
        RGT_PlayerPrefs.playableVehicles.currentVehicleNumber = vehicleNumber;
        UpdateCar();
    }

    public void UpdateCar()
    {
        UpdateCurrency();


        levelBonusTopSpeed = 0;
        for (int i = 0; i < RGT_PlayerPrefs.GetVehicleTopSpeedLevel(vehicleNumber) + 1; i++)
        {
            levelBonusTopSpeed += RGT_PlayerPrefs.playableVehicles.vehicleUpgrades[vehicleNumber].topSpeed[i];
        }

        //		for(int i = 0; i < RGT_PlayerPrefs.GetVehicleAccelerationLevel(vehicleNumber) + 1; i++){
        //			levelBonusAcceleration += RGT_PlayerPrefs.playableVehicles.vehicleUpgrades [vehicleNumber].acceleration [i];
        //		}
        //
        //		for(int i = 0; i < RGT_PlayerPrefs.GetVehicleBrakePowerLevel(vehicleNumber) + 1; i++){
        //			levelBonusBrakePower += RGT_PlayerPrefs.playableVehicles.vehicleUpgrades [vehicleNumber].brakePower [i];
        //		}
        //
        //		for(int i = 0; i < RGT_PlayerPrefs.GetVehicleTireTractionLevel(vehicleNumber) + 1; i++){
        //			levelBonusTireTraction += RGT_PlayerPrefs.playableVehicles.vehicleUpgrades [vehicleNumber].tireTraction [i];
        //		}
        //
        //		for(int i = 0; i < RGT_PlayerPrefs.GetVehicleSteerSensitivityLevel(vehicleNumber) + 1; i++){
        //			levelBonusSteerSensitivity += RGT_PlayerPrefs.playableVehicles.vehicleUpgrades [vehicleNumber].steerSensitivity [i];
        //		}

#if EVP_SUPPORT
        switch (RGT_PlayerPrefs.playableVehicles.speedometerType[vehicleNumber])
        {
            case RG_DistanceMetrics.SpeedType.KilometerPerHour:
                uI.carSpeedText.text = ((RGT_PlayerPrefs.playableVehicles.vehicles[vehicleNumber].GetComponent<EVP.VehicleController>().maxSpeedForward + levelBonusTopSpeed) * 3.6f).ToString("F0") + " KPH";
                break;
            case RG_DistanceMetrics.SpeedType.MilesPerHour:
                uI.carSpeedText.text = ((RGT_PlayerPrefs.playableVehicles.vehicles[vehicleNumber].GetComponent<EVP.VehicleController>().maxSpeedForward + levelBonusTopSpeed) * 2.23694f).ToString("F0") + " MPH";
                break;
            case RG_DistanceMetrics.SpeedType.MeterPerSecond:
                uI.carSpeedText.text = (RGT_PlayerPrefs.playableVehicles.vehicles[vehicleNumber].GetComponent<EVP.VehicleController>().maxSpeedForward + levelBonusTopSpeed).ToString() + " MPS";
                break;
        }
#else
		switch(RGT_PlayerPrefs.playableVehicles.speedometerType[vehicleNumber])
		{
		case RG_DistanceMetrics.SpeedType.KilometerPerHour:
			uI.carSpeedText.text = (RGT_PlayerPrefs.playableVehicles.vehicles[vehicleNumber].GetComponent<RG_CarController>().topSpeed + levelBonusTopSpeed	).ToString() + " KPH";
			break;
		case RG_DistanceMetrics.SpeedType.MilesPerHour:
			uI.carSpeedText.text = (RGT_PlayerPrefs.playableVehicles.vehicles[vehicleNumber].GetComponent<RG_CarController>().topSpeed + levelBonusTopSpeed	).ToString() + " MPH";
			break;
		case RG_DistanceMetrics.SpeedType.MeterPerSecond:
			uI.carSpeedText.text = (RGT_PlayerPrefs.playableVehicles.vehicles[vehicleNumber].GetComponent<RG_CarController>().topSpeed + levelBonusTopSpeed	).ToString() + " MPS";
			break;
		}
#endif



        uI.topSpeedSlider.value = RGT_PlayerPrefs.playableVehicles.topSpeedLevel[vehicleNumber];
        uI.accelerationSlider.value = RGT_PlayerPrefs.playableVehicles.torqueLevel[vehicleNumber];
        uI.brakePowerSlider.value = RGT_PlayerPrefs.playableVehicles.brakeTorqueLevel[vehicleNumber];
        uI.tireTractionSlider.value = RGT_PlayerPrefs.playableVehicles.tireTractionLevel[vehicleNumber];
        uI.steerSensitivitySlidetr.value = RGT_PlayerPrefs.playableVehicles.steerSensitivityLevel[vehicleNumber];
        if (RGT_PlayerPrefs.playableVehicles.topSpeedLevel[vehicleNumber] >= 9)
        {
            uI.upgradeTopSpeedButton.SetActive(false);
        }
        else
        {
            uI.upgradeTopSpeedButton.SetActive(true);
        }
        if (RGT_PlayerPrefs.playableVehicles.torqueLevel[vehicleNumber] >= 9)
        {
            uI.upgradeAccelerationButton.SetActive(false);
        }
        else
        {
            uI.upgradeAccelerationButton.SetActive(true);
        }
        if (RGT_PlayerPrefs.playableVehicles.brakeTorqueLevel[vehicleNumber] >= 9)
        {
            uI.upgradeBrakePowerButton.SetActive(false);
        }
        else
        {
            uI.upgradeBrakePowerButton.SetActive(true);
        }
        if (RGT_PlayerPrefs.playableVehicles.tireTractionLevel[vehicleNumber] >= 9)
        {
            uI.upgradeTireTractionButton.SetActive(false);
        }
        else
        {
            uI.upgradeTireTractionButton.SetActive(true);
        }
        if (RGT_PlayerPrefs.playableVehicles.steerSensitivityLevel[vehicleNumber] >= 9)
        {
            uI.upgradeSteerSensitivityButton.SetActive(false);
        }
        else
        {
            uI.upgradeSteerSensitivityButton.SetActive(true);
        }
    }

    #region RaceSelection methods
    public void Button_RaceSelection()
    {
        if (uI.racesWindow.activeInHierarchy)
        {
            uI.racesWindow.SetActive(false);
            uI.singlePlayerModeWindow.SetActive(true);
            cameraOffset = cameraOffsetMenus;
            AudioMenuBack();
        }
        else
        {
            uI.singlePlayerModeWindow.SetActive(false);
            cameraOffset = cameraOffsetRaces;
            uI.racesWindow.SetActive(true);
            AudioMenuSelect();
        }
    }

    public void SelectRace()
    {
        AudioMenuSelect();
        if (!RGT_PlayerPrefs.raceData.raceLocked[raceNumber])
        {
            RGT_PlayerPrefs.raceData.vehicleNumber = RGT_PlayerPrefs.playableVehicles.currentVehicleNumber;
            //set game mode
            RGT_PlayerPrefs.SetGameMode("SINGLE PLAYER");
            uI.loadingWindow.SetActive(true);
            //set race number
            RGT_PlayerPrefs.SetRaceNumber(raceNumber);
            //set race rewards
            RGT_PlayerPrefs.SetRaceReward(1, firstPrize);
            RGT_PlayerPrefs.SetRaceReward(2, secondPrize);
            RGT_PlayerPrefs.SetRaceReward(3, thirdPrize);
            raceNameToLoad = raceNumber.ToString() + RGT_PlayerPrefs.raceData.raceNames[raceNumber];
            SceneManager.LoadScene(raceNameToLoad);
        }
        else
        {

        }
    }

    public void NextRace()
    {
        AudioMenuSelect();
        if (raceNumber < RGT_PlayerPrefs.raceData.numberOfRaces - 1)
        {
            raceNumber += 1;
        }
        else
        {
            raceNumber = 0;
        }
        UpdateRaceDetails();
    }

    public void PreviousRace()
    {
        AudioMenuSelect();
        if (raceNumber > 0)
        {
            raceNumber -= 1;
        }
        else
        {
            raceNumber = RGT_PlayerPrefs.raceData.numberOfRaces - 1;
        }
        UpdateRaceDetails();
    }

    public void UpdateRaceDetails()
    {
        raceImage.sprite = RGT_PlayerPrefs.raceData.raceImages[raceNumber];
        uI.raceNameText.text = RGT_PlayerPrefs.raceData.raceNames[raceNumber];
        uI.lapText.text = "Laps " + RGT_PlayerPrefs.raceData.raceLaps[raceNumber].ToString();
        uI.numberOfRacersText.text = "Racers " + RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber].ToString();
        if (!RGT_PlayerPrefs.raceData.raceLocked[raceNumber])
        {
            uI.raceDetailsWindow.SetActive(true);
            uI.raceDetails.SetActive(true);//check
            uI.selectRaceText.text = "Select Race";
            uI.raceLockedIcon.SetActive(false);
        }
        else
        {
            uI.raceDetails.SetActive(false);//check
            uI.unlockLevelButtonText.text = "Unlock\n" + RGT_PlayerPrefs.raceData.unlockAmount[raceNumber].ToString("N0") + " Coins";
            uI.selectRaceText.text = RGT_PlayerPrefs.raceData.lockButtonText;
            uI.raceLockedIcon.SetActive(true);
            uI.unlockLevelText.text = "Unlock " + RGT_PlayerPrefs.raceData.raceNames[raceNumber] + "\nfor\n" + RGT_PlayerPrefs.raceData.unlockAmount[raceNumber].ToString("N0") + " Coins";
        }
        CalculateRewardText();
    }

    public void UnlockRaceButton()
    {
        if (currency >= RGT_PlayerPrefs.raceData.unlockAmount[raceNumber])
        {
            uI.unlockRaceConfirmWindow.SetActive(true);
        }
    }

    public void AcceptPurchaseUnlockRace()
    {
        AudioMenuSelect();
        //set currency
        RGT_PlayerPrefs.SetCurrency(currency - RGT_PlayerPrefs.raceData.unlockAmount[raceNumber]);
        //set race lock
        RGT_PlayerPrefs.SetRaceLock(RGT_PlayerPrefs.raceData.raceNames[raceNumber], "UNLOCKED");
        RGT_PlayerPrefs.raceData.raceLocked[raceNumber] = false;
        uI.raceLockedIcon.SetActive(false);
        uI.raceDetailsWindow.SetActive(true);
        uI.selectRaceText.text = "Select Race";
        uI.unlockRaceConfirmWindow.SetActive(false);
        UpdateCurrency();
    }

    public void LapIncrease()
    {
        if (RGT_PlayerPrefs.raceData.raceLaps[raceNumber] < RGT_PlayerPrefs.raceData.lapLimit[raceNumber])
        {
            RGT_PlayerPrefs.raceData.raceLaps[raceNumber] += 1;
        }
        else
        {
            RGT_PlayerPrefs.raceData.raceLaps[raceNumber] = 1;
        }
        uI.lapText.text = "Laps\n" + RGT_PlayerPrefs.raceData.raceLaps[raceNumber].ToString();
        CalculateRewardText();
        //set race laps
        RGT_PlayerPrefs.SetRaceLaps(raceNumber, RGT_PlayerPrefs.raceData.raceLaps[raceNumber]);
    }

    public void LapDecrease()
    {
        if (RGT_PlayerPrefs.raceData.raceLaps[raceNumber] > 1)
        {
            RGT_PlayerPrefs.raceData.raceLaps[raceNumber] -= 1;
        }
        else
        {
            RGT_PlayerPrefs.raceData.raceLaps[raceNumber] = RGT_PlayerPrefs.raceData.lapLimit[raceNumber];
        }
        uI.lapText.text = "Laps\n" + RGT_PlayerPrefs.raceData.raceLaps[raceNumber].ToString();
        CalculateRewardText();
        RGT_PlayerPrefs.SetRaceLaps(raceNumber, RGT_PlayerPrefs.raceData.raceLaps[raceNumber]);
    }

    public void NumberOfRacersIncrease()
    {
        if (RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] < RGT_PlayerPrefs.raceData.racerLimit[raceNumber])
        {
            RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] += 1;
        }
        else
        {
            RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] = 1;
        }
        uI.numberOfRacersText.text = "Racers\n" + RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber].ToString();
        CalculateRewardText();
    }

    public void NumberOfRacersDecrease()
    {
        if (RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] > 1)
        {
            RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] -= 1;
        }
        else
        {
            RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] = RGT_PlayerPrefs.raceData.racerLimit[raceNumber];
        }
        uI.numberOfRacersText.text = "Racers\n" + RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber].ToString();
        CalculateRewardText();
    }

    public void LoadOpenWorldButton()
    {
        uI.loadingWindow.SetActive(true);
        //set game mode
        RGT_PlayerPrefs.SetGameMode("SINGLE PLAYER");
        SceneManager.LoadScene(openWorldName);
    }
    #endregion


    public void Button_MultiplayerGame()
    {
        if (uI.multiplayerWindow.activeInHierarchy)
        {
            cameraOffset = cameraOffsetMenus;
            uI.multiplayerWindow.SetActive(false);
            uI.multiplayerModeWindow.SetActive(true);
            AudioMenuSelect();
        }
        else
        {
            cameraOffset = 0;
            uI.multiplayerModeWindow.SetActive(false);
            //set game mode
            RGT_PlayerPrefs.SetGameMode("MULTIPLAYER");
            uI.multiplayerWindow.SetActive(true);
            AudioMenuBack();
        }
    }

    public void Button_BackLobby()
    {
        uI.multiplayerWindow.SetActive(false);
        uI.multiplayerModeWindow.SetActive(true);
    }

    public void ReloadGarageScene()
    {

    }

    void Reload()
    {
        //set host drop
        RGT_PlayerPrefs.SetHostDrop("SCENERELOADED");
        Destroy(lobbyManager);
        //SceneManager.LoadScene ("Garage");
    }

    void CalculateRewardText()
    {
        if (RGT_PlayerPrefs.GetRaceStatus(RGT_PlayerPrefs.raceData.raceNames[raceNumber]) == "COMPLETE" && !RGT_PlayerPrefs.raceData.unlimitedRewards[raceNumber])
        {
            firstPrize = 0;
            secondPrize = 0;
            thirdPrize = 0;
        }
        else
        {
            firstPrize = (RGT_PlayerPrefs.raceData.raceLaps[raceNumber] - 1) * RGT_PlayerPrefs.raceData.extraLapRewardMultiplier[raceNumber] * RGT_PlayerPrefs.raceData.firstPrize[raceNumber];
            firstPrize += (RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] - 1) * RGT_PlayerPrefs.raceData.extraRacerRewardMultiplier[raceNumber] * RGT_PlayerPrefs.raceData.firstPrize[raceNumber];
            firstPrize += RGT_PlayerPrefs.raceData.firstPrize[raceNumber];
            if (RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] > 1)
            {
                secondPrize = (RGT_PlayerPrefs.raceData.raceLaps[raceNumber] - 1) * RGT_PlayerPrefs.raceData.extraLapRewardMultiplier[raceNumber] * RGT_PlayerPrefs.raceData.secondPrize[raceNumber];
                secondPrize += (RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] - 1) * RGT_PlayerPrefs.raceData.extraRacerRewardMultiplier[raceNumber] * RGT_PlayerPrefs.raceData.secondPrize[raceNumber];
                secondPrize += RGT_PlayerPrefs.raceData.secondPrize[raceNumber];
            }
            else
            {
                secondPrize = 0;
            }
            if (RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] > 2)
            {
                thirdPrize = (RGT_PlayerPrefs.raceData.raceLaps[raceNumber] - 1) * RGT_PlayerPrefs.raceData.extraLapRewardMultiplier[raceNumber] * RGT_PlayerPrefs.raceData.thirdPrize[raceNumber];
                thirdPrize += (RGT_PlayerPrefs.raceData.numberOfRacers[raceNumber] - 1) * RGT_PlayerPrefs.raceData.extraRacerRewardMultiplier[raceNumber] * RGT_PlayerPrefs.raceData.thirdPrize[raceNumber];
                thirdPrize += RGT_PlayerPrefs.raceData.thirdPrize[raceNumber];
            }
            else
            {
                thirdPrize = 0;
            }
        }
        uI.rewardText.text = "\n" + firstPrize.ToString("N0") + " Coins" + "\n" + secondPrize.ToString("N0") + " Coins" + "\n" + thirdPrize.ToString("N0") + " Coins";
    }

    public void UpdateCurrency()
    {
        currency = RGT_PlayerPrefs.GetCurrency();
        uI.currencyText.text = currency.ToString("N0");
    }

    public void UpdatePoints()
    {
        score = RGT_PlayerPrefs.GetPoints();
        uI.pointsText.text = score.ToString();
    }

    public void UpdatePlayerDetails()
    {
        if (GameSparksManager.Instance().TeamName == "AdminEntyvio" || GameSparksManager.Instance().TeamName == "AdminAdcetris")
        {
            uI.pointsText.text = "Points: 0";
        }
        else
        {
            uI.pointsText.text = "Points: " + RGT_PlayerPrefs.GetPoints().ToString();
        }

        uI.playerNameText.text = GameSparksManager.Instance().DisplayName;

    }

    public void EnableLoading(string fLoadingText)
    {
        uI.connectingWindowText.text = fLoadingText;
        uI.connectingWindow.SetActive(true);
    }

    public void DisableLoading()
    {
        uI.connectingWindow.SetActive(false);
    }

    public void FormatSelectedColors()
    {
        for (int i = 0; i < uI.carBodyColorButtons.Length; i++)
        {

            uI.carBodyColorButtons[i].interactable = true;


            uI.carGlassColorButtons[i].interactable = true;


            uI.carBrakeColorButtons[i].interactable = true;


            uI.carRimColorButtons[i].interactable = true;


            uI.carNeonColorButtons[i].interactable = true;
        }
    }

}