﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnScreenLog : MonoBehaviour
{
    private static OnScreenLog instance = null;
    //public GameObject GameConsole;
    //public ScrollRect ScrollLog;
    //public Text GameConsoleText;
    string myLog;
    Queue myLogQueue = new Queue();
    GUIStyle Style = new GUIStyle();
    bool EnabledText = false;
    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
        Style.normal.textColor = Color.red;
        Style.fontSize = 30;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }
    private void Awake()
    {

        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        if (EnabledText)
        {
            //GameConsole.SetActive(true);
        }
        else
        {
            //GameConsole.SetActive(false);
        }

#if UNITY_STANDALONE_WIN
        //Screen.SetResolution(720, 405, false);
        //Debug.LogWarning("change resolution to 720x405p");
#endif

    }
    void HandleLog(string logString, string stackTrace, LogType type)
    {
        myLog = logString;

        if (type == LogType.Error || type == LogType.Exception)
        {
            string newString = " <color=red> " + "\n [" + type + "] : " + myLog + "</color>";
            myLogQueue.Enqueue(newString);

            newString = " <color=red> " + "\n" + stackTrace + "</color>";
            myLogQueue.Enqueue(newString);
        }
        else if (type == LogType.Warning)
        {
            string newString = " <color=orange> " + "\n [" + type + "] : " + myLog + "</color>";
            myLogQueue.Enqueue(newString);
        }
        else
        {
            string newString = "\n [" + type + "] : " + myLog;
            myLogQueue.Enqueue(newString);
        }
        myLog = string.Empty;
        foreach (string mylog in myLogQueue)
        {
            myLog += mylog;
        }
        myLog = Truncate(myLog, 2000);
        //GameConsoleText.text = myLog;
        //ScrollLog.verticalScrollbar.value = 0;
    }
    public static string Truncate(string value, int maxLength)
    {
        if (string.IsNullOrEmpty(value)) return value;
        return value.Length <= maxLength ? value : value.Substring(value.Length - maxLength, maxLength);
    }
    public void Toggle()
    {
        if (EnabledText)
        {
            //GameConsole.SetActive(false);
            EnabledText = false;
        }
        else
        {
            //GameConsole.SetActive(true);
            EnabledText = true;
        }
    }

    void OnGUI()
    {
        GUILayout.Label(myLog);
    }
}
