﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitBehaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("ExitGame", 3.0f);
	}
	
	void ExitGame()
    {
        Debug.Log("Exiting");
        Application.Quit();
    }
}
