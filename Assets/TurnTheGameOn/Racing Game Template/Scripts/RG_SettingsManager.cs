﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TurnTheGameOn.RacingGameTemplate;

public class RG_SettingsManager : MonoBehaviour
{

    public Toggle audioToggle;
    public Text qualityText;
    public string qualityTextPrefix;
    public GameObject mobileSteering;
    public Dropdown mobileSteeringdropdown;
    public GameObject mobileControls;

    void Start()
    {
        if (RGT_PlayerPrefs.inputData.useMobileController)
        {
            //mobileSteering.SetActive(true);
            //mobileSteeringdropdown.value = RGT_PlayerPrefs.GetMobileSteeringType();

            mobileControls.SetActive(true);
            UpdateToggleValues();

        }
        else {
            //mobileSteering.SetActive(false);
            mobileControls.SetActive(false);
        }
        if (RGT_PlayerPrefs.GetAudio() == "OFF") audioToggle.isOn = false;
        if (RGT_PlayerPrefs.GetAudio() == "ON") audioToggle.isOn = true;
        int qualityNumber = RGT_PlayerPrefs.GetQualitySettings();
        QualitySettings.SetQualityLevel(qualityNumber, true);
        qualityText.text = qualityTextPrefix + QualitySettings.names[QualitySettings.GetQualityLevel()];
        UpdateAudio();
    }

    public void UpdateAudio()
    {
        if (audioToggle.isOn)
        {
            AudioListener.pause = false;
            RGT_PlayerPrefs.SetAudio("ON");
        }
        else {
            AudioListener.pause = true;
            RGT_PlayerPrefs.SetAudio("OFF");
        }
    }

    public void QualityUp()
    {
        QualitySettings.IncreaseLevel();
        qualityText.text = qualityTextPrefix + QualitySettings.names[QualitySettings.GetQualityLevel()];
        RGT_PlayerPrefs.SetQualitySettings(QualitySettings.GetQualityLevel());
    }

    public void QualityDown()
    {
        QualitySettings.DecreaseLevel();
        qualityText.text = qualityTextPrefix + QualitySettings.names[QualitySettings.GetQualityLevel()];
        RGT_PlayerPrefs.SetQualitySettings(QualitySettings.GetQualityLevel());
    }

    //public void UpdateMobileSteering()
    //{
    //    for (int i = 0; i < mobileSteeringdropdown.options.Count; i++)
    //    {
    //        if (mobileSteeringdropdown.value == i)
    //        {
    //            RGT_PlayerPrefs.SetMobileSteeringType(i);
    //        }
    //    }
    //}

    public void ToggleUIButtonsControls(bool fIsOn)
    {
        if (fIsOn)
        {
            RGT_PlayerPrefs.SetMobileSteeringType(0);
        }

    }

    public void ToggleTiltControls(bool fIsOn)
    {
        if (fIsOn)
        {
            RGT_PlayerPrefs.SetMobileSteeringType(1);
        }

    }

    public void ToggleJoystickControls(bool fIsOn)
    {
        if (fIsOn)
        {
            RGT_PlayerPrefs.SetMobileSteeringType(2);
        }

    }

    public void UpdateToggleValues()
    {
        for (int i = 0; i < mobileControls.transform.childCount; i++)
        {
            if (RGT_PlayerPrefs.GetMobileSteeringType() == i)
            {
                mobileControls.transform.GetChild(i).GetChild(0).GetComponent<Toggle>().isOn = true;
            }
            else
            {
                mobileControls.transform.GetChild(i).GetChild(0).GetComponent<Toggle>().isOn = false;
            }
        }
    }

}