﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRecordUI : MonoBehaviour
{

    public Text playerRecordText;
    public PlayerRecord myRecord;

    public void SetRecordForView(PlayerRecord fMyRecord)
    {
        myRecord = fMyRecord;
        playerRecordText.text = "PlayerName: " + fMyRecord.DisplayName + 
                                "\n Question# " + fMyRecord.QuestionNumber + 
                                "\n ChosenAnswer# " + fMyRecord.ChosenAnswer + 
                                "\n Correct: " + (fMyRecord.Correct == 0 ? "No" : "Yes")+
                                "\n Time: "+fMyRecord.Time;
    }

    public void SetRecordForLeaderboardView(TakedaPlayer fMyRecord)
    {
        //myRecord = fMyRecord;
        playerRecordText.text = "Name: " + fMyRecord.DisplayName+
                                ", Points: "+fMyRecord.Points;
    }

}
