﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionUI : MonoBehaviour
{

    public Text questionText;
    public QuestionType myQuestion;
    private Toggle myToggle;
    void Start()
    {
        myToggle = GetComponent<Toggle>();
        if (myToggle != null)
        {
            //Debug.Log("toggle found.");
            myToggle.isOn = false;
            myToggle.onValueChanged.AddListener((ToggleValue) =>
            {
                OnValueChanged(ToggleValue);
            });
        }
    }

    public void SetQuestionForPlayerView(QuestionType fMyQuestion)
    {
        myQuestion = fMyQuestion;
        string allAnswers = "";
        foreach (var answer in fMyQuestion.Answers)
        {
            allAnswers += answer;
        }
        questionText.text = "Question#" + fMyQuestion.ID + ", Team:" + fMyQuestion.Team + ", Type:" + fMyQuestion.MyQuestionType + ", Question:" + fMyQuestion.Question + " " + allAnswers;
    }

    public void SetQuestionsForAdminView(QuestionType fMyQuestion)
    {
        myQuestion = fMyQuestion;
        string allAnswers = "";
        foreach (var answer in fMyQuestion.Answers)
        {
            allAnswers += "\n -";
            allAnswers += answer;
        }
        questionText.text = "<b>Question#</b>" + fMyQuestion.ID + "\n Team:" + fMyQuestion.Team + "\n Type:" + fMyQuestion.MyQuestionType + "\n <b>Question:</b>" + fMyQuestion.Question + " " + allAnswers + "\n Right Answer:" + fMyQuestion.RightAnswer;
    }

    private void OnValueChanged(bool fToggleValue)
    {
        if (fToggleValue)
        {
            RG_GarageManager.Instance().QuestionSelected(myQuestion);
        }
        else
        {
            RG_GarageManager.Instance().QuestionUnSelected(myQuestion);
        }
    }
}
