﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class Intro : MonoBehaviour
{
    public GameObject loadingPrefab;
    public GameObject ButtonYes, ButtonNo;
    //public GameObject myVideo;
    private bool isPlaying = false;
    // Use this for initialization
    void Start()
    {
        //Invoke("SkipVideo", 2.0f);
    }


    public void StartVideo()
    {
        ButtonYes.GetComponent<Button>().interactable = false;
        ButtonYes.SetActive(false);
        ButtonNo.GetComponent<Button>().interactable = false;
        ButtonNo.SetActive(false);

        //myVideo.GetComponent<VideoPlayer>().Play();

        IOSVideoPlayerBinding.instance.shouldPauseUnity(false);
        IOSVideoPlayerBinding.instance.setMovieScalingMode(VideoScalingMode.Fill);
        IOSVideoPlayerBinding.instance.addSkipButton("Reset", 500, 500);
        IOSVideoPlayerBinding.instance.PlayVideo("Intro.mp4");
        isPlaying = true;

        //loadingPrefab.SetActive(true);
       
    }

    public void StopVideo()
    {
        //myVideo.GetComponent<VideoPlayer>().Stop();
        isPlaying = false;
        SceneManager.LoadScene("Garage");
    }

    public void SkipVideo()
    {
        ButtonYes.GetComponent<Button>().interactable = false;
        ButtonYes.SetActive(false);
        ButtonNo.GetComponent<Button>().interactable = false;
        ButtonNo.SetActive(false);

        IOSVideoPlayerBinding.instance.LoadLevel();
    }
}
