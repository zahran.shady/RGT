﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionType
{

    private int iD;
    private string _question;
    private List<string> answers = new List<string>();
    private int rightAnswer;
    private int chosenAnswer;
    private int correct;
    private string myQuestionType;
    private string team;
    private int published;

    public int ID
    {
        get
        {
            return iD;
        }

        set
        {
            iD = value;
        }
    }

    public List<string> Answers
    {
        get
        {
            return answers;
        }

        set
        {
            answers = value;
        }
    }

    public int RightAnswer
    {
        get
        {
            return rightAnswer;
        }

        set
        {
            rightAnswer = value;
        }
    }

    public int Published
    {
        get
        {
            return published;
        }

        set
        {
            published = value;
        }
    }

    public string Question
    {
        get
        {
            return _question;
        }

        set
        {
            _question = value;
        }
    }

    public string Team
    {
        get
        {
            return team;
        }

        set
        {
            team = value;
        }
    }

    public string MyQuestionType
    {
        get
        {
            return myQuestionType;
        }

        set
        {
            myQuestionType = value;
        }
    }

    public int ChosenAnswer
    {
        get
        {
            return chosenAnswer;
        }

        set
        {
            chosenAnswer = value;
        }
    }

    public int Correct
    {
        get
        {
            return correct;
        }

        set
        {
            correct = value;
        }
    }

    public override string ToString()
    {
        string result = "";

        result += "_id: " + ID;
        result += " team: " + Team;
        result += " type: " + MyQuestionType;
        result += " question: " + Question;
        result += " answers:";

        foreach (var answer in Answers)
        {
            result += " " + answer;
        }
        result += " rightanswer: " + RightAnswer;
        
        result += " published: " + Published;

        return result;
    }
}
