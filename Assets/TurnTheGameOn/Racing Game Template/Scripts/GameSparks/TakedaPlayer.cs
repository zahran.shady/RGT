﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakedaPlayer
{

    private string userName;
    private string displayName;
    private string team;
    private int rank;
    private int points;
    private List<PlayerRecord> myHistory;

    public List<PlayerRecord> MyHistory
    {
        get
        {
            return myHistory;
        }

        set
        {
            myHistory = value;
        }
    }

    public string UserName
    {
        get
        {
            return userName;
        }

        set
        {
            userName = value;
        }
    }

    public string DisplayName
    {
        get
        {
            return displayName;
        }

        set
        {
            displayName = value;
        }
    }

    public int Rank
    {
        get
        {
            return rank;
        }

        set
        {
            rank = value;
        }
    }

    public int Points
    {
        get
        {
            return points;
        }

        set
        {
            points = value;
        }
    }

    public string Team
    {
        get
        {
            return team;
        }

        set
        {
            team = value;
        }
    }

    public TakedaPlayer(string fUserName, string fDisplayName, string fTeam, int frank, int fpoints)
    {
        UserName = fUserName;
        DisplayName = fDisplayName;
        Team = fTeam;
    }

    public override string ToString()
    {
        return "Username: " + UserName
            + " Displayname: " + DisplayName
            + " Team: " + Team
            + " Rank: " + Rank
            + " Points: " + Points;
    }
}
