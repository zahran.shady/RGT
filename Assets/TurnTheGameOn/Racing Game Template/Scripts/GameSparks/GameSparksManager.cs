﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using GameSparks.RT;
using TurnTheGameOn.RacingGameTemplate;
using UnityEngine.UI;
using System.Linq;

public class GameSparksManager : MonoBehaviour
{

    private static GameSparksManager instance = null;
    private string userName = "";
    private string displayName = "";
    private string password = "";
    private string userID = "";
    private string teamName = "";
    private List<QuestionType> questionsBank;
    private List<QuestionType> questionsSheet;
    private List<PlayerRecord> playersHistory;
    private List<PlayerRecord> scoreLeaderboard;
    private QuestionType currentQuestion;
    private List<TakedaPlayer> players;
    public string Password
    {
        get
        {
            return password;
        }

        set
        {
            password = value;
        }
    }

    public string DisplayName
    {
        get
        {
            return displayName;
        }

        set
        {
            displayName = value;
        }
    }

    public string UserName
    {
        get
        {
            return userName;
        }

        set
        {
            userName = value;
        }
    }

    public string UserID
    {
        get
        {
            return userID;
        }

        set
        {
            userID = value;
        }
    }

    public string TeamName
    {
        get
        {
            return teamName;
        }

        set
        {
            teamName = value;
        }
    }

    public List<QuestionType> QuestionsBank
    {
        get
        {
            return questionsBank;
        }

        set
        {
            questionsBank = value;
        }
    }

    public QuestionType CurrentQuestion
    {
        get
        {
            return currentQuestion;
        }

        set
        {
            currentQuestion = value;
        }
    }

    public List<QuestionType> QuestionsSheet
    {
        get
        {
            return questionsSheet;
        }

        set
        {
            questionsSheet = value;
        }
    }

    public List<PlayerRecord> PlayersHistory
    {
        get
        {
            return playersHistory;
        }

        set
        {
            playersHistory = value;
        }
    }

    public List<PlayerRecord> ScoreLeaderboard
    {
        get
        {
            return scoreLeaderboard;
        }

        set
        {
            scoreLeaderboard = value;
        }
    }

    public List<TakedaPlayer> Players
    {
        get
        {
            return players;
        }

        set
        {
            players = value;
        }
    }

    public static GameSparksManager Instance()
    {
        if (instance != null)
        {
            return instance; // return the singleton if the instance has been setup
        }
        else
        { // otherwise return an error
            Debug.LogWarning("GSM| GameSparksManager Not Initialized...");
        }
        return null;
    }

    public delegate void AuthCallback(AuthenticationResponse _authresp);
    public delegate void RegCallback(RegistrationResponse _regResp);

    public GameObject connectingWindow;
    public Text connectingWindowText;

    public Text questionText;
    private List<Text> answersTextList;
    public Text answer1Text;
    public Text answer2Text;
    public Text answer3Text;
    public Text answer4Text;
    public Text answer5Text;
    public Text answer6Text;
    public Text pointsText;
    public Text timerText;
    public Image timerImage;
    public GameObject questionsWindow;
    public GameObject confirmationWindow;
    public Text confirmationText;

    private float _timer = 0;
    private bool isCounting = false;

    //public RG_GarageManager myGarageManager;
    //private RG_GarageManager myGarageManager;

    public delegate void QBankCallBack();

    void Awake()
    {
        if (instance == null)
        {
            instance = this;// if not, give it a reference to this class...
            DontDestroyOnLoad(this.gameObject); // and make this object persistent as we load new scenes
        }
        else if (instance != this)
        {
            DestroyImmediate(gameObject);
            //return;
        }
    }

    // Use this for initialization
    void Start()
    {
        if (GameSparks.Core.GS.Available)
        {
            connectingWindow.SetActive(false);

        }
        else
        {
            connectingWindowText.text = "Connecting...";
            connectingWindow.SetActive(true);
        }

        if (RG_GarageManager.Instance() != null)
        {
            if (GameSparksManager.Instance().isPlayerAuthenticated())
            {
                RG_GarageManager.Instance().uI.startMenuWindow.SetActive(true);
                RG_GarageManager.Instance().uI.login.SetActive(false);
                RG_GarageManager.Instance().UpdatePlayerDetails();
            }
            else
            {
                RG_GarageManager.Instance().uI.login.SetActive(true);
                RG_GarageManager.Instance().uI.startMenuWindow.SetActive(false);
                //AudioMenuSelect();
            }
        }
        else if (RG_SceneManager.Instance() != null)
        {

        }


        GS.GameSparksAvailable = (isAvailable) =>
        {
            if (isAvailable)
            {
                Debug.Log("GS Available.");
                if (connectingWindow != null)
                {
                    connectingWindow.SetActive(false);
                    //GameSparksManager.Instance().RegisterPlayer("shadyz", "123", "shady zahran", "Adecytris");
                }

                //myGarageManager.ConnectingWindow(false);
                //connectionStatus.text = "Game Connected...";
                //AuthenticatePlayer();
            }
            else
            {
                Debug.Log("GS Disconnected.");

                if (connectingWindow != null)
                {
                    connectingWindowText.text = "Connecting...";
                    connectingWindow.SetActive(true);
                }
            }
        };
        QuestionsBank = new List<QuestionType>();
        CurrentQuestion = null;
        QuestionsSheet = new List<QuestionType>();
        PlayersHistory = new List<PlayerRecord>();
        ScoreLeaderboard = new List<PlayerRecord>();
        Players = new List<TakedaPlayer>();


        answersTextList = new List<Text>();
        answersTextList.Add(answer1Text);
        answersTextList.Add(answer2Text);
        answersTextList.Add(answer3Text);
        answersTextList.Add(answer4Text);
        answersTextList.Add(answer5Text);
        answersTextList.Add(answer6Text);
    }

    void Update()
    {
        if (isCounting)
        {
            _timer += Time.unscaledDeltaTime;
            timerText.text = ((int)_timer).ToString();
            timerText.color = CalculateColor(_timer);
            pointsText.text = "+" + CalculateScore(_timer).ToString() + " pt";
            pointsText.color = timerText.color;
            timerImage.fillAmount = CalculateTime(_timer);
            //Debug.Log("Timer:" + _timer);
        }
    }

    #region Requests
    public void RegisterPlayer(string _userName, string _password, string _displayName, string _teamName)
    {
        new GameSparks.Api.Requests.RegistrationRequest()
            .SetDisplayName(_displayName)
            .SetPassword(_password)
            .SetUserName(_userName)
            .SetDurable(true)
            .Send
            ((response0) =>
                {
                    if (!response0.HasErrors)
                    {
                        Debug.Log("Player registered");
                        Debug.Log(response0.BaseData.JSON);
                        new GameSparks.Api.Requests.LogEventRequest()
                            .SetEventKey("RegisterPlayer")
                            .SetEventAttribute("UserName", _userName)
                            .SetEventAttribute("Password", _password)
                            .SetEventAttribute("DisplayName", _displayName)
                            .SetEventAttribute("Team", _teamName)
                            .Send
                            ((response1) =>
                            {
                                if (!response1.HasErrors)
                                {
                                    Debug.Log("Player details initialized");
                                    Debug.Log(response1.BaseData.JSON);
                                }
                                else
                                {
                                    Debug.Log("Error inittializing Player details");
                                    Debug.Log(response1.BaseData.JSON);
                                }
                            }
                            );
                        //Debug.Log("Player Registered");
                    }
                    else
                    {
                        Debug.Log("Error Registering Player");
                    }
                }
            );




    }

    public void AuthenticatePlayer(string fUserName, string fPassword, AuthCallback _authSuccessCallback, AuthCallback _authFailedCallback)
    {
        new GameSparks.Api.Requests.AuthenticationRequest()
            .SetPassword(fPassword)
            .SetUserName(fUserName)
            .SetDurable(true)
            .Send((response) =>
        {
            if (!response.HasErrors)
            {

                Debug.Log("Player Authenticated...");
                _authSuccessCallback(response);
                UserName = fUserName;
                Password = fPassword;
                UserID = response.JSONData["userId"] as string;
                Debug.Log(UserID);

                GetMyPlayerDetails(UserName);
                RG_GarageManager.Instance().UpdatePlayerDetails();

                GetQuestionsBank();


                GetAllPlayersDetails();
                //RegisterPlayer("moemenm", "123", "Moemen Moharam", "Entyvio");
                //RegisterPlayer("mohamede", "123", "Mohamed Emam", "Entyvio");
                //RegisterPlayer("rimag", "123", "Rima Gharbieh", "Entyvio");
                //RegisterPlayer("ahmedd", "123", "Ahmed Hatem Darwish", "Entyvio");

                //RegisterPlayer("ahmedf", "123", "Ahmed Hany Farghaly", "Adcetris");
                //RegisterPlayer("mohamedu", "123", "Mohamed Usama", "Adcetris");
                //RegisterPlayer("josetted", "123", "Josette Daou", "Adcetris");
                //RegisterPlayer("lebanon", "123", "Lebanon Onco PS", "Adcetris");

                //RegisterPlayer("salwa", "123", "salwa", "Adecytris");
                //RegisterPlayer("player1", "123", "player 1", "Adecytris");
                //RegisterPlayer("player2", "123", "player 2", "Adecytris");
                //RegisterPlayer("player3", "123", "player 3", "Adecytris");
                //RegisterPlayer("player4", "123", "player 4", "Entyvio");
                //RegisterPlayer("player5", "123", "player 5", "Entyvio");
                //RegisterPlayer("player6", "123", "player 6", "Entyvio");
                //RegisterPlayer("player7", "123", "player 7", "Entyvio");
                //IsQuestionAnswered(512);
                //UpdateScore(253); //old + reward.
                //GetQuestion(2);
                //InitializeQuestion("ahmed", 512);
                //myGarageManager.PlayerAuthenticated();


                //RegisterPlayer("adcetris1", "123", "adcetris 1", "Adecytris");
                //RegisterPlayer("adcetris2", "123", "adcetris 2", "Adecytris");
                //RegisterPlayer("adcetris3", "123", "adcetris 3", "Adecytris");
                //RegisterPlayer("adcetris4", "123", "adcetris 4", "Adecytris");
                //RegisterPlayer("adcetris5", "123", "adcetris 5", "Adecytris");
                //RegisterPlayer("adcetris6", "123", "adcetris 6", "Adecytris");
                //RegisterPlayer("adcetris7", "123", "adcetris 7", "Adecytris");

                //RegisterPlayer("entyvio1", "123", "entyvio 1", "Entyvio");
                //RegisterPlayer("entyvio2", "123", "entyvio 2", "Entyvio");
                //RegisterPlayer("entyvio3", "123", "entyvio 3", "Entyvio");
                //RegisterPlayer("entyvio4", "123", "entyvio 4", "Entyvio");
                //RegisterPlayer("entyvio5", "123", "entyvio 5", "Entyvio");
                //RegisterPlayer("entyvio6", "123", "entyvio 6", "Entyvio");
                //RegisterPlayer("entyvio7", "123", "entyvio 7", "Entyvio");
            }
            else
            {
                Debug.Log("Error Authenticating Player...");
                _authFailedCallback(response);
            }
        });
    }
    #endregion

    #region Events
    public void GetMyPlayerDetails(string fUserName)
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("GetPlayerDetails")
            .SetEventAttribute("UserName", fUserName)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("player details retreived.");

                    DisplayName = response.ScriptData.GetString("displayName");
                    TeamName = response.ScriptData.GetString("teamName");
                    RG_GarageManager.Instance().UpdatePlayerDetails();
                    if (TeamName != "")
                    {
                        if (TeamName == "AdminAdcetris")
                        {
                            Debug.Log("AdminAdecytris");
                            GetQuestionsSheet();

                        }
                        else if (TeamName == "AdminEntyvio")
                        {
                            Debug.Log("AdminEntyvio");
                            GetQuestionsSheet();
                        }
                        else
                        {
                            Debug.Log("not admin!");
                            GetScore();
                        }
                    }

                }
                else
                {
                    Debug.Log("Error retreiving player details.");
                    //Debug.Log(response.ScriptData.JSON);
                }
            }
            );
    }

    public void GetAllPlayersDetails()
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("GetAllPlayersDetails")
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {
                    List<GSData> tempDataList = response.ScriptData.GetGSDataList("AllPlayers");
                    if (tempDataList.Count < 1)
                    {
                        Debug.Log("No players details found.");
                    }
                    else
                    {
                        Debug.Log(tempDataList.Count + " player details retreived.");
                        foreach (var player in tempDataList)
                        {
                            TakedaPlayer _player = new TakedaPlayer(player.GetString("UserName"), player.GetString("DisplayName"), player.GetString("Team"), 0, 0);

                            Players.Add(_player);
                        }
                        GetLeaderboard("Score_Leaderboard");
                    }
                }
                else
                {
                    Debug.Log("Error retreiving players details.");
                }
            }
            );
    }

    public void GetQuestion(int questionID)
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("GetQuestionByID")
            .SetEventAttribute("number", questionID)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {

                    if (response.ScriptData.GetGSDataList("Question").Count < 1)
                    {
                        Debug.Log("No Question found with that ID.");
                    }
                    else
                    {
                        Debug.Log("Question retreived.");
                        Debug.Log(response.ScriptData.GetGSDataList("Question")[0].GetString("question"));
                    }
                }
                else
                {
                    Debug.Log("Error retreiving question.");
                    //Debug.Log(response.ScriptData.JSON);
                }
            }
            );
    }

    public void InitializeQuestion(int fQuestionNumber)
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("InitializeQuestion")
            .SetEventAttribute("UserName", UserName)
            .SetEventAttribute("DisplayName", DisplayName)
            .SetEventAttribute("QuestionNumber", fQuestionNumber)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (response.HasErrors)
                {
                    Debug.Log("Error initializing question.");
                }
                else
                {
                    string responseString = response.ScriptData.GetString("response");

                    if (responseString.Length < 1)
                    {
                        Debug.Log("Response Empty.");
                    }
                    else
                    {
                        Debug.Log(responseString);
                        FormatQuestionWindow(CurrentQuestion);
                        DisableLoading();
                        questionsWindow.SetActive(true);
                        EnableTimer();
                    }


                }
            }
            );
    }

    public void SubmitAnswer(int fChosenAnswer)
    {
        DisableTimer();
        CurrentQuestion.ChosenAnswer = fChosenAnswer;
        if (CurrentQuestion.ChosenAnswer == CurrentQuestion.RightAnswer)
        {

        }
        CurrentQuestion.Correct = (CurrentQuestion.ChosenAnswer == CurrentQuestion.RightAnswer ? 1 : 0);
        Debug.Log("Sending: " + UserName + " " + CurrentQuestion.ID + " " + fChosenAnswer + " " + CurrentQuestion.Correct);
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("SubmitAnswer")
            .SetEventAttribute("UserName", UserName)
            .SetEventAttribute("QuestionNumber", CurrentQuestion.ID)
            .SetEventAttribute("ChosenAnswer", CurrentQuestion.ChosenAnswer)
            .SetEventAttribute("Correct", CurrentQuestion.Correct)
            .SetEventAttribute("Time", (int)_timer)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (response.HasErrors)
                {
                    Debug.Log("Error submiting answer.");
                }
                else
                {
                    string responseString = response.ScriptData.GetString("response");

                    if (responseString.Length < 1)
                    {
                        Debug.Log("Response Empty.");
                    }
                    else
                    {
                        Debug.Log(responseString);
                        DisableLoading();
                        if (CurrentQuestion.ChosenAnswer == CurrentQuestion.RightAnswer)
                        {
                            UpdatePoints(RGT_PlayerPrefs.GetPoints() + (10 + CalculateScore(_timer)));
                            ConfirmationWindow("Correct answer " + (10 + CalculateScore(_timer)) + " points.");
                        }
                        else
                        {
                            ConfirmationWindow("Incorrect answer.");
                        }
                    }
                }
            }
            );
    }

    public void GetLeaderboard(string fLeaderboard)
    {

        new GameSparks.Api.Requests.LeaderboardDataRequest()
            .SetLeaderboardShortCode(fLeaderboard)
            .SetEntryCount(50)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {

                    //Debug.Log(response.JSONString);
                    ScoreLeaderboard.Clear();
                    GSEnumerable<LeaderboardDataResponse._LeaderboardData> leaderboardData = response.Data;
                    foreach (var record in leaderboardData)
                    {

                        PlayerRecord tempRecord = new PlayerRecord();
                        tempRecord.UserName = record.UserName;
                        tempRecord.DisplayName = record.UserName;
                        tempRecord.Rank = (int)record.Rank;
                        tempRecord.Points = (int)record.GetNumberValue("score");
                        ScoreLeaderboard.Add(tempRecord);

                        foreach (var player in Players)
                        {
                            if (record.UserName == player.DisplayName)
                            {
                                player.Points = (int)record.GetNumberValue("score");
                                player.Rank = (int)record.Rank;
                            }
                        }
                        //TakedaPlayer _player = new TakedaPlayer(record.UserName, "", "", (int)record.Rank, (int)record.GetNumberValue("score"));
                        //Debug.Log("Rank# " + record.Rank + " Name: " + record.UserName + " Points: " + tempRecord.Points);
                    }
                    Players = Players.OrderByDescending(o => o.Points).ToList();
                    PrintPlayersDetails();
                    
                }
                else
                {
                    Debug.Log("error getting leaderboard.");
                    //Debug.Log("Error retreiving question.");
                    //Debug.Log(response.ScriptData.JSON);
                }
            }
            );
    }

    public void GetScore()
    {
        List<string> tempLeaderboard = new List<string>();
        tempLeaderboard.Add("Score_Leaderboard");
        new GameSparks.Api.Requests.GetLeaderboardEntriesRequest()
            .SetLeaderboards(tempLeaderboard)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {
                    //Debug.Log(response.JSONString);
                    if (response.BaseData.GetGSData("Score_Leaderboard").ContainsKey("score"))
                    {
                        int tempValue = (int)response.BaseData.GetGSData("Score_Leaderboard").GetInt("score");
                        Debug.Log("My score: " + tempValue);
                        RGT_PlayerPrefs.SetPoints(tempValue);
                        RG_GarageManager.Instance().UpdatePlayerDetails();
                    }
                    else
                    {
                        Debug.Log("no score found.");
                        UpdatePoints(0);
                    }


                }
                else
                {
                    Debug.Log("error getting score.");
                }
            }
            );
    }

    public void UpdatePoints(int fPoints)
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("UpdateScore")
            .SetEventAttribute("score", fPoints)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("score updated");
                    RGT_PlayerPrefs.SetPoints(fPoints);
                    if (RG_GarageManager.Instance() != null)
                    {
                        RG_GarageManager.Instance().UpdatePlayerDetails();
                    }
                    //get leaderboard again with updated results
                    GetLeaderboard("Score_Leaderboard");
                }
                else
                {
                    Debug.Log("error updating score.");
                }
            }
            );
    }

    public void IsQuestionAnswered(int fQuestionNumber)
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("IsQuestionAnswered")
            .SetEventAttribute("UserName", UserName)
            .SetEventAttribute("QuestionNumber", fQuestionNumber)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {
                    string tempValue = response.ScriptData.GetString("response");
                    //Debug.Log("is question answered:" + tempValue);
                    if (tempValue == "1")
                    {
                        Debug.Log("Question already answered found, will remove from bank.");
                        RemoveQuestionFromBank(fQuestionNumber);
                    }

                    //RGT_PlayerPrefs.SetScore(tempValue);
                }
                else
                {
                    Debug.Log("error getting if question is answered.");
                }
            }
            );
    }

    public void GetQuestionsBank()
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("GetQuestionsBank")
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {
                    List<GSData> tempDataList = response.ScriptData.GetGSDataList("Questions");
                    if (tempDataList.Count < 1)
                    {
                        Debug.Log("No Questions found from bank.");
                    }
                    else
                    {
                        QuestionsBank.Clear();
                        Debug.Log(tempDataList.Count + " Questions retreived from bank.");

                        //List<QuestionType> questionsList = new List<QuestionType>();
                        foreach (var question in tempDataList)
                        {
                            QuestionType _question = new QuestionType();

                            _question.ID = int.Parse(question.GetString("_id"));
                            _question.Question = question.GetString("question");

                            List<string> tempAnswers = question.GetStringList("answers");
                            foreach (var answer in tempAnswers)
                            {
                                _question.Answers.Add(answer);
                            }

                            _question.RightAnswer = (int)question.GetInt("rightanswer");
                            _question.Team = question.GetString("team");
                            _question.MyQuestionType = question.GetString("type");
                            _question.Published = (int)question.GetInt("published");

                            QuestionsBank.Add(_question);
                        }
                        //sorting
                        QuestionsBank = QuestionsBank.OrderBy(o => o.ID).ToList();


                        if (TeamName == "AdminAdcetris")
                        {
                            FilterQuestionsBankByTeam("Adcetris");
                        }
                        else if (TeamName == "AdminEntyvio")
                        {
                            FilterQuestionsBankByTeam("Entyvio");
                        }
                        else
                        {
                            FilterQuestionsBankByTeam(TeamName);
                        }



                    }
                }
                else
                {
                    Debug.Log("Error retreiving question from bank.");
                }
            }
            );
    }

    private void FilterQuestionsBankByAnswered()
    {
        List<int> tempQuestionNumbers = new List<int>();

        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("GetAnsweredQuestions")
            .SetEventAttribute("UserName", UserName)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {
                    List<GSData> tempDataList = response.ScriptData.GetGSDataList("response");
                    if (tempDataList == null)
                    {
                        Debug.Log("no answered questions.");
                    }
                    else
                    {
                        if (tempDataList.Count < 1)
                        {
                            Debug.Log("No Questions found for that player.");
                        }
                        else
                        {
                            Debug.Log(tempDataList.Count + " Answered Questions found.");
                            foreach (var question in tempDataList)
                            {
                                tempQuestionNumbers.Add((int)question.GetInt("QuestionNumber"));
                            }

                            foreach (var item in tempQuestionNumbers)
                            {
                                RemoveQuestionFromBank(item);
                            }

                        }
                    }
                }
                else
                {
                    Debug.Log("Error retreiving question.");
                    //Debug.Log(response.ScriptData.JSON);
                }


                //PrintQuestionBank();

                //Debug.Log("Requestion Questions:");
                //for (int i = 0; i < 5; i++)
                //{
                //    Debug.Log(RequestQuestion());
                //}
            }
            );
    }

    public void GetQuestionsSheet()
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("GetQuestions")
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (!response.HasErrors)
                {
                    List<GSData> tempDataList = response.ScriptData.GetGSDataList("Questions");
                    if (tempDataList.Count < 1)
                    {
                        Debug.Log("No Questions found from sheet.");
                    }
                    else
                    {
                        Debug.Log(tempDataList.Count + " Questions Sheet retreived.");
                        foreach (var question in tempDataList)
                        {
                            QuestionType _question = new QuestionType();

                            _question.ID = int.Parse(question.GetString("_id"));
                            _question.Question = question.GetString("question");

                            List<string> tempAnswers = question.GetStringList("answers");
                            foreach (var answer in tempAnswers)
                            {
                                _question.Answers.Add(answer);
                            }

                            _question.RightAnswer = (int)question.GetInt("rightanswer");
                            _question.Team = question.GetString("team");
                            _question.MyQuestionType = question.GetString("type");
                            _question.Published = (int)question.GetInt("published");

                            QuestionsSheet.Add(_question);
                        }
                        //sorting
                        QuestionsSheet = QuestionsSheet.OrderBy(o => o.ID).ToList();
                        if (TeamName == "AdminEntyvio")
                        {
                            FilterQuestionsByAdminTeam("Entyvio");
                        }
                        else if (TeamName == "AdminAdcetris")
                        {
                            FilterQuestionsByAdminTeam("Adcetris");
                        }

                        GetPlayersHistory();
                    }
                }
                else
                {
                    Debug.Log("Error retreiving question from sheet.");
                }
            }
            );
    }

    public void PublishQuestion(QuestionType fQuestion)
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("PublishQuestionByID")
            .SetEventAttribute("ID", fQuestion.ID)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (response.HasErrors)
                {
                    Debug.Log("Error publishing question.");
                    Debug.Log(response.JSONString);
                }
                else
                {
                    string responseString = response.ScriptData.GetString("response");

                    if (responseString.Length < 1)
                    {
                        Debug.Log("Response Empty.");
                    }
                    else
                    {
                        Debug.Log(responseString);
                        Debug.Log(response.JSONString);
                        //RG_GarageManager.Instance().FormatQuestionWindow(CurrentQuestion);
                        //RG_GarageManager.Instance().uI.questionsWindow.SetActive(true);
                    }
                    fQuestion.Published = 1;

                    //retreive questions bank again
                    GetQuestionsBank();
                }
                RG_GarageManager.Instance().DisableLoading();
                //RG_GarageManager.Instance().Button_QuestionsSheetToMenu();
            }
            );
    }

    public void DeleteQuestionHistory(int fQuestionNumber)
    {
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("DeleteQuestionHistory")
            .SetEventAttribute("QuestionNumber", fQuestionNumber)
            .SetDurable(true)
            .Send
            ((response) =>
            {
                if (response.HasErrors)
                {
                    Debug.Log("Error republishing question.");
                    Debug.Log(response.JSONString);
                }
                else
                {
                    string responseString = response.ScriptData.GetString("response");

                    if (responseString.Length < 1)
                    {
                        Debug.Log("Response Empty.");
                    }
                    else
                    {
                        Debug.Log(responseString);
                        Debug.Log(response.JSONString);
                    }
                }
                RG_GarageManager.Instance().DisableLoading();
                RG_GarageManager.Instance().uI.genericWindow.SetActive(false);
                RG_GarageManager.Instance().uI.adminWindow.SetActive(true);
            }
            );
    }

    public void GetPlayersHistory()
    {
        new GameSparks.Api.Requests.LogEventRequest()
           .SetEventKey("GetPlayersHistory")
           .SetDurable(true)
           .Send
           ((response) =>
           {
               if (!response.HasErrors)
               {
                   List<GSData> tempDataList = response.ScriptData.GetGSDataList("History");
                   if (tempDataList.Count < 1)
                   {
                       Debug.Log("No Players History found.");
                   }
                   else
                   {
                       PlayersHistory.Clear();
                       Debug.Log(tempDataList.Count + " players history retreived.");


                       foreach (var playerRecord in tempDataList)
                       {
                           PlayerRecord _record = new PlayerRecord();

                           _record.QuestionNumber = (int)playerRecord.GetInt("QuestionNumber");
                           _record.UserName = playerRecord.GetString("UserName");
                           _record.DisplayName = playerRecord.GetString("DisplayName");
                           _record.ChosenAnswer = (int)playerRecord.GetInt("ChosenAnswer");
                           _record.Correct = (int)playerRecord.GetInt("Correct");
                           _record.Time = (int)playerRecord.GetInt("Time");

                           PlayersHistory.Add(_record);
                       }
                       PlayersHistory = PlayersHistory.OrderBy(o => o.QuestionNumber).ToList();
                       if (TeamName == "AdminAdcetris")
                       {
                           FilterPlayerHistoryByAdminTeam("Adcetris");
                       }
                       else if (TeamName == "AdminEntyvio")
                       {
                           FilterPlayerHistoryByAdminTeam("Entyvio");
                       }
                       //PrintPlayersHistory();

                   }
               }
               else
               {
                   Debug.Log("Error retreiving question.");
               }
           }
           );
    }
    #endregion


    public bool isPlayerAuthenticated()
    {
        if (UserName != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    private void FilterQuestionsBankByTeam(string fTeamName)
    {
        //Debug.Log("Filtering bank");
        List<QuestionType> itemsToBeRemoved = new List<QuestionType>();

        foreach (var question in QuestionsBank)
        {
            if (question.Team != fTeamName)
            {
                itemsToBeRemoved.Add(question);
            }
        }

        foreach (var item in itemsToBeRemoved)
        {
            QuestionsBank.Remove(item);
        }

        itemsToBeRemoved.Clear();

        if (fTeamName == "Adcetris" || fTeamName == "Entyvio")
        {
            FilterQuestionsBankByAnswered();
        }

    }

    private void RemoveQuestionFromBank(int fQuestionId)
    {
        QuestionType itemToBeRemoved = null;

        foreach (var question in QuestionsBank)
        {
            if (question.ID == fQuestionId)
            {
                //Debug.Log("Question already answered found");
                itemToBeRemoved = question;
            }
        }

        if (itemToBeRemoved != null)
        {
            QuestionsBank.Remove(itemToBeRemoved);
        }

    }



    private void FilterQuestionsByAdminTeam(string fTeamName)
    {
        List<QuestionType> itemsToBeRemoved = new List<QuestionType>();

        foreach (var question in QuestionsSheet)
        {
            if (question.Team != fTeamName)
            {
                itemsToBeRemoved.Add(question);
            }
        }

        foreach (var item in itemsToBeRemoved)
        {
            QuestionsSheet.Remove(item);
        }
        itemsToBeRemoved.Clear();
    }

    private void FilterPlayerHistoryByAdminTeam(string fTeamName)
    {
        List<PlayerRecord> itemsToBeRemoved = new List<PlayerRecord>();

        foreach (var question in PlayersHistory)
        {
            if (!IsQuestionAvailable(question.QuestionNumber))
            {
                itemsToBeRemoved.Add(question);
            }
        }

        foreach (var item in itemsToBeRemoved)
        {
            PlayersHistory.Remove(item);
        }
        itemsToBeRemoved.Clear();
        PrintPlayersHistory();
    }

    public bool IsQBankAvailable()
    {
        if (QuestionsBank.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PrintQuestionBank()
    {
        //printing question for debugging
        foreach (var question in QuestionsBank)
        {
            Debug.Log(question);
        }
    }

    public void PrintQuestionSheet()
    {
        //printing question for debugging
        foreach (var question in QuestionsSheet)
        {
            Debug.Log(question);
        }
    }

    public void PrintPlayersHistory()
    {
        foreach (var record in PlayersHistory)
        {
            Debug.Log(record);
        }
    }

    public void PrintPlayersDetails()
    {
        foreach (var player in Players)
        {
            Debug.Log(player);
        }
    }

    public QuestionType RequestQuestion()
    {
        QuestionType tempQuestion = null;
        if (QuestionsBank.Count < 1)
        {
            Debug.Log("No questions in bank.");
        }
        else
        {
            tempQuestion = QuestionsBank[0];
            tempQuestion.ChosenAnswer = 0;
            CurrentQuestion = tempQuestion;
            QuestionsBank.RemoveAt(0);
            Debug.Log("question requested.");
        }
        return tempQuestion;
    }

    public void EnableLoading(string fLoadingText)
    {
        connectingWindowText.text = fLoadingText;
        connectingWindow.SetActive(true);
    }

    public void InitiateQuestionWindow()
    {
        QuestionType currentQuestion = RequestQuestion();

        InitializeQuestion(currentQuestion.ID);
    }

    public void FormatQuestionWindow(QuestionType fCurrentQuestion)
    {
        questionText.text = fCurrentQuestion.Question;
        questionText.transform.parent.gameObject.SetActive(true);



        int numberOfAnswers = fCurrentQuestion.Answers.Count;

        for (int i = 0; i < numberOfAnswers; i++)
        {
            answersTextList[i].text = fCurrentQuestion.Answers[i];
        }

        //adding on click events for each answer button
        answer1Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        answer1Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { connectingWindowText.text = "Submitting answer..."; questionsWindow.SetActive(false); connectingWindow.SetActive(true); });
        answer1Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => InitiateSubmitAnswerWindow(1));
        answer1Text.transform.parent.gameObject.SetActive(true);

        answer2Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        answer2Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { connectingWindowText.text = "Submitting answer..."; questionsWindow.SetActive(false); connectingWindow.SetActive(true); });
        answer2Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => InitiateSubmitAnswerWindow(2));
        answer2Text.transform.parent.gameObject.SetActive(true);

        answer3Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        answer3Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { connectingWindowText.text = "Submitting answer..."; questionsWindow.SetActive(false); connectingWindow.SetActive(true); });
        answer3Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => InitiateSubmitAnswerWindow(3));
        answer3Text.transform.parent.gameObject.SetActive(true);

        answer4Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        answer4Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { connectingWindowText.text = "Submitting answer..."; questionsWindow.SetActive(false); connectingWindow.SetActive(true); });
        answer4Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => InitiateSubmitAnswerWindow(4));
        answer4Text.transform.parent.gameObject.SetActive(true);


        answer5Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        answer5Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { connectingWindowText.text = "Submitting answer..."; questionsWindow.SetActive(false); connectingWindow.SetActive(true); });
        answer5Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => InitiateSubmitAnswerWindow(5));
        answer5Text.transform.parent.gameObject.SetActive(false);

        answer6Text.transform.parent.GetComponent<Button>().onClick.RemoveAllListeners();
        answer6Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => { connectingWindowText.text = "Submitting answer..."; questionsWindow.SetActive(false); connectingWindow.SetActive(true); });
        answer6Text.transform.parent.GetComponent<Button>().onClick.AddListener(() => InitiateSubmitAnswerWindow(6));
        answer6Text.transform.parent.gameObject.SetActive(false);

        //activating answer button according to how many answers the current question has
        switch (numberOfAnswers)
        {
            case 2:
                answer1Text.transform.parent.gameObject.SetActive(true);
                answer2Text.transform.parent.gameObject.SetActive(true);
                answer3Text.transform.parent.gameObject.SetActive(false);
                answer4Text.transform.parent.gameObject.SetActive(false);
                answer5Text.transform.parent.gameObject.SetActive(false);
                answer6Text.transform.parent.gameObject.SetActive(false);
                break;
            case 3:
                answer1Text.transform.parent.gameObject.SetActive(true);
                answer2Text.transform.parent.gameObject.SetActive(true);
                answer3Text.transform.parent.gameObject.SetActive(true);
                answer4Text.transform.parent.gameObject.SetActive(false);
                answer5Text.transform.parent.gameObject.SetActive(false);
                answer6Text.transform.parent.gameObject.SetActive(false);
                break;
            case 4:
                answer1Text.transform.parent.gameObject.SetActive(true);
                answer2Text.transform.parent.gameObject.SetActive(true);
                answer3Text.transform.parent.gameObject.SetActive(true);
                answer4Text.transform.parent.gameObject.SetActive(true);
                answer5Text.transform.parent.gameObject.SetActive(false);
                answer6Text.transform.parent.gameObject.SetActive(false);
                break;
            case 5:
                answer1Text.transform.parent.gameObject.SetActive(true);
                answer2Text.transform.parent.gameObject.SetActive(true);
                answer3Text.transform.parent.gameObject.SetActive(true);
                answer4Text.transform.parent.gameObject.SetActive(true);
                answer5Text.transform.parent.gameObject.SetActive(true);
                answer6Text.transform.parent.gameObject.SetActive(false);
                break;
            case 6:
                answer1Text.transform.parent.gameObject.SetActive(true);
                answer2Text.transform.parent.gameObject.SetActive(true);
                answer3Text.transform.parent.gameObject.SetActive(true);
                answer4Text.transform.parent.gameObject.SetActive(true);
                answer5Text.transform.parent.gameObject.SetActive(true);
                answer6Text.transform.parent.gameObject.SetActive(true);
                break;

            default:
                break;
        }
    }

    public void InitiateSubmitAnswerWindow(int fMyAnswer)
    {
        EnableLoading("Submitting answer...");
        SubmitAnswer(fMyAnswer);
        questionsWindow.SetActive(false);
    }

    public void DisableLoading()
    {
        connectingWindow.SetActive(false);
    }

    public void PauseGameForQuestion()
    {
        confirmationWindow.SetActive(false);
        RG_PauseGame.Instance().PauseForQuestions();
    }

    public void ConfirmationWindow(string fMessage)
    {
        confirmationText.text = fMessage;
        confirmationWindow.SetActive(true);
    }

    public void EnableTimer()
    {
        _timer = 0;
        timerText.text = _timer.ToString();
        isCounting = true;
    }

    public void DisableTimer()
    {
        isCounting = false;
    }

    public int CalculateScore(float fTimer)
    {
        if (fTimer <= 10f)
        {
            return 5;
        }
        else if (fTimer <= 20f)
        {
            return 3;
        }
        else if (fTimer <= 30)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public Color CalculateColor(float fTimer)
    {
        if (fTimer <= 10f)
        {
            return Color.green;
        }
        else if (fTimer <= 20f)
        {
            return Color.yellow;
        }
        else if (fTimer <= 30)
        {
            return Color.red;
        }
        else
        {
            return Color.red;
        }
    }

    float CalculateTime(float fCurrentTimer)
    {
        float result = fCurrentTimer / 30.0f;
        if (result > 1)
        {
            return 1;
        }
        else
        {
            return result;
        }
    }

    void ProcessPlayerHistory()
    {
        foreach (var record in PlayersHistory)
        {
            /*
             if player name in record !exists in players then add a new takeda player
             add this record to the player
             */
            if (!DoesPlayerExist(record.UserName))
            {
                //TakedaPlayer tempPlayer = new TakedaPlayer(record.UserName,record.DisplayName);
            }
            /*
            else if player exists
            if this record !exists in this player's history then add it to player's history
            */
        }
    }

    bool DoesPlayerExist(string fUserName)
    {
        bool result = false;
        foreach (var player in Players)
        {
            if (player.UserName == fUserName)
            {
                result = true;
            }
        }
        return result;
    }


    bool IsQuestionAvailable(int fQuestionNumber)
    {
        foreach (var question in QuestionsBank)
        {
            if (question.ID == fQuestionNumber)
            {
                return true;
            }
        }
        return false;
    }
}
