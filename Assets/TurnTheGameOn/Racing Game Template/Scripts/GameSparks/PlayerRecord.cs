﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRecord
{

    private string userName;
    private string displayName;
    private int questionNumber;
    private int chosenAnswer;
    private int correct;
    private int rank;
    private int time;
    private int points;

    public string UserName
    {
        get
        {
            return userName;
        }

        set
        {
            userName = value;
        }
    }

    public string DisplayName
    {
        get
        {
            return displayName;
        }

        set
        {
            displayName = value;
        }
    }

    public int QuestionNumber
    {
        get
        {
            return questionNumber;
        }

        set
        {
            questionNumber = value;
        }
    }

    public int ChosenAnswer
    {
        get
        {
            return chosenAnswer;
        }

        set
        {
            chosenAnswer = value;
        }
    }

    public int Correct
    {
        get
        {
            return correct;
        }

        set
        {
            correct = value;
        }
    }

    public int Rank
    {
        get
        {
            return rank;
        }

        set
        {
            rank = value;
        }
    }

    public int Time
    {
        get
        {
            return time;
        }

        set
        {
            time = value;
        }
    }

    public int Points
    {
        get
        {
            return points;
        }

        set
        {
            points = value;
        }
    }

    public override string ToString()
    {
        string result = "";

        result += "PlayerName: " + DisplayName + " QuestionNumber: " + QuestionNumber;

        result += " ChosenAnswer: " + ChosenAnswer;
        result += " Correct: " + Correct;
        result += " Time: " + Time;

        return result;
    }
}
