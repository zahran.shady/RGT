﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[System.Serializable]
public class RGT_EVPAIHelper : MonoBehaviour {

	[Header("References")]
	public Rigidbody rbody;
	public RG_AIDetection detection;// AI sensor system used for obstacle detection and avoidance
	public EVP.VehicleController carController;
	[HideInInspector()] public RG_WaypointCircuit circuit;	// A route of waypoints used for pathfinding
	[HideInInspector()] public Transform target;// Current waypoint to drive toward
	private RG_SceneManager sceneManager;

	[Space()]
	[Tooltip("Enable or disable AI driving")] public bool driving;
	[Tooltip("Steer speed multiplier, increase to apply steering input faster")][Range(0.0f,1.0f)] public float steerSensitivity = 1;

	[Header("Caution Settings")]
	[Tooltip("Cautious speed multiplier, increase to drive faster when turning")][Range(0.0f,1.0f)] public float cautiousSpeedFactor = 1f;
	[Tooltip("Turn angle to use maximum caution")][Range(0, 180)] public float cautiousMaxAngle = 70;
	[Tooltip("Cautious turning/spin multiplier, increase to be more cautious when turning")][Range(0, 100)] public float cautiousAngularVelocityFactor = 30f;
	[Tooltip("Target offset along route")][Range(0, 200)] public float lookAheadForTargetOffset = 25;
	[Tooltip("Target offset speed multiplier, increase to look farther when driving faster")][Range(0,20)] public float lookAheadForTargetFactor = 0.1f;
	[Tooltip("Offset along route to look for turns and apply caution")][Range(0, 200)] public float lookAheadForSpeedOffset = 50;
	[Tooltip("Offset along route speed multiplier, increase to look farther when driving faster")][Range(0,20)] public float lookAheadForSpeedFactor = 0.5f;

	[Header("Stuck Vehicle Settings")]
	[Tooltip("If the vehicle is stuck for this amount of time, it will be moved to its current target")][Range(0, 30)] public float stuckResetTimer = 7;
	public float respawnYOffset = 5;

	[Space]
	public float avoidForce;
	public float rotationSpeed = 0.8f;

	[HideInInspector] public int opponentNumber;
	[HideInInspector] public float timer;
	float stuckTimer;
	public RG_WaypointCircuit.RoutePoint targetPoint { get; private set; }
	public RG_WaypointCircuit.RoutePoint speedPoint { get; private set; }
	public RG_WaypointCircuit.RoutePoint progressPoint { get; private set; }
	float cautiousnessRequired;
	private float progressDistance;
	float approachingCornerAngle;
	float spinningAngle;
	private float desiredSpeed = 1;
	private float spOff;
	private float tarOff;
	private Vector3 tempPosition;
	float speedFactor;

	void Start(){
		if (rbody == null)	rbody = GetComponent<Rigidbody> ();
		if (carController == null)	carController = GetComponent<EVP.VehicleController> ();
		sceneManager = GameObject.Find ("Scene Manager").GetComponent<RG_SceneManager>();
		circuit = GameObject.Find("Scene Manager").GetComponent<RG_WaypointCircuit>();
		target = new GameObject(name + " Waypoint Target").transform;
		spOff = lookAheadForSpeedOffset;
		tarOff = lookAheadForTargetOffset;
		progressDistance = 0;
		driving = true;
	}

	void Update () {
		target.position = circuit.GetRoutePoint(progressDistance + lookAheadForTargetOffset + lookAheadForTargetFactor*carController.speed).position;
		target.rotation = Quaternion.LookRotation(circuit.GetRoutePoint(progressDistance + lookAheadForSpeedOffset + lookAheadForSpeedFactor*carController.speed).direction);
		progressPoint = circuit.GetRoutePoint(progressDistance);
		Vector3 progressDelta = progressPoint.position - transform.position;
		if (Vector3.Dot(progressDelta, progressPoint.direction) < 0){
			progressDistance += progressDelta.magnitude*0.5f;
		}
		if (detection.sensorHits.forward1Hit || detection.sensorHits.forward2Hit || detection.sensorHits.forward3Hit) {
			if (detection.sensorHitLayer.forward1HitLayer == "Car Collider" || detection.sensorHitLayer.forward2HitLayer == "Car Collider" || detection.sensorHitLayer.forward3HitLayer == "Car Collider" ) {
				if (!detection.sensorHits.forwardLeft1Hit || !detection.sensorHits.forwardLeft2Hit || !detection.sensorHits.forwardLeft3Hit) {
					transform.Rotate (0, Time.deltaTime * (-rotationSpeed * 20), 0, Space.World);
					rbody.AddForce (-Vector3.right * avoidForce);
					speedFactor = 0.75f;
				} else {
					transform.Rotate (0, Time.deltaTime * (rotationSpeed * 20), 0, Space.World);
					rbody.AddForce (Vector3.right * avoidForce);
					speedFactor = 1.2f;
				}
			}
		}else if(cautiousnessRequired < 0.35f){
			speedFactor = 1.5f;
		} else {
			speedFactor = 1;
		}
		CheckIfStuck ();
	}

	void CheckIfStuck (){
		if (carController.speed < 1.0f && sceneManager.countUp) {
			stuckTimer += Time.deltaTime * 1;
		} else {
			stuckTimer = 0;
		}
		if (stuckTimer >= stuckResetTimer) {
			stuckTimer = 0;
			transform.position = target.position;
			transform.rotation = target.rotation;
		}
	}

	private void FixedUpdate() {
		if (target == null || !driving) {
			carController.throttleInput = 0;
			carController.brakeInput = 1;
			carController.handbrakeInput = 1;
		}
		else {			
			Vector3 fwd = transform.forward;
			if (rbody.velocity.magnitude > carController.maxSpeedForward * 0.1f) {
				fwd = rbody.velocity;
			}
			desiredSpeed = 1;
			approachingCornerAngle = Vector3.Angle(target.forward, fwd);// the car will brake according to the upcoming change in direction of the target, check out the angle of our target compared to the current direction of the car
			spinningAngle = rbody.angularVelocity.magnitude*cautiousAngularVelocityFactor;// also consider the current amount we're turning, multiplied up and then compared in the same way as an upcoming corner angle
			cautiousnessRequired = Mathf.InverseLerp(0, cautiousMaxAngle, Mathf.Max(spinningAngle, approachingCornerAngle));// if it's different to our current angle, we need to be cautious (i.e. slow down) a certain amount
			desiredSpeed = cautiousSpeedFactor * (1 - cautiousnessRequired);
			if (desiredSpeed < 0.05f) {
				if (carController.speed < 10) {
					desiredSpeed = 0.5f;
				}
			}
			if (carController.speed < 10) {
				desiredSpeed = 0.5f;
				lookAheadForTargetOffset = 0;
				lookAheadForSpeedOffset = 0;
			} else if (carController.speed < 20){
				lookAheadForTargetOffset = 5;
				lookAheadForSpeedOffset = 15;
			}else {
				lookAheadForTargetOffset = tarOff;
				lookAheadForSpeedOffset = spOff;
			}
			carController.throttleInput = desiredSpeed * speedFactor;
			Vector3 localTarget = transform.InverseTransformPoint(target.position);													// work out the local angle towards the target
			float targetAngle = Mathf.Atan2(localTarget.x, localTarget.z)*Mathf.Rad2Deg;
			float steer = Mathf.Clamp(targetAngle*steerSensitivity, -1, 1)*Mathf.Sign(carController.speed);							// get the amount of steering needed to aim the car towards the target
			carController.steerInput = steer;																							// steer
		}
	}

	private void OnDrawGizmos()	{
		if (Application.isPlaying && circuit != null){
			Gizmos.color = Color.green;
			Gizmos.DrawLine(transform.position, target.position);
			Gizmos.DrawWireSphere(circuit.GetRoutePosition(progressDistance), 1);
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(target.position, target.position + target.forward);
		}
	}

}