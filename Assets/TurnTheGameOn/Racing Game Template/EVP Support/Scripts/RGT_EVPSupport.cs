﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TurnTheGameOn.RacingGameTemplate;

public class RGT_EVPSupport : MonoBehaviour {

	public GameObject cameraController;
	public EVP.VehicleController vehicleController;
	public EVP.VehicleAudio vehicleAudio;
	public GameObject waypointArrow;
	public GameObject waypointArrowCamera;
	public bool autoFindSpeedText = true;
	public bool autoFindSpeedometerText = true;
	public bool autoFindGearText = true;
	public bool autoFindRPMSlider = true;
	Text speedText;
	Text spedometerTypeText;
	Text gearText;
	Slider RPMSlider;								//Edys		RPMs are not supported, Throttle Input is used as a substitute
	[HideInInspector()] public int vehicleNumber;
	[HideInInspector()] public int levelTopSpeed;						//Edys		Max Speed forward
	[HideInInspector()] public int levelAcceleration;					//Edys		Tire Friction
	[HideInInspector()] public int levelBrakePower;						//Edys		Max Brake Force
	[HideInInspector()] public int levelTireTraction;					//Edys		Max Drive Slip
	[HideInInspector()] public int levelSteerSensitivity;				//Edys		Steering Assist Ratio
	public float levelBonusTopSpeed = 5;
	public float levelBonusAcceleration = 0.2f;
	public float levelBonusBrakePower = 100;
	public float levelBonusTireTraction = -1;
	public float levelBonusSteerSensitivity = 0.02f;

	float currentSpeed;

	void Start () {
		if (RGT_PlayerPrefs.GetGameMode() == "SINGLE PLAYER") {
			waypointArrow.SetActive (true);
			waypointArrowCamera.SetActive (true);
		}
		if (autoFindSpeedText) {
			speedText = GameObject.Find("Speed Text").GetComponent<Text>();
		}
		if (autoFindSpeedometerText) {
			spedometerTypeText = GameObject.Find("MPH Text").GetComponent<Text>();
		}
		if (autoFindGearText) {
			gearText = GameObject.Find("Gear Text").GetComponent<Text>();
		}
		if(autoFindRPMSlider){
			RPMSlider = GameObject.Find("RPM Slider").GetComponent<Slider>();
		}
		cameraController.transform.parent = null;
		vehicleNumber = RGT_PlayerPrefs.GetVehicleNumber ();
		levelTopSpeed = RGT_PlayerPrefs.GetVehicleTopSpeedLevel (vehicleNumber);
		levelAcceleration = RGT_PlayerPrefs.GetVehicleAccelerationLevel (vehicleNumber);
		levelBrakePower = RGT_PlayerPrefs.GetVehicleBrakePowerLevel (vehicleNumber);
		levelTireTraction = RGT_PlayerPrefs.GetVehicleTireTractionLevel (vehicleNumber);
		levelSteerSensitivity = RGT_PlayerPrefs.GetVehicleSteerSensitivityLevel (vehicleNumber);
		vehicleController.maxSpeedForward += levelTopSpeed * levelBonusTopSpeed;
		vehicleController.tireFriction += levelAcceleration * levelBonusAcceleration;
		vehicleController.maxBrakeForce += levelBrakePower * levelBonusBrakePower;
		vehicleController.maxDriveSlip += levelTireTraction * levelBonusTireTraction;
		vehicleController.steeringAssistRatio += levelSteerSensitivity * levelBonusSteerSensitivity;
	}
		
	void Update () {
		switch(RGT_PlayerPrefs.playableVehicles.speedometerType[vehicleNumber])
		{
		case RG_DistanceMetrics.SpeedType.KilometerPerHour:
			currentSpeed = 3.6f * vehicleController.speed;
			spedometerTypeText.text = "KPH";
			break;
		case RG_DistanceMetrics.SpeedType.MilesPerHour:
			currentSpeed = 2.23694f * vehicleController.speed;
			spedometerTypeText.text = "MPH";
			break;
		case RG_DistanceMetrics.SpeedType.MeterPerSecond:
			currentSpeed = vehicleController.speed;
			spedometerTypeText.text = "MPS";
			break;
		}

		switch(vehicleAudio.m_gear)
		{
		case -1:
			gearText.text = "R";
			break;
		case 0:
			gearText.text = "N";
			break;
		case 1:
			gearText.text = "1";
			break;
		case 2:
			gearText.text = "2";
			break;
		case 3:
			gearText.text = "3";
			break;
		case 4:
			gearText.text = "4";
			break;
		case 5:
			gearText.text = "5";
			break;
		case 6:
			gearText.text = "6";
			break;
		case 7:
			gearText.text = "7";
			break;
		}

		speedText.text = Mathf.Abs(currentSpeed).ToString("F0");
		if (RPMSlider) {
			RPMSlider.value = vehicleAudio.m_engineRpm;
		}
	}

}