﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerControl : MonoBehaviour {
	private Animator _animator;
	private AnimatorStateInfo currentBaseState;
	private CharacterController _collMain;
	private LayerMask characterMask = 5;
	private Camera MainCam;
	private AudioSource m_AudioSource;
	public AudioClip[] m_FootstepSounds;
	public AudioClip[] m_MetalFootstepSounds;
	public AudioClip m_jumpSound;

	public float lookWeight;
	public float jumpSpeed;
	public float RHandWeight;
	public Transform hintRight;
	public Transform hintLeft;
	public Transform rifleLeftIkPos;
	public Transform pistolIkPos;
	public Transform  myHand;
	public Transform  pistolHandHolder;
	public Transform rifleHolster;
	public Transform pistolHolster;

	public float lookIKweight;
	public float bodyWeight;
	public float headWeight;
	public float eyesWeight;
	public float clampWeight;
	public float health;

	public Transform lookAtPos;
	private Vector3 moveDirection = Vector3.zero;

	private float _mouseY;
	private float _mouseX;
	private float _speed;
	private float _strafe;
	private float _distToGround;
	private float shootTime = 0;
	private float chairHandWeight;
	public float gravity=9.8f;

	private bool _jumpButton;
	private bool _run;
	private bool _canLook;
	private bool _inChair;
	private bool _canPilot;
	private bool _canShoot;

	public bool driving;
	public bool usingPistol;
	public bool usingRifle;
	public bool atWall;
	public bool canRotate;
	public bool canUseIK;
	public bool alive;
	public bool inSpace;

	public GameObject curHover;
	public GameObject curChair;
	public GameObject curShip;
	public GameObject curCar;

	public GameObject _carRightDoor;
	public GameObject _carLeftDoor;

	public GameObject cursorUnarmed;
	public GameObject cursorPistol;
	public GameObject cursorRiffle;

	//ANIMATION STATES
	static int jumpState = Animator.StringToHash("Base Layer.JumpUpHigh");
	static int jumpShortState = Animator.StringToHash("Base Layer.jumpShort");

	//WEAPON SECTION
	
	/// assign all the values in the inspector
	
	public WeaponInfo currentWeapon;
	public List<WeaponInfo> WeaponList = new List<WeaponInfo>();
	[System.Serializable]
	public class WeaponInfo{
		public string weaponName = "veapon name";
		public float fireRate = 0.1f;
		public Transform weaponTransform;
		public Transform weaponMuzzlePoint;
		public float weaponKickBack;
		public GameObject bulletPrefab;
		public int totalAmmo;
		public int magazine;
	}
	// Use this for initialization
	void Start () {
		m_AudioSource = GetComponent<AudioSource>();
		MainCam = Camera.main;
		_animator = gameObject.GetComponent<Animator> ();
		_collMain = gameObject.GetComponent<CharacterController>();
		_canShoot = false;
		currentWeapon = WeaponList[0];	
		if(inSpace){
		MainCam.clearFlags= CameraClearFlags.SolidColor;
		}
		else{
			MainCam.clearFlags= CameraClearFlags.Skybox;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		_mouseX = Input.GetAxis ("Mouse X");
		_speed = Input.GetAxis("Vertical");//reading vertical axis input
		_strafe = Input.GetAxis("Horizontal");//reading horizontal axis input
		_jumpButton = Input.GetButton("Jump") ? true : false;//check if jump button was pressed
		_run = Input.GetKey(KeyCode.LeftShift) ? true : false;//check if run button was pressed
		RHandWeight = _animator.GetFloat ("rifleLeftHandWeight");

		Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
		lookAtPos.position = ray.GetPoint (15);
//PROCESS SHOOTING
		if (Input.GetMouseButton (0) && _canShoot) {
			if (shootTime <= Time.time) {
				shootTime = Time.time + currentWeapon.fireRate;
				ShootRay();
				currentWeapon.magazine -= 1;
				if (currentWeapon.magazine < 0) {
					currentWeapon.magazine = 0;
					}
				if (currentWeapon.magazine == 0 && currentWeapon.totalAmmo>0) {
						
						_animator.SetTrigger ("ReloadWeapon");	
				}
			}
		}
//PROCESSING ROTATION
		Vector3 aimPoint =  Camera.main.transform.forward*10f;
		if(!atWall && canRotate){
			Quaternion targetRotation = Quaternion.LookRotation(aimPoint);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 250 * Time.deltaTime);
			//this.transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5* Time.deltaTime);
			this.transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
		}
//Applying Gravity

		if(isGrounded()){
			if (Input.GetButton ("Jump")) {				
				_animator.SetTrigger ("jump");
				if(_animator.GetBool("jump") == true){
					moveDirection.y = jumpSpeed;
				}
			}
		}
		moveDirection.y -= gravity * Time.deltaTime;
		if(_collMain.enabled){
		_collMain.Move (moveDirection * Time.deltaTime);
		if(moveDirection.y < -15f){
			moveDirection.y = -15f;
		}
		}

		if((currentBaseState.fullPathHash == jumpState)||(currentBaseState.fullPathHash == jumpShortState)){
			_animator.ResetTrigger("jump");
		}
		//WEAPON PROCESSING
		if(Input.GetKeyDown(KeyCode.Alpha2)){
			if(!usingRifle){
				StartCoroutine(SwapRifle());
			}
			if(usingRifle){
				StartCoroutine(HideRifle());
			}
		}
		if(Input.GetKeyDown(KeyCode.Alpha1)){
			if(!usingPistol){
				StartCoroutine(SwapPistol());
			}
			if(usingPistol){
				StartCoroutine(HidePistol());
			}
		}
//SHOOTING RAY FORWARD
		Vector3 ahead = transform.forward;
		Vector3 rayStart = new Vector3(this.transform.position.x, this.transform.position.y+1f, this.transform.position.z);
		Ray	rayFrwrd = new Ray(rayStart, ahead);
		RaycastHit hit;
		Debug.DrawRay(rayStart, transform.forward, Color.green);
		if(Physics.Raycast(rayFrwrd, out hit, 2f)){
			if(hit.transform.gameObject.name == ("chair")){
				curChair = hit.transform.gameObject;
			}
		}
		if (curChair != null) {
			float dist = Vector3.Distance(curChair.transform.position, this.transform.position);
			if(dist>2){
				curChair = null;
				curShip = null;
			}
		}
		if (Physics.Raycast (rayFrwrd, out hit, 1f)) {
			if (hit.transform.gameObject.name == ("spaceShipRight")) {
				curShip = hit.transform.parent.gameObject;
				_canPilot = true;
			}
		}
		if (curShip != null && curChair == null) {
			float distToSship = Vector3.Distance (curShip.transform.position, this.transform.position);
			if (distToSship > 4) {
				curShip = null;
				_canPilot = false;
			}
		}
		if(Physics.Raycast(rayFrwrd, out hit, 1f)){
			if(hit.transform.gameObject.name == ("doorR")){
				curCar = hit.transform.parent.gameObject;
				_carRightDoor = hit.transform.gameObject;
			}
			if(hit.transform.gameObject.name == ("doorL")){
				curCar = hit.transform.parent.gameObject;
				_carLeftDoor = hit.transform.gameObject;
			}
			if(hit.transform.gameObject.name == ("hover")){
				curHover = hit.transform.gameObject;
			}
		}
		if (curHover != null) {
			float distToHover = Vector3.Distance(curHover.transform.position, this.transform.position);
			if(distToHover>3){
				curHover = null;
			}
		}

		if (curCar != null) {
			if (_carLeftDoor != null) {
				float distToLDoor = Vector3.Distance (_carLeftDoor.transform.position, this.transform.position);
				if (distToLDoor > 4) {
					curCar = null;
					_carLeftDoor = null;
				}
			}
			if (_carRightDoor != null) {
				float distToRDoor = Vector3.Distance (_carRightDoor.transform.position, this.transform.position);
				if (distToRDoor > 4) {
					curCar = null;
					_carRightDoor = null;
				}
			}
		}
		if (curCar &&!driving && Input.GetKeyDown(KeyCode.E) && _collMain.enabled == true) {
			StartCoroutine(GetInCar ());
		}
		if (driving && Input.GetKeyDown (KeyCode.H)) {
			StartCoroutine (GetOutCar ());
		}
		_animator.SetFloat("mouseX",_mouseX, 0.3f, Time.deltaTime);
		_animator.SetFloat("Speed", _speed);
		_animator.SetFloat("Strafe", _strafe);
		_animator.SetBool("grounded", isGrounded());
		_animator.SetBool("Jump", _jumpButton);
		_animator.SetBool ("Run", _run);
		_animator.SetFloat ("VertVelocity", _collMain.velocity.y);
		currentBaseState = _animator.GetCurrentAnimatorStateInfo(0);

		if(Input.GetKeyDown(KeyCode.E)){
			if((curChair != null)&&(_collMain.enabled)&&(canUseIK)){								
				StartCoroutine(SitInChair());
			}
			if (_canPilot && canRotate) {
				StartCoroutine (GetInShip ());
				}
			if(curHover && _collMain.enabled){
				StartCoroutine(GetOnHover());
			}
		}
		if(Input.GetKeyDown(KeyCode.H)){
			if(_inChair && !_collMain.enabled){
				StartCoroutine(GetUpChair());
			}
			if(_canPilot && curShip.GetComponent<SpaceShipControl>().landed){
				StartCoroutine (GetOutShip());
			}
			if (curHover != null) {
				if (curHover.GetComponent<Hover> ().canControl) {
					StartCoroutine (GetOffHover ());
				}
			}
		}
	}
	void OnAnimatorIK(int layerIndex)
	{
		if(canUseIK){
		_animator.SetLookAtWeight (lookIKweight, bodyWeight, headWeight, eyesWeight, clampWeight);
		_animator.SetLookAtPosition (lookAtPos.position);
		}
		if (usingRifle) {
			_animator.SetIKPositionWeight (AvatarIKGoal.LeftHand, RHandWeight);
			_animator.SetIKPosition (AvatarIKGoal.LeftHand, rifleLeftIkPos.position);
			_animator.SetIKRotation (AvatarIKGoal.LeftHand, rifleLeftIkPos.rotation);
			_animator.SetIKHintPositionWeight (AvatarIKHint.LeftElbow, 1f);
			_animator.SetIKHintPosition (AvatarIKHint.LeftElbow, hintRight.position);
		}
		if (usingPistol) {			
			_animator.SetIKPositionWeight (AvatarIKGoal.RightHand, RHandWeight);
			_animator.SetIKPosition (AvatarIKGoal.RightHand, pistolIkPos.position);
			_animator.SetIKRotation (AvatarIKGoal.RightHand, pistolIkPos.rotation);
			_animator.SetIKHintPositionWeight (AvatarIKHint.RightElbow, 1f);
			_animator.SetIKHintPosition (AvatarIKHint.RightElbow, hintLeft.position);
		}
		//placing a hand on a joystic
		if(curChair!=null){
			GameObject handPos = curChair.transform.Find("handHold").gameObject;
			_animator.SetIKPositionWeight (AvatarIKGoal.RightHand, chairHandWeight);
			_animator.SetIKPosition (AvatarIKGoal.RightHand, handPos.transform.position);
			_animator.SetIKRotation (AvatarIKGoal.RightHand, handPos.transform.rotation);
			_animator.SetIKHintPositionWeight (AvatarIKHint.RightElbow, chairHandWeight);
			_animator.SetIKHintPosition (AvatarIKHint.RightElbow, hintLeft.position);
		}

	}
	bool isGrounded()	{
		return Physics.CheckSphere(transform.position, 1f, characterMask | 1<<9);
	}
	public void jump(){
		
	}

//SHOOTING LOGIC
	void ShootRay(){
		float x = Screen.width / 2;
		float y = Screen.height / 2;
		Ray ray = Camera.main.ScreenPointToRay (new Vector3 (x, y, 0));
		RaycastHit hit;
		GameObject bullet = Instantiate(currentWeapon.bulletPrefab, currentWeapon.weaponMuzzlePoint.position, currentWeapon.weaponMuzzlePoint.rotation) as GameObject;
		LineRenderer bulletTrail = bullet.GetComponent<LineRenderer> ();
		Vector3 startPos = currentWeapon.weaponMuzzlePoint.TransformPoint (Vector3.zero);
		Vector3 endPos = Vector3.zero;
			if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
				float distance = Vector3.Distance(currentWeapon.weaponMuzzlePoint.transform.position, hit.point);
				RaycastHit[] hits = Physics.RaycastAll(startPos, hit.point - startPos, distance);
				currentWeapon.weaponMuzzlePoint.transform.LookAt(hit.point);
			foreach(RaycastHit hit2 in hits){
				if(hit2.transform.GetComponent<Rigidbody>()){
					Vector3 direction = hit2.transform.position - transform.position;
					direction = direction.normalized;
					hit2.transform.GetComponent<Rigidbody>().AddForce(direction * 200);
					}
				}
			endPos = hit.point;
			}
			else{//ray, out hit, Mathf.Infinity				
				currentWeapon.weaponMuzzlePoint.transform.LookAt(ray.GetPoint(100));
				endPos = ray.GetPoint(100);
			}
		bulletTrail.SetPosition (0, startPos);
		bulletTrail.SetPosition (1, endPos);
	}
//SWITCHING TO RIFFLE
	IEnumerator SwapRifle(){
		if (usingPistol) {
			_canShoot = false;
			_animator.SetBool ("pistolActive", false);
			yield return new WaitForSeconds (0.7f);
			usingPistol = false;
			WeaponList [1].weaponTransform.position = pistolHolster.transform.position;
			WeaponList [1].weaponTransform.rotation = pistolHolster.transform.rotation;
			WeaponList [1].weaponTransform.parent = pistolHolster;
		}
		_animator.SetBool("rifleActive", true);
		currentWeapon = WeaponList[0];
		yield return new WaitForSeconds(0.7f);	
		usingRifle = true;
		usingPistol = false;
		cursorRiffle.SetActive (true);
		cursorPistol.SetActive (false);
		cursorUnarmed.SetActive (false);
		WeaponList[0].weaponTransform.position = myHand.transform.position;
		WeaponList[0].weaponTransform.rotation = myHand.transform.rotation;
		WeaponList[0].weaponTransform.parent = myHand.transform;
		yield return new WaitForSeconds (1);
		_canShoot = true;
	}
	IEnumerator HideRifle(){
		_canShoot = false;
		_animator.SetBool("rifleActive", false);
		currentWeapon = WeaponList[0];
		yield return new WaitForSeconds(1.3f);	
		usingRifle = false;
		cursorRiffle.SetActive (false);
		cursorPistol.SetActive (false);
		cursorUnarmed.SetActive (true);
		WeaponList[0].weaponTransform.position = rifleHolster.transform.position;
		WeaponList[0].weaponTransform.rotation = rifleHolster.transform.rotation;
		WeaponList[0].weaponTransform.parent = rifleHolster.transform;
	
	}
//SWITCHING TO PISTOL
	IEnumerator SwapPistol(){
		if (usingRifle) {
			_canShoot = false;
			_animator.SetBool ("rifleActive", false);
			yield return new WaitForSeconds (0.7f);
			usingRifle = false;
			WeaponList [0].weaponTransform.position = rifleHolster.transform.position;
			WeaponList [0].weaponTransform.rotation = rifleHolster.transform.rotation;
			WeaponList [0].weaponTransform.parent = rifleHolster;
		}
		_animator.SetBool("pistolActive", true);
		currentWeapon = WeaponList[1];
		yield return new WaitForSeconds(0.4f);	
		usingRifle = false;
		usingPistol = true;
		_canShoot = true;
		cursorRiffle.SetActive (false);
		cursorPistol.SetActive (true);
		cursorUnarmed.SetActive (false);
		WeaponList[1].weaponTransform.position = pistolHandHolder.transform.position;
		WeaponList[1].weaponTransform.rotation = pistolHandHolder.transform.rotation;
		WeaponList[1].weaponTransform.parent = pistolHandHolder.transform;
	}
	IEnumerator HidePistol(){
		_canShoot = false;
		_animator.SetBool("pistolActive", false);
		currentWeapon = WeaponList[1];
		yield return new WaitForSeconds(0.7f);	
		usingPistol = false;
		cursorRiffle.SetActive (false);
		cursorPistol.SetActive (false);
		cursorUnarmed.SetActive (true);
		WeaponList[1].weaponTransform.position = pistolHolster.transform.position;
		WeaponList[1].weaponTransform.rotation = pistolHolster.transform.rotation;
		WeaponList[1].weaponTransform.parent = pistolHolster.transform;
	}
//SIT IN CHAIR
	IEnumerator SitInChair(){
		_animator.SetTrigger ("Stop");
		_collMain.enabled = false;
		canUseIK = false;
		canRotate = false;
		if(usingPistol){
			StartCoroutine(HidePistol());
		}
		if (usingRifle) {
			StartCoroutine (HideRifle ());
		}
		yield return new WaitForSeconds(0.5f);
		GameObject sitPoint = curChair.transform.Find("SitPoint").gameObject;
		this.transform.parent = sitPoint.transform;
		this.transform.position = sitPoint.transform.position;
		this.transform.rotation = sitPoint.transform.rotation;
		_animator.SetBool("sitInChair", true);
		Animator chairAnim=curChair.GetComponent<Animator>();
		chairAnim.SetBool ("sitIn", true);
		MainCam.GetComponent<MouseOrbitImproved> ().enabled = false;
		yield return new WaitForSeconds(2f);
		chairHandWeight = 1;
		GameObject seat = curChair.transform.Find("seat").gameObject;
		this.transform.position = seat.transform.position;
		yield return new WaitForSeconds(2f);
		_inChair = true;
		curShip = curChair.transform.root.gameObject;
		curShip.GetComponent<SpaceShipEngine>().EngineStarted = true;
		curShip.GetComponent<SpaceShipControl> ().HideDockedCar();
		curShip.GetComponent<SpaceShipControl> ().canControl = true;
		curShip.GetComponent<Rigidbody> ().isKinematic = false;
		curChair.GetComponent<Rigidbody> ().detectCollisions = false;
		SwitchCam(curShip, 50f);
		if (curShip.GetComponent<SpaceShipControl> ().isUsingReflProbes) {
			curShip.GetComponent<SpaceShipControl> ().HideReflection();
		}
	}
//GET OFF CHAIR
	IEnumerator GetUpChair(){
		Animator chairAnim=curChair.GetComponent<Animator>();
		chairAnim.SetBool ("sitIn", false);
		chairHandWeight = 0;
		_animator.SetBool("sitInChair", false);
		curShip.GetComponent<SpaceShipControl> ().canControl = false;
		curShip.GetComponent<Rigidbody> ().isKinematic = true;
		curChair.GetComponent<Rigidbody> ().detectCollisions = true;
		SwitchCamPlayer ();
		MainCam.transform.SetParent(null);
		if (curShip.GetComponent<SpaceShipControl> ().isUsingReflProbes) {
			curShip.GetComponent<SpaceShipControl> ().ShowReflection();
		}
		curShip.GetComponent<SpaceShipControl> ().ShowDockedCar();
		curShip.GetComponent<SpaceShipEngine>().EngineStarted = false;
		yield return new WaitForSeconds(1f);
		MainCam.transform.SetParent(this.transform);
		GameObject sitPoint = curChair.transform.Find("SitPoint").gameObject;
		this.transform.parent = null;
		_collMain.enabled = true;
		transform.position = sitPoint.transform.position;
		canRotate = true;		
		canUseIK = true;
		_inChair = false;
	}
//GET IN THE CAR
	IEnumerator GetInCar(){
		Animator CarAnim = curCar.GetComponent<Animator> ();
		_collMain.enabled = false;
		canUseIK = false;
		canRotate = false;
		_animator.SetTrigger ("Stop");
		if(usingPistol){
			StartCoroutine(HidePistol());
			yield return new WaitForSeconds (2);
		}
		if(usingRifle){
			StartCoroutine(HideRifle());
			yield return new WaitForSeconds (2);
		}
		yield return new WaitForSeconds(0.5f);
		this.transform.parent = curCar.transform;
		if (_carRightDoor != null) {
			GameObject SitStartPos = curCar.transform.Find("sitInCarPosition").gameObject;
			CarAnim.SetTrigger ("rightDoor");
			this.transform.position = SitStartPos.transform.position;
			this.transform.rotation = SitStartPos.transform.rotation;
			_animator.SetBool ("SitInCarRight", true);

		}
		if (_carLeftDoor != null) {
			GameObject SitStartPos = curCar.transform.Find("sitInCarPositionL").gameObject;
			CarAnim.SetTrigger ("leftDoor");
			this.transform.position = SitStartPos.transform.position;
			this.transform.rotation = SitStartPos.transform.rotation;
			_animator.SetBool ("SitInCarLeft", true);
		}
		MainCam.GetComponent<MouseOrbitImproved> ().enabled = false;
		yield return new WaitForSeconds (4);
		driving = true;
		GameObject carSeat = curCar.transform.Find("CarChairPosition").gameObject;
		this.transform.position = carSeat.transform.position;
		this.transform.rotation = carSeat.transform.rotation;
		SwitchCam(curCar, 20f);
	}
//GET OUT THE CAR
	IEnumerator GetOutCar(){
		Animator CarAnim = curCar.GetComponent<Animator> ();
		CarAnim.SetTrigger ("OpenFromInside");
		_animator.SetBool ("SitInCarRight", false);
		_animator.SetBool ("SitInCarLeft", false);
		driving = false;
		GameObject SitStartPos = curCar.transform.Find("sitInCarPosition").gameObject;
		SwitchCamPlayer ();
		yield return new WaitForSeconds (4);
		this.transform.position = SitStartPos.transform.position;
		this.transform.rotation = SitStartPos.transform.rotation;
		_collMain.enabled = true;
		canUseIK = true;
		canRotate = true;
		this.transform.parent = null;
	}
//GET IN SPACESHIP
	IEnumerator GetInShip(){
		canUseIK = false;
		canRotate = false;
		_animator.SetTrigger ("Stop");
		if(usingPistol){
			StartCoroutine(HidePistol());
			yield return new WaitForSeconds (2);
		}
		if(usingRifle){
			StartCoroutine(HideRifle());
			yield return new WaitForSeconds (2);
		}
		_collMain.enabled = false;
		this.transform.parent = curShip.transform;
		GameObject sitPoint = curShip.transform.Find("SitPointR").gameObject;
		this.transform.position = sitPoint.transform.position;
		this.transform.rotation = sitPoint.transform.rotation;
		_animator.SetBool("GetInShipR", true);
		yield return new WaitForSeconds(3f);
		GameObject seat = curShip.transform.Find("seat").gameObject;
		this.transform.position = seat.transform.position;
		this.transform.rotation = seat.transform.rotation;
		curShip.GetComponent<SpaceShipControl> ().canControl = true;
		curShip.GetComponent<SpaceShipEngine>().EngineStarted = true;
		curShip.GetComponent<Rigidbody> ().isKinematic = false;
		SwitchCam(curShip, 20f);
	}
//GET OUT SPACESHIP
	IEnumerator GetOutShip(){
		_animator.SetBool("GetInShipR", false);
		curShip.GetComponent<SpaceShipEngine>().EngineStarted = false;
		curShip.GetComponent<SpaceShipControl> ().canControl = false;
		curShip.GetComponent<Rigidbody> ().isKinematic = true;
		SwitchCamPlayer ();
		yield return new WaitForSeconds(3f);
		GameObject sitPoint = curShip.transform.Find("SitPointR").gameObject;
		this.transform.parent = null;
		_collMain.enabled = true;
		transform.position = sitPoint.transform.position;
		canRotate = true;		
		canUseIK = true;
	}
//GET ON HOVER
	IEnumerator GetOnHover(){
		canUseIK = false;
		canRotate = false;
		_animator.SetTrigger ("Stop");
		if(usingPistol){
			StartCoroutine(HidePistol());
			yield return new WaitForSeconds (2);
		}
		if(usingRifle){
			StartCoroutine(HideRifle());
			yield return new WaitForSeconds (2);
		}
		_collMain.enabled = false;
		this.transform.SetParent(curHover.transform);
		GameObject sitPoint = curHover.transform.Find("SitPoint").gameObject;
		this.transform.position = sitPoint.transform.position;
		this.transform.rotation = sitPoint.transform.rotation;
		_animator.SetBool("GetInHover", true);
		yield return new WaitForSeconds(5f);
		this.GetComponent<HandPlacement>().LeftHandPos = curHover.GetComponent<Hover>().LHandPos;
		this.GetComponent<HandPlacement>().RightHandPos = curHover.GetComponent<Hover>().RHandPos;
		this.GetComponent<HandPlacement>().handIk = true;
		GameObject seat = curHover.transform.Find("seat").gameObject;
		this.transform.position = seat.transform.position;
		this.transform.rotation = seat.transform.rotation;
		curHover.GetComponent<Rigidbody> ().isKinematic = false;
		curHover.GetComponent<Hover> ().canControl = true;
		SwitchCam(curHover, 10f);
	}
//Get Off hover
	IEnumerator GetOffHover(){
		_animator.SetBool("GetInHover", false);
		curHover.GetComponent<Rigidbody> ().drag = 5;
		curHover.GetComponent<Hover> ().canControl = false;
		SwitchCamPlayer ();
		this.GetComponent<HandPlacement>().handIk = false;
		yield return new WaitForSeconds(5f);
		GameObject sitPoint = curHover.transform.Find("SitPoint").gameObject;
		curHover.GetComponent<Rigidbody> ().isKinematic = true;
		this.transform.parent = null;
		_collMain.enabled = true;
		transform.position = sitPoint.transform.position;
		canRotate = true;		
		canUseIK = true;
	}
//CAMERA SWITCHER
	void SwitchCam(GameObject camTarget, float camDist){
		if(inSpace && curHover == null){
			MainCam.GetComponent<MouseOrbitSimple> ().enabled = true;
			MainCam.GetComponent<SmoothFollow> ().enabled = false;
			MainCam.GetComponent<MouseOrbitImproved> ().enabled = false;
			MainCam.GetComponent<MouseOrbitSimple>().PlayerCamTarget = curShip.transform.Find("camTarget");
		}else{
		MainCam.GetComponent<SmoothFollow> ().enabled = true;
		MainCam.GetComponent<SmoothFollow> ().target = camTarget.transform;
		MainCam.GetComponent<SmoothFollow> ().distance = camDist;
		MainCam.GetComponent<MouseOrbitImproved> ().enabled = false;
		}
	}
	void SwitchCamPlayer(){
		MainCam.GetComponent<SmoothFollow> ().enabled = false;
		MainCam.GetComponent<MouseOrbitImproved> ().enabled = true;
		MainCam.GetComponent<MouseOrbitSimple> ().enabled = false;
	}
//FOOTSTEP SOUND FUNCTION IS CALLED FROM ANIMATION EVENT 
	void footStep(){
		if (isGrounded () && _collMain.enabled==true) {	
//SHOOTING RAY DOWN
			Vector3 bottom = -transform.up;
			Vector3 btmRayStart = new Vector3(this.transform.position.x, this.transform.position.y+1, this.transform.position.z);
			Ray	btmRay = new Ray(btmRayStart, bottom);
			RaycastHit hit;
			Debug.DrawRay(btmRayStart, transform.forward, Color.green);
			if(Physics.Raycast(btmRay, out hit, 2f)){
				if(hit.transform.gameObject.layer == 9){
					int n = Random.Range (1, m_MetalFootstepSounds.Length);
					m_AudioSource.clip = m_MetalFootstepSounds [n];
					m_AudioSource.PlayOneShot (m_AudioSource.clip);
					//			move picked sound to index 0 so it's not picked next time
					m_MetalFootstepSounds [n] = m_MetalFootstepSounds [0];
					m_MetalFootstepSounds [0] = m_AudioSource.clip;
				}else{
					int n = Random.Range (1, m_FootstepSounds.Length);
					m_AudioSource.clip = m_FootstepSounds [n];
					m_AudioSource.PlayOneShot (m_AudioSource.clip);
					//			move picked sound to index 0 so it's not picked next time
					m_FootstepSounds [n] = m_FootstepSounds [0];
					m_FootstepSounds [0] = m_AudioSource.clip;
				}
			}
		}
	}
	void jumpSound(){
		m_AudioSource.clip = m_jumpSound;
		m_AudioSource.PlayOneShot (m_AudioSource.clip);
	}
}

