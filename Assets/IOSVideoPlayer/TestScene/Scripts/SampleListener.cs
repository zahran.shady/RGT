using UnityEngine;
using System.Collections;

public class SampleListener : MonoBehaviour
{

	void Start ()
	{
		//IOSVideoPlayerBinding.onVideoPlaybackCompleteEvent += VideoPlayBackCompletedNormally;
		//IOSVideoPlayerBinding.onVideoEndBySkipEvent += VideoPlaybackEndedBySkipButton;
		//IOSVideoPlayerBinding.onPlayButtonTapEvent += PlayButtonPushed;
		//IOSVideoPlayerBinding.onPauseButtonTapEvent += PauseButtonPushed;
		//IOSVideoPlayerBinding.onStopButtonTapEvent += StopButtonPushed;
		//IOSVideoPlayerBinding.onCustomButtonTapEvent += CustomButtonPushed;
		//IOSVideoPlayerBinding.onErrorMessageEvent += ErrorMessageReceived;
	}
	
	void VideoPlayBackCompletedNormally()
	{
		Debug.Log( "Video playback complete" );
	}
	
	void VideoPlaybackEndedBySkipButton()
	{
		Debug.Log( "Video playback ended by Skip Button" );
	}
	
	void PlayButtonPushed()
	{
		Debug.Log( "Play button pushed" );
	}
	
	void PauseButtonPushed()
	{
		Debug.Log( "Pause button pushed" );
	}
	
	void StopButtonPushed()
	{
		Debug.Log( "Stop button pushed" );
	}

	void CustomButtonPushed(int id)
	{
		Debug.Log( "Custom button pushed:" + id.ToString() );
	}

	void ErrorMessageReceived( VideoPlayerErrorCode errorCode, int givenID )
	{
		Debug.Log( "Error Message Received: " + errorCode.ToString() + ", Given ID: " + givenID );
	}
}
