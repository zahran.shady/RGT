using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO; 
public class GuiManager : MonoBehaviour
{
	bool[] tog = new bool[ 7 ];
	float buttonHeight = Screen.height / 5;
	float buttonWidth = ( Screen.width / 2 ) - 20;

	
	void Start()
	{
		tog = new bool[]{ false, false, false, false,true,false, false  };

	}

	void Update() 
	{
		buttonHeight = Screen.height / 7;
		buttonWidth =  ( Screen.width / 2 ) - 20;
	}



	void OnGUI ()
	{
		GUI.color = Color.black;
		float buttonPositionX = 10;
		float buttonPositionY = 10;
		
		GUI.color = Color.white;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Play Video with name" ) )
		{
				IOSVideoPlayerBinding.instance.PlayVideo( "Teaser_Final.mov" );
		}
		
		buttonPositionY += buttonHeight + 10;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Play Video with path" ) )
		{
				IOSVideoPlayerBinding.instance.PlayVideo( Application.dataPath + "/Raw/","Teaser_Final.mov" );
		}
		
		buttonPositionY += buttonHeight + 10;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Play with Subtitles" ) )
		{
			IOSVideoPlayerBinding.instance.playSubtitlesFromFile( "Teaser_Final_EN.srt" );
			IOSVideoPlayerBinding.instance.PlayVideo( "Teaser_Final.mov" );
		}
		
		buttonPositionY += buttonHeight + 10;
		if( !tog[ 4 ] )
		{
			GUI.color = Color.green;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Should pause unity" ) )
			{
					IOSVideoPlayerBinding.instance.shouldPauseUnity( true );
					tog[ 4 ] = !tog[ 4 ];
			}
			
			GUI.color = Color.white;
			GUI.Label( new Rect( buttonPositionX, ( buttonPositionY + buttonHeight + 5 ), buttonWidth, buttonHeight), "Current Status: Unity will keep running during video playback." );
		}
		else
		{
			GUI.color = Color.red;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Should not pause unity" ) )
			{
					IOSVideoPlayerBinding.instance.shouldPauseUnity( false );
					tog[ 4 ] = !tog[ 4 ];
			}
			
			GUI.color = Color.white;
			GUI.Label( new Rect( buttonPositionX, ( buttonPositionY + buttonHeight + 5 ), buttonWidth, buttonHeight ), "Current Status: Unity will be paused during video playback." );
		}
		
		buttonPositionY = 10;
		buttonPositionX += buttonWidth + 10;
		if( !tog[ 0 ] )
		{
			GUI.color = Color.green;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Add Play/Pause Button" ) )
			{
					IOSVideoPlayerBinding.instance.addPlayPauseButton();
					tog[ 0 ] = !tog[ 0 ];
			}
		}
		else
		{
			GUI.color = Color.red;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Remove Play/Pause Button" ) )
			{
				IOSVideoPlayerBinding.instance.removePlayPauseButton();
				tog[ 0 ] = !tog[ 0 ];
			}
		}
				
		buttonPositionY += buttonHeight + 10;
		if( !tog[ 2 ] )
		{
			GUI.color = Color.green;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Add Stop Button" ) )
			{
					IOSVideoPlayerBinding.instance.addStopButton();
					tog[ 2 ] = !tog[ 2 ];
			}
		}
		else
		{
			GUI.color = Color.red;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Remove Stop Button" ) )
			{
					IOSVideoPlayerBinding.instance.removeStopButton();
					tog[ 2 ] = !tog[ 2 ];
			}
		}
			
		buttonPositionY += buttonHeight + 10;
		if( !tog[ 3 ] )
		{
			GUI.color = Color.green;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Add Seeking Bar" ) )
			{
					IOSVideoPlayerBinding.instance.addSeekingBar();
					tog[ 3 ] = !tog[ 3 ];
			}
		}
		else
		{
			GUI.color = Color.red;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Remove Seeking Bar" ) )
			{
					IOSVideoPlayerBinding.instance.removeSeekingBar();
					tog[ 3 ] = !tog[ 3 ];
			}
		}
		
		buttonPositionY += buttonHeight + 10;
		if( !tog[ 1 ] )
		{
			GUI.color = Color.green;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Add Skip Button" ) )
			{
				IOSVideoPlayerBinding.instance.addSkipButton();
				tog[ 1 ] = !tog[ 1 ];
			}
		}
		else
		{
			GUI.color = Color.red;
			if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "Remove Skip Button" ) )
			{
				IOSVideoPlayerBinding.instance.removeSkipButton();
				tog[ 1 ] = !tog[ 1 ];
			}
		}

		//Custom Button
		buttonPositionY += buttonHeight + 10;
		if( !tog[ 5 ] )
		{
			GUI.color = Color.green;
			if( GUI.Button( new Rect( buttonPositionX , buttonPositionY, buttonWidth, buttonHeight ), "Add Custom Button" ) )
			{
				int ID =222;
				IOSVideoPlayerBinding.instance.addCustomButton(ID);




				tog[ 5 ] = !tog[ 5 ];
			}
		}
		else
		{
			GUI.color = Color.red;
			if( GUI.Button( new Rect( buttonPositionX , buttonPositionY, buttonWidth, buttonHeight ), "Remove Custom Button" ) )
			{
				IOSVideoPlayerBinding.instance.removeCustomButton(222);
				tog[ 5 ] = !tog[ 5 ];
			}
		}
		buttonPositionY += buttonHeight + 10;
		if( !tog[ 6 ] )
		{
			GUI.color = Color.green;
			if( GUI.Button( new Rect( buttonPositionX , buttonPositionY, buttonWidth, buttonHeight ), "Add Custom Button 2" ) )
			{
				int ID =223;
				IOSVideoPlayerBinding.instance.addCustomButton(20,300,ID);
								
				
				tog[ 6 ] = !tog[ 6 ];
			}
		}
		else
		{
			GUI.color = Color.red;
			if( GUI.Button( new Rect( buttonPositionX , buttonPositionY, buttonWidth, buttonHeight ), "Remove Custom Button 2" ) )
			{
				IOSVideoPlayerBinding.instance.removeCustomButton(223);
				tog[ 6 ] = !tog[ 6 ];
			}
		}

	}
}