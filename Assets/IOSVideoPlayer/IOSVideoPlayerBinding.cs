using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum VideoScalingMode
{
	Default = 0,		//Default mode is AspectFit
	AspectFit = 1,
	AspectFill = 2,
	Fill = 3
}

public enum VideoPlayerErrorCode
{
	ButtonAlreadyExistsWithGivenID = 1,
	ButtonDoesNotExistWithGivenID = 2
}

public class IOSVideoPlayerBinding  : MonoBehaviour
{
	[ DllImport ( "__Internal" ) ]
	private static extern void _setObjectName( string objName );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _playMovieWithName( string fileName );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _playMovieWithURL( string fileName );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _playMovieWithTrimmedDuration( string movieName, float startDuration, float endDuration );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _playMovieTrimmedFromStart( string movieName, float startDuration );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _playMovieTrimmedFromEnd( string movieName, float endDuration );
	
	[ DllImport ( "__Internal" ) ]
	private static extern  void _addPlayPauseButton( string playImageName, string pauseImageName, float x, float y );
	
    [ DllImport ( "__Internal" ) ]
	private static extern void _addSkipButton( string skipImageName, float x, float y );
	
    [ DllImport ( "__Internal" ) ]
	private static extern void _addStopButton( string stopImageName, float x, float y );

	//Custom Button
	[ DllImport ( "__Internal" ) ]
	private static extern void _addCustomButton( string customImageName, float x, float y, int id );

	//Custom Button
	
   	[ DllImport ( "__Internal" ) ]
	private static extern void _addSeekingBarAtPosition( float x, float y, string coveredImagename, string reamaningImageName, string thumbNormalName, string thumbDownName );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _playSubtitlesFromFile( string subtitleFileName );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _setSubtitleFontSize( float thisSize );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _setSubtitleYCoordiante( float thisCoordinate );

	[ DllImport ( "__Internal" ) ]
	private static extern void _setMovieScalingMode( int givenScalingModeInt );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void  _removePlayPauseButton();
    
	[ DllImport ( "__Internal" ) ]
	private static extern void  _removeSkipButton();
    
	[ DllImport ( "__Internal" ) ]
	private static extern void  _removeStopButton();
	//Custom Button

	[ DllImport ( "__Internal" ) ]
	private static extern void  _removeCustomButton(int ButtonId);

    //Custom Button

	[ DllImport ( "__Internal" ) ]
	private static extern void  _removeSeekingBar();
	
	[ DllImport ( "__Internal" ) ]
	private static extern float  _getDeviceDisplayScale();
	
	[ DllImport ( "__Internal" ) ]
	private static extern void  _shouldPauseUnity( bool flag );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _setSubtitleFontStyle( string subtitleFontName );

	[ DllImport ( "__Internal" ) ]
	private static extern void _setSkipButtonLabelWithTextFontStyleAndSize( string givenText, string givenFontName, float givenFontSize );

	[ DllImport ( "__Internal" ) ]
	private static extern void _playPauseToggle();

	[ DllImport ( "__Internal" ) ]
	private static extern float _getCurrentPlaybackTime();

    public GameObject loadingPrefab;
    public static IOSVideoPlayerBinding instance;
    private bool isVideoDone = false;
    private string goName = "IOSVideoPlayer";

    private Transform loadingBar;
    void Start()
	{
		instance = this;
		DontDestroyOnLoad( gameObject );
		if( gameObject != null && gameObject.name != null )
		{
			goName = gameObject.name;
			if( !Application.isEditor )
			{
				_setObjectName( goName );
			}
		}
		else
		{
			Debug.LogWarning( "GameObject Not Found" );
		}
        loadingBar = loadingPrefab.transform.GetChild(0).GetChild(0);
    }
	
	//Plays a video file of the given name.
	//This file should be place in the Assets>StreamingAssets folder in the Unity project. ( plays from *.app/Data/Raw )
	public void PlayVideo( string name )
	{
		Debug.Log( "playMovieWithName" );
	
		if ( !Application.isEditor )
		{
			
			if( System.IO.File.Exists( Application.dataPath + "/" + name ) )
			{
				_playMovieWithName( name );
			}
			else if( System.IO.File.Exists( Application.dataPath + "/Raw/" + name ) )
			{
				_playMovieWithName( "/Data/Raw/" + name );
			}
			else
			{
				Debug.Log( "File Not found at " + Application.dataPath + "/Raw/" + name );
			}
		}
        StartCoroutine("AsynchronousLoad");
    }
	
	//Plays a video file of given name, located at the given absolute path
	public void PlayVideo( string path, string name )
	{
		Debug.Log( "playMovieWithURL" );
		if( !Application.isEditor )
		{
			if( path.EndsWith( "/" ) )
			{
				if( System.IO.File.Exists( path + name ) )
				{
					_playMovieWithURL( path + name );
				}
				else
				{
					Debug.Log( "File Not found at " + path + name );
				}
			}
			else
			{
				if( System.IO.File.Exists ( path + "/" + name ) )
				{
					_playMovieWithURL( path + "/" + name );
				}
				else
				{
					Debug.Log( "File Not found at " + path + "/" + name );
				}
			}
		}
	}
	
	//Play a part of a video clip of the given name movieName
	//The Arguments startDuration and endDuration define ( in seconds ) the starting and ending point for playback 
	public void playMovieWithTrimmedDuration( string movieName, float startDuration, float endDuration )
	{
		if( !Application.isEditor )
		{
			_playMovieWithTrimmedDuration( movieName, startDuration, endDuration );
		}
	}
	
	//Play a part of a video clip of the given name movieName, trimmed from start
	//The argument startDuration defines the starting point for playback ( in seconds )
	public void playMovieTrimmedFromStart( string movieName, float startDuration )
	{
		if( !Application.isEditor )
		{
			_playMovieTrimmedFromStart( movieName, startDuration );
		}	
	}
	
	//Play a part of a video clip of the given name movieName, trimmed from end
	//The argument endDuration defines the end point for playback ( in seconds )
	public void playMovieTrimmedFromEnd( string movieName, float endDuration )
	{
		if( !Application.isEditor )
		{
			_playMovieTrimmedFromEnd( movieName, endDuration );
		}	
	}
	
	
	//To show the Play / Pause button during video playback
	//The arguments playImageName and pauseImageName are the names of images to show for the play and pause buttons, respectively
	//The arguments x and y set the position of the button on screen
	public void addPlayPauseButton( string playImageName, string pauseImageName, float x, float y )
	{
		if( !Application.isEditor )
		{
			_addPlayPauseButton( playImageName, pauseImageName, x, y );
		}	
	}
	
	//To show the default Play / Pause button during video playback
	//The arguments x and y set the position of the button on screen
	public void addPlayPauseButton( float x, float y )
	{
		if( !Application.isEditor )
		{
			_addPlayPauseButton( "", "", x, y );
		}	
	}
	
	//To show the default Play / Pause button during video playback, at the default position on screen
	public void addPlayPauseButton()
	{
		if( !Application.isEditor )
		{
			_addPlayPauseButton( "", "", 20, 100 );
		}
	}
	
	
	//To show the Skip button during video playback
	//The argument skipImageName is the name of the image to show for the skip button
	//The arguments x and y set the position of the button on screen
	public void addSkipButton( string skipImageName, float x, float y )
	{
		if( !Application.isEditor )
		{
			_addSkipButton( skipImageName, x, y );
		}
	}
	
	//To show the default Skip button during video playback
	//The arguments x and y set the position of the button on screen
	public void addSkipButton( float x, float y )
	{
		if( !Application.isEditor )
		{
			_addSkipButton( "", x, y );
		}
	}
	
	//To show the default Skip button during video playback, at the default position on screen
	public void addSkipButton()
	{
		if( !Application.isEditor )
		{
			_addSkipButton( "", 20, 150 );
		}
	}
	

	//To show the Stop button during video playback
	//The argument stopImageName is the name of the image to show for the stop button
	//The arguments x and y set the position of the button on screen
	public void addStopButton( string stopImageName, float x, float y )
	{
		if( !Application.isEditor )
		{
			_addStopButton( stopImageName, x, y );
		}
	}
	
	//To show the default Stop button during video playback
	//The arguments x and y set the position of the button on screen
	public void addStopButton( float x, float y )
	{
		if( !Application.isEditor )
		{
			_addStopButton( "", x, y );
		}
	}
	
	//To show the default Stop button during video playback, at the default position on screen
	public void addStopButton()
	{
		if( !Application.isEditor )
		{
			_addStopButton( "", 20, 200 );
		}
	}
	//Custom Button
	//To show the Custom button during video playback
	//The argument customImageName is the name of the image to show for the custom button
	//The arguments x and y set the position of the button on screen
	public void addCustomButton( string customImageName, float x, float y, int id )
	{
		if( !Application.isEditor )
		{
			_addCustomButton( customImageName, x, y, id );
		}
	}
	
	//To show the default Custom button during video playback
	//The arguments x and y set the position of the button on screen
	public void addCustomButton( float x, float y, int id )
	{
		if( !Application.isEditor )
		{
			_addCustomButton( "", x, y, id );
		}
	}
	
	//To show the default Custom button during video playback, at the default position on screen
	public void addCustomButton(int id)
	{
		if( !Application.isEditor )
		{
			_addCustomButton( "", 20, 250, id );
		}
	}
	//Custom Button
	

	//To show the Seek Bar during video playback
	//The arguments x and y set the position of the bar on screen. The ( x, y ) coordinate is set as the position of the top left corner of the bar
	//The argument coveredImageName is the name of the image used for the part of the bar that shows the portion of the track already played ( to the left of the thumb )
	//The argument remainingImageName is the name of the image used for the part of the bar that shows the portion of the track that is yet to be played ( to the right of the thumb )
	//The argument thumbNormalName sets the image of given name as the thumb of the seek bar ( the moving head that shows the portion of the video track that has been played )
	//The argument thumbDownName sets the image of given name as the thumb for the duration when the thumb is held down ( or being dragged ) by touch
	public void addSeekingBar( float x, float y, string coveredImageName = "", string reamaningImageName = "", string thumbNormalName = "", string thumbDownName = "" )
	{
		if( !Application.isEditor )
		{
			_addSeekingBarAtPosition ( x, y, coveredImageName, reamaningImageName, thumbNormalName, thumbDownName );
		}
	}

	//To show the Seek Bar during video playback, using default images for the covered and remaining seek bar; and the default images for the thumb
	//The arguments x and y set the position of the bar on screen. The ( x, y ) coordinate is set as the position of the top left corner of the bar
	public void addSeekingBar( float x, float y )
	{
		if( !Application.isEditor )
		{
			_addSeekingBarAtPosition( x, y, "", "", "", "" );
		}
	}
	
	//To show the Seek Bar during video playback, using default images for the covered and remaining seek bar; and the default images for the thumb
	public void addSeekingBar()
	{
		if( !Application.isEditor )
		{
			_addSeekingBarAtPosition( ( ( Screen.width / 2 ) - ( 150 *_getDeviceDisplayScale() ) ), ( Screen.height - 20 ), "", "", "", "" );
		}
	}


	//Plays subtitles from the file of given name ( also include ".srt" in the file name e.g. "SubtitlesFile.srt" )
	public void playSubtitlesFromFile( string subtitleFileName )
	{
		if( !Application.isEditor )
		{
//			if(System.IO.File.Exists(Application.dataPath+"/"+name))
//			{
				_playSubtitlesFromFile( subtitleFileName );
//			}
//			else if(System.IO.File.Exists(Application.dataPath+"/Raw/"+name))
//			{
//				_playSubtitlesFromFile(subtitleFileName);
//			}
//			else
//				Debug.Log("File Not found at "+Application.dataPath+"/Raw/"+name);
		}
	}
	
	//Sets the subtitle text font size
	public void setSubtitleFontSize( float thisSize )
	{
		if( !Application.isEditor )
		{
			_setSubtitleFontSize( thisSize );
		}
	}
	
	//Sets the y coordinate ( height ) of the subtitle text shown on the screen
	//The text will always appear in the middle horizontally
	public void setSubtitleYCoordiante( float thisCoordinate )
	{
		if( !Application.isEditor )
		{
			_setSubtitleYCoordiante( thisCoordinate );
		}
	}

	//Sets the scaling mode for video playback
	//Default mode is AspectFit
	public void setMovieScalingMode( VideoScalingMode scalingMode )
	{
		if( !Application.isEditor )
		{
			_setMovieScalingMode( ( int )scalingMode );
		}
	}
	
	
	//Removes the Play/Pause button from screen
	public void removePlayPauseButton()
	{
		if( !Application.isEditor )
		{
			_removePlayPauseButton();
		}
	}
	
	//Removes the Skip button from screen
    public void removeSkipButton()
	{
		if( !Application.isEditor )
		{
			_removeSkipButton();
		}
	}
	
	//Removes the Stop button from screen
    public void removeStopButton()
	{
		if( !Application.isEditor )
		{
			_removeStopButton();
		}
	}

	//Custom Button
	//Removes the custom button from screen
	public void removeCustomButton(int ButtonId)
	{
		if( !Application.isEditor )
		{
			_removeCustomButton(ButtonId);
		}
	}
	//Custom Button
	
	//Removes the Seeking bar from screen
    public void removeSeekingBar()
	{
		if( !Application.isEditor )
		{
			_removeSeekingBar();
		}
	}

	//Removes all the control buttons from screen
	//Also removes the seek bar	
	public void removeAllButtons()
	{
		if( !Application.isEditor )
		{
			_removePlayPauseButton();
			_removeSkipButton();
			_removeStopButton();
			_removeSeekingBar();
			_removeCustomButton(222); //Custom Button
		}
	}
	

	//Option to pause unity during video playback
	//Set the argument to true to pause unity, or false to keep it running in the background
	public void shouldPauseUnity( bool flag )
	{
		if( !Application.isEditor )
		{
			_shouldPauseUnity( flag );
		}
	}


	//To show the subtitles in your desired font style
	//The custom font needs to be added to the Unity-exported Xcode project manually
	//The argument "subtitleFontName" is the PostScript name of the font
	//For Times New Roman Italic, this would be: TimesNewRomanPS-ItalicMT
	public void setSubtitleFontStyle( string subtitleFontName )
	{
		if( !Application.isEditor )
		{
			_setSubtitleFontStyle( subtitleFontName );
		}
	}

	//To show a text label over the skip button image
	//First argument is the text to be shown
	//Second argument is the font name
	//Third argument is the font size
	public void setSkipButtonLabelWithTextFontStyleAndSize( string givenText, string givenFontName, float givenFontSize )
	{
		if( !Application.isEditor )
		{
			_setSkipButtonLabelWithTextFontStyleAndSize( givenText, givenFontName, givenFontSize );
		}
	}

	//Calling this function is the same as clicking the Play / Pause button that is shown during the video playback
	//Plays the video if it is paused and pauses it if it is being played
	//Do not use as an alternative for any of the _playMovie.... functions
	//REQUIRES Unity to be running in the background during video playback. Look at the "shouldPauseUnity()" function
	public void playPauseToggle()
	{
		if( !Application.isEditor )
		{
			_playPauseToggle();
		}
	}

	//Returns the current playback time of the video in seconds
	//Returns O or NaN if the video is not playing.
	public float getCurrentPlaybackTime()
	{
		if( !Application.isEditor )
		{
			float theTime = _getCurrentPlaybackTime();
			return theTime;
		}
		else
		{
			return -1f;
		}
	}


    public IEnumerator AsynchronousLoad()
    {
        yield return null;

        AsyncOperation ao = SceneManager.LoadSceneAsync("Garage");
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(ao.progress / 0.9f);
            //Debug.Log("Loading progress: " + (progress * 100) + "%");
            loadingBar.GetComponent<Image>().fillAmount = progress;
            // Loading completed
            if (ao.progress == 0.9f && isVideoDone)
            {
                //Debug.Log("Press a key to start");
                //if (Input.AnyKey())
                ao.allowSceneActivation = true;
            }

            yield return null;
        }
    }

    public void LoadLevel()
    {
        StartCoroutine("AsynchronousLoad");
        loadingPrefab.SetActive(true);
        isVideoDone = true;
    }

#if UNITY_IPHONE

    public static event Action onVideoPlaybackCompleteEvent;
	public static event Action onVideoEndBySkipEvent;
	public static event Action onPlayButtonTapEvent;
	public static event Action onPauseButtonTapEvent;
	public static event Action onStopButtonTapEvent;
	public static event Action < VideoPlayerErrorCode, int > onErrorMessageEvent;
	public static event Action < int > onCustomButtonTapEvent; //Custom Button Event

	//Called when the video playback is complete either because the video reached its end
	//or the Skip Button was pushed
	//The argument msg contains:
	//the string "Done" for video reaching its end;
	//the string "Skip" for when the Skip button is tapped
	public void VideoDonePlaying( string msg )
	{
		if( msg == "Done" )
		{
			//Debug.Log( "Video playback complete" );
			if( onVideoPlaybackCompleteEvent != null )
			{
				onVideoPlaybackCompleteEvent();
			}
		}
		else if( msg == "Skip" )
		{
			//Debug.Log( "Video playback ended by Skip Button" );
			if( onVideoEndBySkipEvent != null )
			{
				onVideoEndBySkipEvent();
			}
		}
        loadingPrefab.SetActive(true);
        isVideoDone = true;
    }
	
	//Called when user taps the Play/Pause button
	//The argument actionInitiated contains:
	//the string "Play" when button tap plays/resumes the video;
	//the string "Pause" when the button tap pauses the video
	public void PlayPauseButtonPushed( string actionInitiated )
	{
		if( actionInitiated == "Play" )
		{
			//Debug.Log( "Play button pushed" );
			if( onPlayButtonTapEvent != null )
			{
				onPlayButtonTapEvent();
			}
		}
		else if( actionInitiated == "Pause" )
		{
			//Debug.Log( "Pause button pushed" );
			if( onPauseButtonTapEvent != null )
			{
				onPauseButtonTapEvent();
			}
		}
	}
	
	//Called when user taps the Stop button
	public void StopButtonPushed( string msg )
	{
		//Debug.Log( "Stop button pushed" );
		if( onStopButtonTapEvent != null )
		{
			onStopButtonTapEvent();
		}
	}
	public void CustomButtonPushed( string msg )
	{
		Debug.Log( "Custom button pushed IVPB:" +msg );
		if( onCustomButtonTapEvent != null )
		{
			onCustomButtonTapEvent( int.Parse( msg ) );
		}
	}
	public void ErrorDisplay(string msg)
	{
		Debug.Log ("Error Occured");

		string[] receivedData = msg.Split( '^' );

		VideoPlayerErrorCode receivedErrorCode = ( VideoPlayerErrorCode )( int.Parse( receivedData[ 0 ] ) );
		int userGivenID = int.Parse( receivedData[ 1 ] );

		if( onErrorMessageEvent != null )
		{
			onErrorMessageEvent( receivedErrorCode, userGivenID );
		}
	}
	#endif
}