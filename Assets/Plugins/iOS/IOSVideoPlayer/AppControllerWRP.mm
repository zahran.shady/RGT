//
//  Created by weRplay on 6/28/13.
//	Modified 10/10/13
//  Copyright (c) 2013 weRplay. All rights reserved.
//


//#import "AppController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "WRPCustomMoviePlayer.h"
#import "UnityInterface.h"


extern "C"
{
	void _playMovieWithTrimmedDuration( char *movieName, double startDuration, double endDuration );
	void _playMovieTrimmedFromStart(  char *movieName, double startDuration );
	void _playMovieTrimmedFromEnd( char *movieName, double endDuration );
	
	void _playMovieWithName(char *str);
    void _playMovieWithURL(char *str);
	
	void _playSubtitlesFromFile( char *subtitleFileName );
	void _setSubtitleFontSize( float thisSize );
	void _setSubtitleYCoordiante( float thisCoordinate );
    
    void _addPlayPauseButton (char * playImageName ,char * pauseImageName ,float x ,float y);
    void _addSkipButton (char * skipImageName ,float x ,float y);
    void _addStopButton(char * stopImageName ,float x ,float y);
    //CustomButon
    void _addCustomButton(char * customImageName ,float x ,float y, int customButtonid);
    //CustomButon
    void _addSeekingBarAtPosition (float x, float y,char * coveredImagename ,char * reamaningImageName,char * thumbNormalName ,char * thumbDownName);
	
	void _setSubtitleFontStyle( char *subtitleFontStyle );
	void _setSkipButtonLabelWithTextFontStyleAndSize( char *givenText, char *givenFontName, float givenFontSize );
	
	void _setMovieScalingMode( int givenScalingModeInt );
	
	void _playPauseToggle();
	float _getCurrentPlaybackTime();
	
    void  _removePlayPauseButton();
    void  _removeSkipButton();
    void  _removeStopButton();
    //Custom Button
    void  _removeCustomButton(int ButtonId);
    //Custom Button
    void  _removeSeekingBar();

    float _getDeviceDisplayScale();
    
	void  _shouldPauseUnity(bool flag);

	void _setObjectName(char *str);
}


void _playMovieWithTrimmedDuration( char *movieName, double startDuration, double endDuration )
{
	[ WRPCustomMoviePlayer playMovieWithName: movieName TrimmedFrom: startDuration To: endDuration ];
	
	[ WRPCustomMoviePlayer setCallObject: [ UnityInterface sharedInstance ] ];
}

void _playMovieTrimmedFromStart(  char *movieName, double startDuration )
{
	[ WRPCustomMoviePlayer playMovieWithName: movieName TrimmedFrom: startDuration ];
	
	[ WRPCustomMoviePlayer setCallObject: [ UnityInterface sharedInstance ] ];
}

void _playMovieTrimmedFromEnd( char *movieName, double endDuration )
{
	[ WRPCustomMoviePlayer playMovieWithName: movieName TrimmedTo: endDuration ];
	
	[ WRPCustomMoviePlayer setCallObject: [ UnityInterface sharedInstance ] ];
}

void _playMovieWithName(char *str)
{
    [ WRPCustomMoviePlayer playMovieWithName: str ];

   	[ WRPCustomMoviePlayer setCallObject: [ UnityInterface sharedInstance ] ];
}

void _playMovieWithURL(char *str)
{
    [ WRPCustomMoviePlayer playMovieWithURL: str ];
	
   	[ WRPCustomMoviePlayer setCallObject: [ UnityInterface sharedInstance ] ];
}

void _playSubtitlesFromFile( char *subtitleFileName )
{
	[ WRPCustomMoviePlayer playSubtitlesFromFile: subtitleFileName ];
}

void _setSubtitleFontSize( float thisSize )
{
	[ WRPCustomMoviePlayer setSubtitleFontSize: thisSize ];
}

void _setSubtitleYCoordiante( float thisCoordinate )
{
	[ WRPCustomMoviePlayer setSubtitleYCoordiante: thisCoordinate ];
}

void _addPlayPauseButton (char * playImageName ,char * pauseImageName ,float x ,float y)
{
    [ WRPCustomMoviePlayer addPlayPauseButtonWithPlayImageName: playImageName PauseImageName : pauseImageName AtPositionX : x PositionY : y ];
    
}
void _addSkipButton (char * skipImageName ,float x ,float y)
{
    [ WRPCustomMoviePlayer addSkipButtonWithImageName: skipImageName AtPositionX:x PositionY:y ];
    
}
void _addStopButton(char * stopImageName ,float x ,float y)
{
    [ WRPCustomMoviePlayer addStopButtonWithImageName: stopImageName AtPositionX: x PositionY: y ];
}
//Custom Button
void _addCustomButton(char * customImageName ,float x ,float y, int customButtonid)
{
    [ WRPCustomMoviePlayer addCustomButtonWithImageName: customImageName AtPositionX: x PositionY: y WithId: customButtonid];
}
//Custom Button

void _addSeekingBarAtPosition (float x, float y,char * coveredImagename ,char * remainingImageName,char * thumbNormalName ,char * thumbDownName)
{
    [ WRPCustomMoviePlayer addSeekingBarAtPositionX:x PositionY:y WithImageCovered: coveredImagename ImageRemaining: remainingImageName ImageThumbNormal: thumbNormalName ImageThumbDown: thumbDownName ];
}



void _setSubtitleFontStyle( char *subtitleFontStyle )
{
	[ WRPCustomMoviePlayer setSubtitleFontStyle: subtitleFontStyle ];
}

void _setSkipButtonLabelWithTextFontStyleAndSize( char *givenText, char *givenFontName, float givenFontSize )
{
	[ WRPCustomMoviePlayer setSkipButtonLabelWithText: givenText FontStyle: givenFontName FontSize: givenFontSize ];
}

void _setMovieScalingMode( int givenScalingModeInt )
{
	[ WRPCustomMoviePlayer setMovieScalingMode: givenScalingModeInt ];
}

void _playPauseToggle()
{
	[ WRPCustomMoviePlayer playPauseToggle ];
}

float _getCurrentPlaybackTime()
{
	return [ WRPCustomMoviePlayer getCurrentPlaybackTime ];
}



void  _removePlayPauseButton()
{
    [ WRPCustomMoviePlayer removePlayPauseButton ];
}

void  _removeSkipButton()
{
    [ WRPCustomMoviePlayer removeSkipButton ];
    
}
void  _removeStopButton()
{
    [ WRPCustomMoviePlayer removeStopButton ];
    
}
//Custom Button
void  _removeCustomButton(int ButtonId)
{
    [ WRPCustomMoviePlayer removeCustomButton: ButtonId ];
    
}
//Custom Button

void  _removeSeekingBar()
{
    [ WRPCustomMoviePlayer removeSeekingBar ];
    
}

float _getDeviceDisplayScale()
{
    return [ [ UIScreen mainScreen ] scale ];
}

void  _shouldPauseUnity(bool flag)
{
     [ WRPCustomMoviePlayer shouldPauseUnity: flag ];
}

void _setObjectName(char *str)
{
	NSLog(@"-> Object name %s", str);
    
    [ WRPCustomMoviePlayer setObjectName: str ];
	[ WRPCustomMoviePlayer setCallObject: [ UnityInterface sharedInstance ] ];
}