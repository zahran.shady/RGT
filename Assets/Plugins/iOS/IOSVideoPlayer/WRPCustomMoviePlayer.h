//
//  WRPCustomMoviePlayer.h
//  CustomMoviePlayerSkinnable
//
//  Created by fhq on 6/9/13.
//  Copyright (c) 2013 fhq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WRPCustomMoviePlayer : NSObject
{
	
}

+( void ) playMovieWithName: ( const char * )movieName TrimmedFrom: ( double )startDuration To: ( double )endDuration;
+( void ) playMovieWithName: ( const char * )movieName TrimmedFrom: ( double )startDuration;
+( void ) playMovieWithName: ( const char * )movieName TrimmedTo: ( double )endDuration;

+( void ) playMovieWithName: ( const char * )movieName;
+( void ) playMovieWithURL: ( const char * )movieNameWithPath;

+( void ) playSubtitlesFromFile: ( const char * )subtitleFileName;
+( void ) setSubtitleFontStyle: ( const char * )subtitleFontStyle;
+( void ) setSubtitleFontSize: ( float )thisSize;
+( void ) setSubtitleYCoordiante: ( float )thisCoordinate;

+( void ) setMovieScalingMode: ( int )givenScalingModeInt;

+( void ) addPlayPauseButtonWithPlayImageName: ( const char * )playImageFileName PauseImageName: ( const char * )pauseImageFileName AtPositionX: ( float )coordinateX PositionY: ( float )coordinateY;
+( void ) addSkipButtonWithImageName: ( const char * )imageFileName AtPositionX: ( float )coordinateX PositionY: ( float )coordinateY;
+( void ) setSkipButtonLabelWithText: ( const char * )givenText FontStyle: ( const char * )givenFontName FontSize: ( float )givenFontSize;
+( void ) addStopButtonWithImageName: ( const char * )imageFileName AtPositionX: ( float )coordinateX PositionY: ( float )coordinateY;
+( void ) addSeekingBarAtPositionX: ( float )coordinateX PositionY: ( float )coordinateY WithImageCovered: ( const char * )trackCoveredFileName ImageRemaining: ( const char * )trackRemainingFileName ImageThumbNormal: ( const char * )thumbNormalFileName ImageThumbDown: ( const char * )thumbDownFileName;

// Custom Button
+( void ) addCustomButtonWithImageName: ( const char * )imageFileName AtPositionX: ( float )coordinateX PositionY: ( float )coordinateY WithId: ( int )customButtonid;
+( void ) removeCustomButton: (int) ButtonId;
// Custom Button

+( void ) removePlayPauseButton;
+( void ) removeSkipButton;
+( void ) removeStopButton;
+( void ) removeSeekingBar;

+( void ) shouldPauseUnity: ( bool )thisValue;

+( void ) setObjectName: ( const char * )objectName;
+( void ) setCallObject: ( id )givenObject;

+( void ) shouldResumeVideoPlaybackOnApplicationResume: ( BOOL )resumeStatus;
+( void ) playPauseToggle;

+( float ) getCurrentPlaybackTime;

@end