1
00:00:02,617 --> 00:00:07,600
Les cristaux étaient bien trop puissants pour Idana

2
00:00:08,829 --> 00:00:11,800
Son esprit était faible… trop faible

3
00:00:13,000 --> 00:00:17,100
Avec elle, les cristaux étaient aussi perdus,
et la Tour des Songes détruite

4
00:00:19,000 --> 00:00:22,500
Helios, Dieu du Soleil, apparut

5
00:00:23,507 --> 00:00:26,100
Il confia à Nito le soin de reconstruire
la Tour des Songes

6
00:00:27,000 --> 00:00:29,177
de rassembler les Orbes de Songes dispersés

7
00:00:30,100 --> 00:00:33,900
Helios offrit à Nito d'utiliser les Portails
des Songes afin de mener sa quête à bien

8
00:00:34,819 --> 00:00:38,198
une nouvelle aventure attendait Nito

