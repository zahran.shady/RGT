1
00:00:02,617 --> 00:00:07,600
The crystals were too powerful for Idana

2
00:00:08,829 --> 00:00:11,800
her spirit was weak…too weak

3
00:00:13,000 --> 00:00:17,100
with her, the crystals were also lost and
the Tower of Dreams destroyed

4
00:00:19,000 --> 00:00:22,500
Helios, the God of Sun, appeared

5
00:00:23,507 --> 00:00:26,100
he told Nito that the Tower of Dreams
must be rebuilt

6
00:00:27,000 --> 00:00:29,177
the scattered Dream Orbs need to be returned

7
00:00:30,100 --> 00:00:33,900
Helios would grant Nito Dream Gates to aid
this quest

8
00:00:34,819 --> 00:00:38,198
a new adventure awaited Nito

